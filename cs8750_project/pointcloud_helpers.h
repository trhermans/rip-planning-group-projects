#pragma once

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/PointCloud2.h>

#include <gt_3d_laser/TableInfo.h>
#include <gt_3d_laser/PlaneInfo.h>
#include <gt_3d_laser/PlaneInfos.h>
#include <gt_3d_laser/ObjectInfo.h>
#include <gt_3d_laser/ObjectInfos.h>

//#include <pcl/io/io.h>
#include <pcl/point_types.h>
//#include <pcl_ros/subscriber.h>

#include <pcl/registration/transforms.h>
//#include <pcl/kdtree/kdtree.h>
//#include <pcl/kdtree/kdtree_flann.h>
//#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl_ros/surface/convex_hull.h>
#include <visualization_msgs/Marker.h>


typedef pcl::PointXYZ Point;

pcl::PointIndices getIndices(const pcl::PointCloud<pcl::PointXYZ>& full_cloud,const pcl::PointCloud<pcl::PointXYZ>& to_extract);
double calcTableHeight(gt_3d_laser::TableInfo table, tf::StampedTransform surfacetobase);
gt_3d_laser::PlaneInfos getAllPlanes(const pcl::PointCloud<Point>& cloud, int ksearch);
gt_3d_laser::PlaneInfos getPlanes(const pcl::PointCloud<Point>& cloud, int max_planes, bool cluster_planes);
gt_3d_laser::PlaneInfo getPlane(const pcl::PointCloud<Point>& cloud);
void drawPlaneMarkers(const gt_3d_laser::PlaneInfos& planes, const ros::Publisher& plane_pub, float r=0.0f, float g=1.0f, float b=0.0f);
gt_3d_laser::ObjectInfos getObjectsOverPlane(const gt_3d_laser::PlaneInfo& plane, const pcl::PointCloud<Point>& cloud);
void drawObjectMarkers(const gt_3d_laser::ObjectInfos& objects, const ros::Publisher& object_pub);
bool polygonOverlap(const pcl::PointCloud<Point>& poly1, const pcl::PointCloud<Point>& poly2);
gt_3d_laser::PlaneInfos mergePlaneSegments(const gt_3d_laser::PlaneInfos& planes);
bool lineSegIntersection(Point p11, Point p12, Point p21, Point p22);
gt_3d_laser::ObjectInfos mergeObjectsVertically(const gt_3d_laser::ObjectInfos& curr_objects, const gt_3d_laser::PlaneInfo& ground_plane);
geometry_msgs::Point32 calcCentroid(const pcl::PointCloud<pcl::PointXYZ>& cloud);
gt_3d_laser::ObjectInfo mergeObjectInfos(const gt_3d_laser::ObjectInfo& o1, const gt_3d_laser::ObjectInfo& o2);
bool cloudHasOverlap(pcl::PointCloud<pcl::PointXYZ> pc1,
		     pcl::PointCloud<pcl::PointXYZ> pc2);
gt_3d_laser::ObjectInfos mergeObjectsVerticallyFullCloud(const gt_3d_laser::ObjectInfos& curr_objects, const gt_3d_laser::PlaneInfo& ground_plane);
void drawObjectPrisms(const gt_3d_laser::ObjectInfos& objects, const ros::Publisher& object_pub, const gt_3d_laser::PlaneInfo& plane, float r=0.0f, float g=0.0f, float b=1.0f);
gt_3d_laser::PlaneInfo mergePlaneInfos(const gt_3d_laser::PlaneInfo& p1, const gt_3d_laser::PlaneInfo& p2);
