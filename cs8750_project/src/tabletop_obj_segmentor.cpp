 //TODO: make this templated on point type, so we can do XYZ or XYZRGB, etc
//TODO: break more of it out into the library functions
//TODO: make msgs simpler, build struct containers to handle rest internally

#include <ros/ros.h>
#include "/home/akansel/CogROS/trunk/gt_3d_laser/include/gtpointcloud/pointcloud_helpers.h"

#include "pcl_ros/filters/statistical_outlier_removal.h"
#include <pcl_ros/filters/passthrough.h>
#include <pcl_ros/filters/voxel_grid.h>
#include "pcl_ros/filters/project_inliers.h"
#include "pcl_ros/surface/convex_hull.h"
#include "pcl_ros/segmentation/extract_polygonal_prism_data.h"
#include <visualization_msgs/Marker.h>


class TabletopObjSegmentor
{
  typedef pcl::PointXYZ Point;

private:

  bool detect_objects;
  bool detect_ground_plane;
  bool filter_outliers;
  bool downsample_cloud;
  int filter_outliers_meank;
  double filter_outliers_stddev_thresh;
  double cluster_tolerance;
  int min_cluster_size;
  double ptu_tilt_correction;

  bool filter_spatial;
  double filter_spatial_z;

  bool perform_coarse_clustering_;

public:
  ros::NodeHandle n_;

  ros::Subscriber pc2_sub_;

  std::vector<ros::Publisher> obj_pubs_;
  int num_obj_pubs;

  ros::Publisher obj_marker_pub_;
  ros::Publisher obj_marker_pub2_;

  ros::Publisher plane_marker_pub_;
  ros::Publisher planes_pub_;
  std::vector<ros::Publisher> plane_pubs_;
  int num_plane_pubs;

  ros::Publisher cluster_cloud_pub_;
  ros::Publisher filtered_cloud_pub_;

  ros::Publisher plane1_pub_;
  ros::Publisher plane2_pub_;

  ros::Publisher object_pub_;

  TabletopObjSegmentor():
    n_("~")
  {
    n_.param("filter_outliers",filter_outliers,false);
    n_.param("filter_outliers_meank",filter_outliers_meank,50);
    n_.param("filter_outliers_stddev_thresh", filter_outliers_stddev_thresh,1.0);

    n_.param("filter_spatial",filter_spatial,true);
    n_.param("filter_spatial_z",filter_spatial_z,3.0);

    n_.param("perform_coarse_clustering",perform_coarse_clustering_,false);

    n_.param("detect_ground_plane",detect_ground_plane,true);
    n_.param("detect_objects",detect_objects,true);
    n_.param("ptu_tilt_correction",ptu_tilt_correction,-0.34906585);
    n_.param("cluster_tolerance",cluster_tolerance,0.10);
    n_.param("min_cluster_size",min_cluster_size,100);
    
    n_.param("downsample_cloud",downsample_cloud,false);

      pc2_sub_ = n_.subscribe("/camera/depth/points",1,&TabletopObjSegmentor::CloudCallback, this);


    //Make some cloud publishers
    num_obj_pubs = 20;
    for(int i = 0; i < num_obj_pubs; i++){
        char pubname[1024];
        sprintf(pubname,"obj%d",i);
        ros::Publisher obj_pub = n_.advertise<sensor_msgs::PointCloud2>(pubname, 1);
        obj_pubs_.push_back(obj_pub);
    }

    //Make some plane publishers
    num_plane_pubs = 20;
    for(int i = 0; i < num_plane_pubs; i++){
      char pubname[1024];
      sprintf(pubname, "plane%d",i);
      ros::Publisher plane_pub = n_.advertise<sensor_msgs::PointCloud2>(pubname, 1);
      plane_pubs_.push_back(plane_pub);
    }
    
    obj_marker_pub_ = n_.advertise<visualization_msgs::Marker>("obj_markers",1);
    obj_marker_pub2_ = n_.advertise<visualization_msgs::Marker>("obj_markers2",1);
    plane_marker_pub_ = n_.advertise<visualization_msgs::Marker>("plane_markers",1);

    cluster_cloud_pub_ = n_.advertise<sensor_msgs::PointCloud2>("scene",1);
    filtered_cloud_pub_ = n_.advertise<sensor_msgs::PointCloud2>("filtered_cloud",1);

    planes_pub_ = n_.advertise<gt_3d_laser::PlaneInfos>("plane_infos",1);
    object_pub_ = n_.advertise<gt_3d_laser::ObjectInfos>("object_infos",1);
    ROS_INFO("Initiated Obj Segmentor");
  }

  void CloudCallback(const sensor_msgs::PointCloud2ConstPtr &msg)
  {
    
    //Pull the cloud out of the message
    pcl::PointCloud<Point> cloud;
    pcl::fromROSMsg(*msg,cloud);
    // cloud=msg->points;

    ROS_INFO("New PT_CLOUD with %d points",cloud.points.size());


    filter_spatial=true;
    //Filter Spatially
    if(filter_spatial)
      {
      pcl::PassThrough<Point> pass;
      pass.setInputCloud(boost::make_shared<pcl::PointCloud<Point> >(cloud));
      pass.setFilterFieldName("z");
      pass.setFilterLimits(0.0,filter_spatial_z);
      //pass.filter(cloud);
    }
    

    //Filter Outliers
    filter_outliers=false;
    if(filter_outliers){
        pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor_filter;
        sor_filter.setInputCloud(boost::make_shared<pcl::PointCloud<Point> >(cloud));
        sor_filter.setMeanK(20);//50
        sor_filter.setStddevMulThresh(1.0);
        sor_filter.filter(cloud);
    }
    
    downsample_cloud=false;
    if(downsample_cloud){
      //downsample
      pcl::VoxelGrid<Point> grid;
      grid.setInputCloud(boost::make_shared<pcl::PointCloud<Point> >(cloud));
      grid.setLeafSize(0.01,0.01,0.01);//0.05
      //grid.setFilterName("z");
      //grid.setFilterLimits(-1.20,2.0);
      grid.filter(cloud);
    }

    


    /* 
    //Get the planes
    ROS_INFO("Getting planes");
    gt_3d_laser::PlaneInfos planes = getPlanes(cloud,2,true);
    drawPlaneMarkers(planes,plane_marker_pub_);    
    planes_pub_.publish(planes);
    
    //Get objects
    
    if(detect_objects)
      {
	for(int i = 0; i < planes.planes.size(); i++)
	  {
	    gt_3d_laser::ObjectInfos objects = getObjectsOverPlane(planes.planes[i],cloud);
	  }
      }
    */
    
    ROS_INFO("Scene Segmentation Completed!");  
      }
    
};


int main (int argc, char** argv)
{
  ros::init(argc, argv, "TabletopObjSegmentorNode");

  TabletopObjSegmentor TOS;
  ros::spin();

  return (0);
}
