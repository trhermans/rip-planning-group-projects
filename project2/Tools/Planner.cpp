/*
 *  Planner.cpp
 *  RST
 *
 *  Created by Jonathan Scholz on 9/28/09.
 *  modified by martin
 *  Updated and fixed for RIP by Martin 
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "Planner.h"

// pointer to the viewer (only needed for showProgress)
#include "../GUI/Viewer.h"
extern Viewer* viewer;	
#include "Matrix.h"
//#include "timeutil.h"

#include "Robot.h"
#include "World.h"

extern World* world; 	// pointer to the real world (needed for showProgress)

#include <vector>
#include <iostream>
#include <sstream>
#include <ctime>

RSTRRT::RSTRRT() {

}

RSTRRT::~RSTRRT() {
	cleanup();
}

void RSTRRT::setWorld(World *w, int rid)
{
	robotID = rid;
	pworld = w;
}

bool RSTRRT::checkCollisions(rstate &s)
{
	/*
	 * This is what we're supposed be doing, but it doesn't work yet
	 * despite all the stuff I already fixed...
	 */
	//plannerWorld->robots[robotID]->setConf(s);
	//plannerWorld->updateRobot(plannerWorld->robots[robotID]);
	//return !plannerWorld->checkCollisions();

	world->robots[robotID]->setConf(s);
	world->updateRobot(world->robots[robotID]);
	// viewer->UpdateCamera();
	return !world->checkCollisions();
}

Planner::Planner(World* w, rstate start, rstate goal, int whichRobot, bool beGreedy, bool useConnect, bool showProg, int rrt_style)
{
 	pworld = new World(*w);
 	probot = pworld->robots[whichRobot];

 	rrtStyle = rrt_style;
 	greedyMode = beGreedy;
 	connectMode = useConnect;
 	showProgress = showProg;

 	numLinks = probot->activeLinks.size();//robot->activeLinks.size();

 	startConf = start; // sets the size of the vector to no more than numLinks...
 	goalConf = goal;
 	lowBounds.resize(numLinks);
 	highBounds.resize(numLinks);

 	for(int i=0; i<numLinks; i++){
 		lowBounds[i] = probot->activeLinks[i]->jMin;
 		highBounds[i] = probot->activeLinks[i]->jMax;
 	}

 	solved = false;
}



Planner::~Planner()
{
//	delete pworld; nothing to do here right now
}



void Planner::getBestConf(RSTRRT* rrt)
{
 	cout << "Best configuration: ";
 	for(int i=0; i < numLinks; i++){
 		cout << rrt->bestRstate[i] << " ";
 		//pworld->robots[0]->setConf(rrt->bestRstate); // uncomment to move arm to this config
 	}
 	// pworld->updateRobot(pworld->robots[0],true);
	// UpdateCamera();
 }

void Planner::getCurrentConf(RSTRRT* rrt)
{
 	cout << "Current configuration: ";
 	for(int i=0; i<numLinks; i++){
 		cout << rrt->bestRstate[i] << " ";
 		// pworld->robots[0]->setConf(rrt->configVector.back()); // uncomment to move arm to this config
 	}
 	// pworld->updateRobot(pworld->robots[0],true);
	// UpdateCamera();
}


bool Planner::PlanDouble()
{
	//here implement bi-directioal rrt
	cout<<"Starting Bi-directional RRT"<<endl;
	solved = false;

	RSTRRT* rrtGoal = new RSTRRT();
	rrtGoal->setWorld(pworld, pworld->findRobot(probot->name));
	rrtGoal->initialize(goalConf,startConf, lowBounds, highBounds);

	RSTRRT* rrtStart = new RSTRRT();
	rrtStart->setWorld(pworld, pworld->findRobot(probot->name));
	rrtStart->initialize(startConf,goalConf, lowBounds, highBounds);


	int pass;

	if((rrtStart->connect(goalConf))==true)
	{
		solved=true;
	}
	else
	{




		for(pass = 0; pass < MAX_POINTS; ++pass)
		{

			cout<<" "<<pass;

			if (pass % 100 == 0) //step from the start tree
			{

			}

			if (pass % 2 == 0) //step from the start tree
			{
				rstate& randConfig=rrtStart->getRandomRstate();
				rrtStart->connect(randConfig);
				bool result=rrtGoal->connect(rrtStart->newestnode);
				if(result)
				{
					solved=true;
					//bestRstateIDX
					break;
				}

			}
			else //step from the goal tree
			{
				rstate& randConfig=rrtGoal->getRandomRstate();
				rrtGoal->connect(randConfig);
				bool result=rrtStart->connect(rrtGoal->newestnode);
				if(result)
				{
					solved=true;
					break;
				}
			}
		}


	}
	if(solved)
	{
		cerr << "SOLVED!" << endl;
		// Read tree forward:
		tracePathDouble(rrtStart,rrtGoal,true);
		cout << "Path Length = " << path.size();
		cout << "(" << pass << " iterations, ";
		cout << rrtStart->rstateVector.size()+rrtGoal->rstateVector.size() << " nodes in tree)" << endl;

		delete rrtGoal;
		delete rrtStart;
		return true;

	}
	else
	{
		cout<<"Not Solved!"<<endl;
		delete rrtGoal;
			delete rrtStart;
			return false;
		}


}

bool Planner::PlanSingle()
{
	// Initialize the RRT
	RSTRRT* rrt = new RSTRRT();
	rrt->setWorld(pworld, pworld->findRobot(probot->name));

	/*
	rstate strt(7);
	strt[0]=0.810732;
	strt[1]=-1.58768;
	strt[2]=-0.810732;
	strt[3]=-0.675608;
	strt[4]=0.55738;
	strt[5]=-0.371589;
	strt[6]=0.0;

	rstate goaal(7);
	goaal[0]=-0.506713;
	goaal[1]=-1.48635;
	goaal[2]=-2.17884;
	goaal[3]=0.540485;
	goaal[4]=0.760065;
	goaal[5]=0.135123;
	goaal[6]=0.0;
	rrt->initialize(strt, goaal, lowBounds, highBounds);
*/



	rrt->initialize(startConf, goalConf, lowBounds, highBounds);


	int greediness = GREEDINESS;

	int pass;
	for(pass = 0; pass < MAX_POINTS; ++pass)
	{

		if (greedyMode && (pass % greediness == 0))
		{ //greediness
			if (connectMode)
			{
				bool tempBool=rrt->connect(goalConf);
				if(tempBool==true)
				{
					cout<<"connect algo reached"<<endl;
				}
			}
			else
			{

				if(rrt->stepGreedy(goalConf)==false)
				{
					cout<<"goal greedy false"<<endl;
					int upperLimit;
					if(rrt->rstateVector.size()<10)
					{
					upperLimit=rrt->rstateVector.size();
					}
					else
					{
					upperLimit=10;
					}

					for(int jj=0;jj<upperLimit;jj++)
					{
						if(rrt->stepFromRandomNodeToTarget(goalConf)==true)
						{
							break;
						}
						cout<<"rndm greedy false"<<endl;
					}
				}

			}
		}
		else
		{
			rrt->stepRandom();
		}

		// Check if goal was reached
		if ( rrt->bestSD <= pow(rrt->step_size,2) ) {
			solved = true;
			break;
		}

		// To visualize arm states as they're added:
		if (showProgress && rrt->rstateVector.size() >= 2)
		{
			static double bestSD = DBL_MAX;
			if (rrt->bestSD < bestSD)
			{
				bestSD = rrt->bestSD;
				int worldR = world->findRobot(probot->name);
				world->robots[worldR]->setConf(rrt->bestRstate);
				world->updateRobot(world->robots[worldR]);
				viewer->UpdateCamera();
			}
		}

		if (pass%100 == 0)
		{
			cout << pass << " ";
			getBestConf(rrt);
			// report the distance b/t this config and goal
			cout << " Dist: " << rrt->bestSD << "(tree size = " << rrt->rstateVector.size() << ")" << endl;	
		}
	}


	if(!solved){
		cerr << "OUT OF SPACE" << endl;
		return false;
	}

	cerr << "SOLVED!" << endl;
	// Read tree forward:
	tracePathSingle(rrt);
	cout << "Path Length = " << path.size();
	cout << "(" << pass << " iterations, ";
	cout << rrt->rstateVector.size() << " nodes in tree)" << endl;

	delete rrt;
	return true;
}


void Planner::updateBot(rstate state)
{
	int worldR = world->findRobot(probot->name);
	world->robots[worldR]->setConf(state);
	world->updateRobot(world->robots[worldR]);
	viewer->UpdateCamera();
}




void Planner::tracePathSingle(RSTRRT* rrt)
{
	rrt->tracePath();
	path.clear();
	cout << "Tracing path" << endl;
	for(int i = 0; i < rrt->path.size(); i++)
		path.push_back(rrt->path[i]);
}


void Planner::tracePathDouble(RSTRRT* inittree, RSTRRT* goaltree, bool isinit)
{
	path.clear();

	rstate midnode;

	if (isinit) 
		midnode = inittree->newestnode;
	else
		midnode = goaltree->newestnode;

	inittree->tracePath(inittree->rstateVector[inittree->getNearestNeighbor(midnode)]);
	goaltree->tracePath(goaltree->rstateVector[goaltree->getNearestNeighbor(midnode)]);


	for(int i = 0; i < inittree->path.size(); i++)
		path.push_back(inittree->path[i]);
	

	for(int i = goaltree->path.size() - 1; i >= 0; i--)
		path.push_back(goaltree->path[i]);


}

bool Planner::Plan()
{
	clock_t begintime = clock();
	bool result;
	if (rrtStyle == 0)
		result = PlanSingle();
	else
		result = PlanDouble();

	clock_t endtime = clock();
	clock_t timetaken = endtime - begintime;
	cout << "Time taken: " << timetaken << " clock cycles, " << timetaken / CLOCKS_PER_SEC << " seconds" << endl;

	return result;
}
