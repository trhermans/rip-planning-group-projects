\documentclass[10pt]{hermans-hw}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{amsmath}
\usepackage{amssymb} % For set naming (i.e. R^n)

\author{Akansel Cosgun, Victor Emeli, Tucker Hermans}
\title{Project 2: Motion Planning}
\duedate{1 November 2010}
\workedWith{}
\class{Computer Science 8803-RIP}
\institute{Georgia Institute of Technology}
\setcounter{section}{2}

\begin{document}
\maketitle
\section{Project Part I: Rapidly Exploring Random Trees}

%\subsection{Goal-Biased RRT}
%\subsection{Bi-Directional RRT}
%\subsection{Questions}
\subsection*{Questions}
\begin{enumerate}
\item We performed this evaluation looking at finding a path between three different start and goal configurations of varying difficulty in the same environment. Figures~\ref{fig:part1-startgoal} shows the tested configurations. Unless otherwise stated, the planning parameters were as follows: \(50,000\) \texttt{MAX\_POINTS}, \(5\) \texttt{GREEDINESS}, \(0.5\) \texttt{STEP\_SIZE}. Since given runs of the same planner produce different results, we compare the results of five trials of each planner on the given tasks. We give the start and goal configurations for the three example tasks in degrees, for ease of repeatability.

\textbf{Task 1:} DESKTOP-ARM-WITH-HAND:
\begin{itemize}
\item \textbf{Start Config:} [0, -58.0, 0, -62.0, 0, -32.0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
\item \textbf{ Goal Config:} [150.0, -88.0, 0, -4.0, 0, 80.0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
\end{itemize}

\textbf{Task 2:} DESKTOP-ARM-WITH-HAND:
\begin{itemize}
\item \textbf{Start Config:} [0, -106.0, -46.07, 53.51, 0, 0, , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
\item \textbf{ Goal Config:} [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
\end{itemize}

\textbf{Task 3:} DESKTOP-ARM-WITH-HAND:
\begin{itemize}
\item \textbf{Start Config:} [42.27, -120.0, -70.77, 78.84, 86.91, 117.47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
\item \textbf{ Goal Config:} [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -90.0, 90.0, 90.0, 90.0, -90.0, -90.0]
\end{itemize}

\begin{figure}[ht!]
  \centering
  \subfloat[Task 1 Start Configuration]{\includegraphics[width=0.45\textwidth]{./config1/start-config}}
  \subfloat[Task 1 Goal Configuration]{\includegraphics[width=0.45\textwidth]{./config1/goal-config}} \\
  \vspace{2pt}
  \subfloat[Task 2 Start 
Configuration]{\includegraphics[width=0.45\textwidth]{./config2/start-config}}
  \subfloat[Task 2 Goal Configuration]{\includegraphics[width=0.45\textwidth]{./config2/goal-config}} \\
  \vspace{2pt}
  \subfloat[Task 3 Start 
Configuration]{\includegraphics[width=0.45\textwidth]{./config3/start-config}}
  \subfloat[Task 3 Goal Configuration]{\includegraphics[width=0.45\textwidth]{./config3/goal-config}} \\
  \caption{Start and goal configurations for comparing greedy and bi-directional RRT.\label{fig:part1-startgoal}}
\end{figure}

\item The bi-directional search planner produced faster results more reliably than the greedy method. For many situations the goal-biased planner became stuck in local maxima, near obstacles, very quickly, unable to produce a valid plan.  For example Table~\ref{table:greedy-env1} shows that for the first task the greedy approach fails to produce a valid result in any of the five attempts with the standard parameters. In comparison to this, the bi-directional RRT produces five distinctly different plans with computation time varying between 1 and 10 seconds. Increasing the value of \texttt{MAX\_POINTS} and the maximum number of nodes allowed in the search tree to \(100,000\) and \(200,000\) respectively, did not create a valid solution. We can attribute the success of the bi-directional search to the fact, that if the forward growing tree gets stuck in an local optima, it can change the target direction of movement by growing towards new nodes on the frontier of the goal-rooted tree, while in the greedy approach the tree can only jump to random points locally, with no consistent signal driving it away from the optima.

\begin{table}[h!]
  \centering
  \begin{tabular}{|r|c|c|c|c|}
    \hline
    Trial# & Success & Time (s) & Plan Steps & Tree Nodes\\ \hline
    1 & Fail & 134 & - & 50,002 \\
    2 & Fail & 144 & - & 50,002 \\
    3 & Fail & 159 & - & 50,002 \\
    4 & Fail & 136 & - & 50,002 \\
    5 & Fail & 137 & - & 50,002 \\ \hline
  \end{tabular}
  \caption{Results for goal-biased planner on task 1\label{table:greedy-env1}}
\end{table}

\begin{table}[h!]
  \centering
  \begin{tabular}{|r|c|c|c|c|}
    \hline
    Trial# & Success & Time (s) & Plan Steps & Tree Nodes \\ \hline
    1 & Pass & 1 & 418 & 441\\
    2 & Pass & 2 & 827 & 2492\\
    3 & Pass & 8 & 1010 & 3344 \\
    4 & Pass & 6 & 783  & 3423 \\
    5 & Pass & 10 & 1564 & 4456 \\ \hline
  \end{tabular}
  \caption{Results for bi-directional planner on task 1\label{table:bi-env1}}
\end{table}

Figure~\ref{fig:end-fails} shows the final configuration reached for two failure attempts with the greedy RRT approach. Interestingly, since the planning occurs in joint space, we can create a similar goal configuration in Cartesian space, where we keep the first joint, close to its initial position and instead change the value of the second link.  Using this method a valid path is easily generated by the greedy planner. This augmented goal configuration is [-36.57, 94.04 0 -4.0 0 -75.67 -180.0, 0, 0, 0, 0, 0, 0, 0, 0, 0] and produces a 159 step plan using the greedy RRT in 4 seconds.  For the same configuration using the bi-directional method, we note that the bi-directional search creates a path of length 83 in less than 1 second. The resulting plan can be seen in \texttt{task1-augmented-greedy.mp4}. This evaluation shows the importance of the chosen joint space encoding, when the operator only cares about the final position of the end effector in the Cartesian world.

\begin{figure}
  \centering
  \subfloat{\includegraphics[width=0.45\textwidth]{./config1/fail2-end}}
  \subfloat{\includegraphics[width=0.45\textwidth]{./config1/fail4-end}}
  \caption{Failure end configurations for goal-biased RRT on task 1\label{fig:end-fails}}
\end{figure}
After the difficulty experienced by the greedy approach in finding a valid plan for the first test configurations, we examine as our second task a much simpler act of raising and turning the arm, where no obstacles exist in the straight line path between start and goal configurations.  The quantitative results are shown in Tables~\ref{table:greedy-env2}~and~\ref{table:bi-env2}. Reported times are from an 8 core 2.67 GHz Intel i7 Ubuntu 10.04 machine with 6 GB of RAM.

\begin{table}[h!]
  \centering
  \begin{tabular}{|r|c|c|c|c|}
    \hline
    Trial# & Success & Time (s) & Plan Steps & Tree Nodes\\ \hline
    1 & Pass & 69 & \(< 1\) & 192 \\
    2 & Pass & 63 & \(< 1\) & 192 \\
    3 & Pass & 66 & \(< 1\) & 192 \\
    4 & Pass & 68 & \(< 1\) & 182 \\
    5 & Pass & 73 & \(< 1\) & 187 \\ \hline
  \end{tabular}
  \caption{Results for goal-biased planner on task 2\label{table:greedy-env2}}
\end{table}

\begin{table}[h!]
  \centering
  \begin{tabular}{|r|c|c|c|c|}
    \hline
    Trial# & Success & Time (s) & Plan Steps & Tree Nodes \\ \hline
    1 & Pass & 41 & \(< 1\) & 43\\
    2 & Pass & 41 & \(< 1\) & 43 \\
    3 & Pass & 41 & \(< 1\) & 43 \\
    4 & Pass & 41 & \(< 1\) & 43 \\
    5 & Pass & 41 & \(< 1\) & 43 \\ \hline
  \end{tabular}
  \caption{Results for bi-directional planner on task 2\label{table:bi-env2}}
\end{table}

We see in these results, that while both planners give valid plans in under a second, the bi-directional planner produces shorter plans (\(\approx 60\%\) of the goal-biased length).  We also note that when no obstacles are present between the start and goal configurations, the returned plans by the bi-directional planner are consistently the same, since there is no need to explore configurations expect for those on the straight line path between the start and goal configurations. This artifact of the bi-directional planner, is another advantage with respect to the goal-biased approach, since the only parameter that matters in determining the path length is the step size used in expanding the search tree.

Our final example task has the robot arm unfold to a vertical position while opening the hand. While both planners produce valid plans on all trials, the bi-directional planner produces shorter, identical paths, as was observed in task 2. Similar to task 2 the shorter paths were approximately 61\% of the length of the greedy method.

\begin{table}[h!]
  \centering
  \begin{tabular}{|r|c|c|c|c|}
    \hline
    Trial# & Success & Time (s) & Plan Steps & Tree Nodes\\ \hline
    1 & Pass & 172 & \(< 1\) & 437 \\
    2 & Pass & 175 & 1 & 427 \\
    3 & Pass & 170 & 1 & 447 \\
    4 & Pass & 168 & 1 & 457 \\
    5 & Pass & 170 & \(< 1\) & 437 \\ \hline
  \end{tabular}
  \caption{Results for goal-biased planner on task 3\label{table:greedy-env3}}
\end{table}

\begin{table}[h!]
  \centering
  \begin{tabular}{|r|c|c|c|c|}
    \hline
    Trial# & Success & Time (s) & Plan Steps & Tree Nodes \\ \hline
    1 & Pass & 105 & \(< 1\) & 107 \\
    2 & Pass & 105 & \(< 1\) & 107 \\
    3 & Pass & 105 & \(< 1\) & 107 \\
    4 & Pass & 105 & \(< 1\) & 107 \\
    5 & Pass & 105 & \(< 1\) & 107 \\ \hline
  \end{tabular}
  \caption{Results for bi-directional planner on task 3\label{table:bi-env3}}
\end{table}

\item We claim that a better plan is one, which produces a shorter overall path from start to goal configuration. In all trials across all of our example tasks the bi-directional RRT planner gave shorter paths.  In addition to this, the paths given had fewer jerky motions, compared to those of the greedy method, see movies \texttt{task3-greedy.mp4} and \texttt{task3-bi.mp4} for a good example of this jerky motion. Beyond this, the bi-directional planner was consistent in finding solutions, where the greedy approach would fail, thus even if the plans produced from the greedy approach were better, it would still be advantageous to deploy the bi-directional method.

\item We have attached two movies for each task, representing an example run for the goal-biased (\texttt{task?-greedy.mp4}) and bi-directional (\texttt{task?-bi.mp4}) RRT planners.

\end{enumerate}

\section{Project Part II: Broader Goals in Motion Planning}
As broader goals we chose to take on the tasks of manipulation and integration of semantic planning into the motion planning system. The details of both approaches are described below.

\subsection{Manipulation}

\setlength{\parindent}{0pt}
\setlength{\parskip}{2ex}   We implemented a function for pick and place tasks, which takes object name, start pose and goal pose as input. 

\setlength{\parindent}{0pt}
\setlength{\parskip}{2ex}   The function selects a possible grasp from the list, first runs IK for that grasp position. Then, it runs the bi-directional RRT to plan from the current robot configuration to the grasping position. If it succeeds, that means we can reach to the object. Then it determines the transformation of the final end effector frame in robot base frame, by taking the current grasp pose into account. The IK is run for the feasibility of this end effector pose. Then the bi-directional RRT is run again to plan from the grasp pose to the goal pose. If all those succeeds, we have a valid plan.

\setlength{\parindent}{0pt}
\setlength{\parskip}{2ex} This paragraph describes how the IK library is used. The start/goal poses for the objects are defined as $T_{bookStart}^{0}$ and $T_{bookGoal}^{0}$, as transformations from the world frame to objects' frames. For a given object, a set of possible grasps are defined. For the book object, we defined 3 grasps per surface so 18 in total.) The grasps are hard coded as transformations from object frame to end effector frame, i.e $T_{eeStart}^{bookStart}$. Since every object needed special hardcoding for possible grasps, we only used the book obejct to manipulate. To use the Inverse Kinematics(IK) function, we need $T_{eeStart}^{base}$. We assume the robot base is not mobile so the robot base frame is fixed, therefore $T_{base}^{0}$ is fixed and can be fetched from the simulation. $T_{eeStart}^{base}={(T_{base}^{0})}^{-1}T_{bookStart}^{0}T_{eeStart}^{base}$. However, it turned out to be that the IK function of the simulator doesn't use the 'base' frame. It is using another frame at the same point, with different rotation. This frame and the end effector frame turned out to be left-handed!. Therefore it needed special attention and calculations other than the transformation above.

When the object is reached, it is moved to the infinity(i.e. somewhere far in the simulation), then a new link is added to the robot with the proper joint angle and offset. When the placing position is reached, the last link is deleted and the object is moved back to the goal position. However, our implementation lacks one feature: when carrying the object, we ignored the collisions of the object with the environment. This is because, we ran the RRT planners without the object as a link, RRT planners were run several times for different grasp poses. So it was going to be hard to add/remove a new link to the robot everytime a grasp pose is tried. Therefore in the videos, the carried object, but not the robot links are subject to colliding with the obstacles around. We also didn't want to lose time by actually adding the robot hand. The objects seem like flying, but solving the fundamental problems were of more importance.

\subsection{Semantic}
We were interested in applying semantic reasoning to our above described manipulation abilities.  As such, we defined methods for encoding the semantic relations of: \texttt{on\_top\_of}, \texttt{left\_of}, and \texttt{right\_of} into our geometric representations. These are computed by translating the position of the manipulated object to a relative position around the destination object, no matter where the destination object is within the robot manipulators workspace. This relative position is dictated by the semantic value of the command and is encoded a unique transform for each value. Specifically, a transformation matrix $T_{obj1}^{obj2}$ is defined for  \texttt{left\_of}, and \texttt{right\_of} words, which is hard-coded, just as how the grasping poses are defined. Therefore, when a semantic command "left of obj1" is given, $T_{obj2}^{0}=T_{obj1}^{0}T_{obj2}^{obj1}$ is calculated and the desired goal pose in the world frame is fed to the grasp planner. \texttt{on\_top\_of} command is reasoned in a different manner by adding the +z value in the world frame, not in the frame relative to the object that we are putting something on.

We introduce a constant book, which is the base object and a manipulated book, which is the object that is move based on the semantic command.  The constant book is dynamically moved to any position within the workspace of the robot manipulator.  The manipulated book is also dynamically moved.  We then chose either the Left Of, Right Of, or the On Top command and the planner solves a path that places the manipulated book in the desired location, relative to the constant book.  This capability allows for dynamic complex geometric information to be encoded in a simple semantic command.  

We recorded six videos Top1.mpeg4, Top2.mpeg4, Left1.mpeg4, Left2.mpeg4, Right1.mpeg4, and Right2.mpeg4 which illustrate the execution of these semantic commands in tough geometric configurations with multiple obstacles.

% \subsection{Path Shortening}
\section*{Group Member Participation}
All members discussed the implemented approaches and analysis thereof. Additionally all members produced solutions to the first part in order to become familiar with RST.
\begin{itemize}
\item \textbf{Akansel:} Focused on implementation of Manipulation.
\item \textbf{Tucker:} Focused on evaluation of greedy vs. bi-directional RRT and composition of this document.
\item \textbf{Victor:} Focused on implementation of semantic planning, helped with manipulation implementation.
\end{itemize}

\end{document}
