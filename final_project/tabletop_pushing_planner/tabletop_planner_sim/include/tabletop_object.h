#include <opencv2/core/core.hpp>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <vector>

class TabletopObject
{
 public:
  TabletopObject() : x_size(0), y_size(0),
                     is_oval(false), is_pushable(true),
                     disp_color(0,0,0)
  { }
  int x_size;
  int y_size;
  bool is_oval;
  bool is_pushable;
  cv::Scalar disp_color;
  geometry_msgs::Pose2D pose;
  std::vector<geometry_msgs::Point> vertices;
};
