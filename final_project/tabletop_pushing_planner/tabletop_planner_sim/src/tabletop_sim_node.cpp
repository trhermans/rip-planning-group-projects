// ROS
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// STL
#include <time.h> // for srand(time(NULL))
#include <cstdlib> // for MAX_RAND
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

// Ours
#include "tabletop_object.h"
#include "tabletop_planner_sim/TabletopEnvironment.h"
#include "tabletop_planner_sim/PlacementScores.h"
#include "tabletop_planner_sim/PlacementHeuristic.h"

#define WINDOW_NAME "Tabletop Simulator"
#define FORCE_CIRCLES 1
//#define DEBUG_CDF_CALC 1
//#define DISPLAY_BINARY_IMG 1
//#define DISPLAY_FILTER_IMGS 1
//#define DISPLAY_FILTER_SCORES 1
//#define DISPLAY_TABLETOP 1
//#define DISPLAY_BEST_PLACEMENT 1

using std::vector;
using tabletop_planner_sim::TabletopEnvironment;
using tabletop_planner_sim::PlacementScores;
using tabletop_planner_sim::PlacementHeuristic;

typedef tabletop_planner_sim::TabletopObject ROSTabletopObject;
//typedef std::pair<geometry_msgs::Pose2D, float> Mode;

class Mode
{
 public:
  geometry_msgs::Pose2D pose;
  double weight;
  double cdf_weight;
};

typedef std::vector<Mode> SampleList;

class TabletopSimNode
{
 public:
  TabletopSimNode(ros::NodeHandle &n) :
      n_(n), table_color_(0, 102, 151), padding_color_(0, 0, 0),
      use_binary_padding_(false)
  {
    ros::NodeHandle n_private("~");
    n_private.param("x_padding", x_padding_, 20);
    n_private.param("y_padding", y_padding_, 20);
    n_private.param("table_x", table_x_, 400);
    n_private.param("table_y", table_y_, 400);
    n_private.param("oval_prob", oval_prob_, 0.0);
    n_private.param("pushable_prob", pushable_prob_, 0.3);
    n_private.param("max_score_percent", max_score_thresh_percent_, 0.8);
    n_private.param("only_use_modes", only_use_modes_, true);
    n_private.param("num_test_orientations", num_test_orientations_, 1);
    n_private.param("test_x", test_x_, 100);
    n_private.param("test_y", test_y_, 50);
    n_private.param("use_binary_padding", use_binary_padding_, false);
    // Setup service server
    placement_srv_ = n_.advertiseService("placement_candidates",
                                         &TabletopSimNode::tabletopEnvCallback,
                                         this);
  }

  /**
   * Here is the service method to determin candidate poses, given the current
   * tabletop arrangement.
   *
   * @param req The ROS service request
   * @param res The ROS service response
   *
   * @return false if computation failed, true otherwise
   */
  bool tabletopEnvCallback(PlacementHeuristic::Request &req,
                           PlacementHeuristic::Response &res)
  {
    // Set tabletop information from the request
    cv::Mat bin_img = drawTabletop(req.te);

    // Perform convolution to get candidate poses
    TabletopObject to_place;
    to_place.x_size = req.to_place.x_size;
    to_place.y_size = req.to_place.y_size;
    to_place.is_oval = req.to_place.is_oval;
    to_place.vertices = req.to_place.vertices;

    res = findPlacement(to_place, bin_img, num_test_orientations_,
                        use_binary_padding_);

#ifdef DISPLAY_BEST_PLACEMENT

    to_place.pose = res.ps.poses[0];
    const int n = to_place.vertices.size();
    cv::Point poly[n];
    for (int i = 0; i < n; ++i)
    {
      cv::Point vert;
      vert.x =  to_place.vertices[i].x;
      vert.y =  to_place.vertices[i].y;
      vert = rotateVector(vert, to_place.pose.theta);
      poly[i] = vert;
    }

    float min_x = 10000.0;
    float max_x = 0.0;
    float min_y = 10000.0;
    float max_y = 0.0;
    for (int i = 0; i < n; ++i)
    {
      const float cur_x = poly[i].x;
      if( cur_x > max_x)
        max_x = cur_x;
      if( cur_x < min_x)
        min_x = cur_x;
      const float cur_y = poly[i].y;
      if( cur_y > max_y)
        max_y = cur_y;
      if( cur_y < min_y)
        min_y = cur_y;
    }

    int x_range = 0;
    int y_range = 0;
    // Offset the point locations based on the min and max values
    for (int i = 0; i < n; ++i)
    {
      poly[i].x -= min_x;
      poly[i].y -= min_y;
      if (poly[i].x > x_range) x_range = poly[i].x;
      if (poly[i].y > y_range) y_range = poly[i].y;
    }

    std::cout << "Orientation is: "
              << to_place.pose.theta*num_test_orientations_/M_PI
              << std::endl;
    std::cout << "Position is: (" << to_place.pose.x << ", " << to_place.pose.y
              << ")" << std::endl;
    for (int i = 0; i < n; ++i)
    {
      std::cout << "poly[" << i << "]: (" << poly[i].x << ", " << poly[i].y
                << ")" << std::endl;
    }
    for (int i = 0; i < n; ++i)
    {
      poly[i].x = poly[i].x + to_place.pose.x - x_range/2;
      poly[i].y =  (poly[i].y + to_place.pose.y - y_range/2);
    }
    cv::fillConvexPoly(bin_img, poly, n, cv::Scalar(0.5,0.5,0.5));
    cv::Mat disp_bin_img(bin_img.size(), bin_img.type());
    cv::flip(bin_img, disp_bin_img, 0);
    cv::imshow("Best Placement", disp_bin_img);
    cv::waitKey();
#endif // DISPLAY_BEST_PLACEMENT
    return true;
  }


  /**
   * Method to initialize the components necessary to display the tabletop.
   */
  void initializeWindow()
  {
    cv::namedWindow(WINDOW_NAME);
#ifdef DISPLAY_BINARY_IMG
    cv::namedWindow("Binary Table");
#endif // DISPLAY_BINARY_IMG
    for (int i = 0; i < num_init_objects_; ++i)
    {
      TabletopObject obj = generateRandObject();
      objects_.push_back(obj);
    }
    cv::Mat bin_img = drawTabletop();
  }


  /**
   * Draw the table given a description of the environment
   *
   * @param te The environment to be set.
   *
   * @return A binary image of the tabletop
   */
  cv::Mat drawTabletop(TabletopEnvironment te)
  {
    // Set table to use the new te information
    table_x_ = te.table.x_size;
    table_y_ = te.table.y_size;

    objects_.clear();

    for (unsigned int i = 0; i < te.objects.size(); ++i)
    {
      TabletopObject to;
      to.pose = te.objects[i].pose;
      to.x_size = te.objects[i].x_size;
      to.y_size = te.objects[i].y_size;
      to.is_oval = te.objects[i].is_oval;
      to.is_pushable = te.objects[i].is_pushable;
      if (te.objects[i].vertices.size() > 0)
      {
        to.vertices = te.objects[i].vertices;
      }
      else
      {
        const int N = 4;
        cv::Point points[N];
        getRectangleCorners(objects_[i], points);
        for (int j = 0; j < N; ++j)
        {
          cv::Point cur_point = worldPointToImagePoint(points[j]);
          geometry_msgs::Point p;
          p.x = cur_point.x;
          p.y = cur_point.y;
          to.vertices.push_back(p);
        }
      }

      cv::Scalar rand_color;
      rand_color[0] = rand() % 255;
      rand_color[1] = rand() % 255;
      rand_color[2] = rand() % 255;
      to.disp_color = rand_color;
      objects_.push_back(to);
    }

    return drawTabletop();
  }

  /**
   * Method to create the image of the tabletop to be displayed in simulation.
   */
  cv::Mat drawTabletop()
  {
    // Create the display_image with the uniform background color
    cv::Mat disp_img(table_y_+2*y_padding_,
                     table_x_+2*x_padding_, CV_8UC3, padding_color_);

    // Add the table
    cv::Point table_pt1 = worldPointToImagePoint(cv::Point(0,0));
    cv::Point table_pt2 = worldPointToImagePoint(cv::Point(table_x_, table_y_));
    cv::rectangle(disp_img, table_pt1, table_pt2, table_color_, CV_FILLED);

    // Draw the objects
    // NOTE: Currently does not care about collisions / overlaps
    for(unsigned int i = 0; i < objects_.size(); ++i)
    {
      cv::Point world_center(objects_[i].pose.x, objects_[i].pose.y);
      cv::Point img_center = worldPointToImagePoint(world_center);
      if(objects_[i].is_oval)
      {
        // TODO: Check that the transformations here are done correctly.
        // NOTE: Currently doesn't matter since we are forcing circles.
        cv::Size axes(objects_[i].x_size/2, objects_[i].y_size/2);
        cv::ellipse(disp_img, img_center, axes,
                    objects_[i].pose.theta*180.0/M_PI, 0.0, 360.0,
                    objects_[i].disp_color, CV_FILLED);
      }
      else
      {
        const int N = objects_[i].vertices.size();
        cv::Point points[N];
        int min_x = 1000;
        int min_y = 1000;
        int max_x = 0;
        int max_y = 0;
        for (int j = 0; j < N; ++j)
        {
          points[j].x = objects_[i].vertices[j].x;
          points[j].y = objects_[i].vertices[j].y;
          points[j] = rotateVector(points[j], objects_[i].pose.theta);
          if( points[j].x < min_x) min_x = points[j].x;
          if( points[j].x > max_x) max_x = points[j].x;
          if( points[j].y < min_y) min_y = points[j].y;
          if( points[j].y > max_y) max_y = points[j].y;
        }

        int x_range = 0;
        int y_range = 0;
        for (int j = 0; j < N; ++j)
        {
          points[j].x -= min_x;
          points[j].y -= min_y;
          if (points[j].x > x_range) x_range = points[j].x;
          if (points[j].y > y_range) y_range = points[j].y;

        }
        for (int j = 0; j < N; ++j)
        {
          points[j].x = points[j].x + objects_[i].pose.x - x_range/2;
          points[j].y = points[j].y + objects_[i].pose.y - y_range/2;
          points[j] = worldPointToImagePoint(points[j]);
        }
        cv::fillConvexPoly(disp_img, points, N, objects_[i].disp_color);
      }
    }

    cv::Mat bin_img = getBinaryTabletopImage(disp_img, use_binary_padding_);

    // Update the open cv display window
    cv::Mat disp_img_flip(disp_img.size(), disp_img.type());
    cv::flip(disp_img, disp_img_flip, 0);

#ifdef DISPLAY_TABLETOP
    cv::imshow(WINDOW_NAME, disp_img_flip);
#ifdef DISPLAY_BINARY_IMG
    cv::Mat disp_bin_img(bin_img.size(), bin_img.type());
    cv::flip(bin_img, disp_bin_img, 0);
    cv::imshow("Binary Table", disp_bin_img);
#endif // DISPLAY_BINARY_IMG
    char c = cv::waitKey(3);
#endif // DISPLAY_TABLETOP
    return bin_img;
  }

  /**
   * Exectuive control method.
   */
  void spin()
  {
    while(n_.ok())
    {
      ros::spinOnce();
    }
  }

  /**
   * Simple comparison method used for STL sort
   *
   * @param a The first item
   * @param b The second item
   *
   * @return true if a has a lower score than b, false otherwise
   */
  static bool compareModes(Mode a, Mode b)
  {
    return a.weight > b.weight;
  }

 protected:
  PlacementHeuristic::Response findPlacement(TabletopObject to_place,
                                             cv::Mat& bin_img,
                                             int num_orientations = 1,
                                             bool use_padding = false)
  {
    int x_padding = 0;
    int y_padding = 0;
    PlacementHeuristic::Response res;

    if (use_padding)
    {
      x_padding = x_padding_;
      y_padding = y_padding_;
    }

    to_place.pose.x = 0;
    to_place.pose.y = 0;

    double free_space_count = 0;
    for (int r = y_padding; r < y_padding + bin_img.rows; ++r)
    {
      for (int c = x_padding; c < x_padding + bin_img.cols; ++c)
      {
        if ( bin_img.at<float>(r,c) == 1.0)
          ++free_space_count;
      }
    }

    SampleList sample_list;
    double mode_sum = 0;
    double max_sum = 0;
    // Perform convolution along multiple orientations
    for (int o = 0; o < num_orientations; ++o)
    {
      // Determine the current pose
      to_place.pose.theta = M_PI / num_orientations * o;

      const int n = to_place.vertices.size();
      cv::Point poly[n];
      for (int i = 0; i < n; ++i)
      {
        cv::Point vert;
        vert.x =  to_place.vertices[i].x;
        vert.y =  to_place.vertices[i].y;
        vert = rotateVector(vert, to_place.pose.theta);
        poly[i] = vert;
      }

      float min_x = 10000.0;
      float max_x = 0.0;
      float min_y = 10000.0;
      float max_y = 0.0;
      for (int i = 0; i < n; ++i)
      {
        const float cur_x = poly[i].x;
        if( cur_x > max_x)
          max_x = cur_x;
        if( cur_x < min_x)
          min_x = cur_x;
        const float cur_y = poly[i].y;
        if( cur_y > max_y)
          max_y = cur_y;
        if( cur_y < min_y)
          min_y = cur_y;
      }

      float x_range = 0;
      float y_range = 0;
      // Offset the point locations based on the min and max values
      for (int i = 0; i < n; ++i)
      {
        poly[i].x -= min_x;
        poly[i].y -= min_y;
        if (poly[i].x > x_range) x_range = poly[i].x;
        if (poly[i].y > y_range) y_range = poly[i].y;
      }

      cv::Mat kernel(y_range, x_range, bin_img.type(), 0.0);
      cv::fillConvexPoly(kernel, poly, n, 1.0);

      // Count the number of white pixels for gathering our statistic
      if (o == 0)
      {
        float object_pixel_count = 0;
        for(int r = 0; r < y_range; ++r)
        {
          for(int c = 0; c < x_range; ++c)
          {
            if(kernel.at<float>(r,c) == 1.0)
              ++object_pixel_count;
          }
        }
        float total_pixels = bin_img.rows*bin_img.cols;
        res.object_free_space = object_pixel_count / free_space_count *100;
        res.clutter_percent = (1-free_space_count/total_pixels);
        //std::cout << "Total surface free is: " << free_space_count << std::endl;
        //std::cout << "Total object count is: " << object_pixel_count
        //          << std::endl;
        //std::cout << "Total number of pixels is: " << total_pixels << std::endl;
        // std::cout << "Object/Free Space %: " << res.object_free_space << std::endl;
        // std::cout << "Clutter %: " << res.clutter_percent << std::endl;
      }

      cv::Mat result(bin_img.size(), bin_img.type());
      cv::filter2D(bin_img, result, -1, kernel, cv::Point(-1,-1), 0,
                   cv::BORDER_CONSTANT);

      // Find global maximum and perform non-maximal suppresion
      double max_val = 0;
      for (int r = y_padding; r < y_padding + result.rows; ++r)
      {
        for (int c = x_padding; c < x_padding + result.cols; ++c)
        {
          if (r < y_padding + y_range/2 || c < x_padding + x_range/2 ||
              r > y_padding + result.rows - y_range/2 ||
              c > x_padding + result.cols - x_range/2 )
            continue;

          float val = result.at<float>(r,c);
          // Check global maximum
          if( val > max_val)
            max_val = val;
        }
      }
      SampleList modes;
      SampleList maxes;

      for (int r = y_padding; r < result.rows + y_padding; ++r)
      {
        for (int c = x_padding; c < result.cols + x_padding; ++c)
        {
          float val = result.at<float>(r,c);
          if (r < y_padding + y_range/2 || c < x_padding + x_range/2 ||
              r > y_padding + result.rows - y_range/2 ||
              c > x_padding + result.cols - x_range/2 )
            continue;
          if (val <  max_score_thresh_percent_*max_val)
            continue;
          Mode m;
          geometry_msgs::Pose2D pose;
          pose.x = c - x_padding;
          pose.y =  (r - y_padding);
          pose.theta = to_place.pose.theta;
          m.pose = pose;
          m.weight = val;
          max_sum += val;

          maxes.push_back(m);
          if (c > 0) // Has left
          {
            if( val < result.at<float>(r,c-1))
              continue;
            if (r > 0 && val < result.at<float>(r - 1, c - 1))
              continue;
            if (r < result.rows - 1 && val < result.at<float>(r + 1, c - 1))
              continue;
          }
          if (c < result.cols - 1) // Has Right
          {
            if( val < result.at<float>(r,c + 1))
              continue;
            if (r > 0 && val < result.at<float>(r - 1, c + 1))
              continue;
            if (r < result.rows - 1 && val < result.at<float>(r + 1, c + 1))
              continue;
          }
          if (r > 0 && val < result.at<float>(r - 1, c)) // Has above
            continue;
          if (r < result.rows - 1 && val < result.at<float>(r + 1, c)) // Has below
            continue;

          // Store the local maxima value
          modes.push_back(m);
          mode_sum += val;
        }
      }

      // Store information for comparisons across orientations
      if (only_use_modes_)
      {
        sample_list.insert(sample_list.end(), modes.begin(), modes.end());
      }
      else
      {
        sample_list.insert(sample_list.end(), maxes.begin(), maxes.end());
      }

#ifdef DISPLAY_FILTER_IMGS
      std::stringstream win_name;
      std::stringstream kern_name;
      win_name << "Convolution result" << o;
      kern_name << "Kernel" << o;
      // Scale the result to display ranges
      if (max_val > 0)
        result /= max_val;
      else
        std::cout << "Max_val is: " << max_val << std::endl;
      cv::Mat col_result(bin_img.size(), CV_8UC1);
      cv::cvtColor(result, col_result, CV_GRAY2RGB);
      cv::Mat disp_col_res(col_result.size(), col_result.type());
      cv::flip(col_result, disp_col_res, 0);
      cv::imshow(win_name.str(), disp_col_res);

#ifdef DISPLAY_FILTER_SCORES
      for (unsigned int i = 0; i < maxes.size() && false; ++i)
      {
        cv::circle(col_result, cv::Point(maxes[i].pose.x+x_padding,
                                         maxes[i].pose.y+y_padding),
                   2, cv::Scalar(0,255,0));
      }
      for (unsigned int i = 0; i < modes.size(); ++i)
      {
        cv::circle(col_result, cv::Point(modes[i].pose.x + x_padding,
                                         modes[i].pose.y + y_padding), 2,
                   cv::Scalar(0,0,255));
      }
#endif // DISPLAY_FILTER_SCORES
      cv::flip(col_result, disp_col_res, 0);
      cv::Mat disp_kernel(kernel.size(), kernel.type());
      cv::flip(kernel, disp_kernel, 0);
      cv::flip(col_result, disp_col_res, 0);
      win_name << "-modes";
      cv::imshow(win_name.str(), disp_col_res);

      cv::imshow(kern_name.str(), disp_kernel);
      cv::waitKey(3);
#endif // DISPLAY_FILTER_IMGS
    }

    // Sort modes by value
    std::sort(sample_list.begin(), sample_list.end(),
              TabletopSimNode::compareModes);
    double running_cdf = 0.0;
#ifdef DEBUG_CDF_CALC
    ROS_INFO_STREAM("Calculating cdfs!");
#endif // DEBUG_CDF_CALC
    // Calculate cdf_weight for each sample
    for (int i = sample_list.size() - 1; i >= 0; --i)
    {
      running_cdf += sample_list[i].weight;
      sample_list[i].cdf_weight = running_cdf;
    }
#ifdef DEBUG_CDF_CALC
    ROS_INFO_STREAM("Calculated cdfs!");
#endif // DEBUG_CDF_CALC
    // Normalize nodes to be a posterior distribution
    for (unsigned int i = 0; i < sample_list.size(); ++i)
    {
      sample_list[i].cdf_weight /= running_cdf;
      sample_list[i].weight /= running_cdf;
    }
#ifdef DEBUG_CDF_CALC
    ROS_INFO_STREAM("Sample 0 cdf is: " << sample_list[0].cdf_weight);
    ROS_INFO_STREAM("Sample 0 weight is: " << sample_list[0].weight);
    ROS_INFO_STREAM("Sample last cdf is: " <<
                    sample_list[sample_list.size()-1].cdf_weight);
    ROS_INFO_STREAM("Sample last weight is: " <<
                    sample_list[sample_list.size()-1].weight);
    ROS_INFO_STREAM("Normalized CDF values!");
#endif // DEBUG_CDF_CALC

    // Convert into the correct form
    PlacementScores ps;
    for (unsigned int i; i < sample_list.size(); ++i)
    {
      ps.poses.push_back(sample_list[i].pose);
      ps.scores.push_back(sample_list[i].cdf_weight);
    }
    res.ps = ps;
#ifdef DISPLAY_FILTER_IMGS
    cv::waitKey();
#endif // DISPLAY_FILTER_IMGS
    return res;
  }

  cv::Mat getBinaryTabletopImage(cv::Mat& disp_img, bool use_padding = false)
  {
    int y_padding = 0;
    int x_padding = 0;
    if (use_padding)
    {
      y_padding = y_padding_;
      x_padding = x_padding_;
    }
    cv::Mat bin_img(table_y_ + 2*y_padding, table_x_ + 2*x_padding, CV_32FC1,
                    0.0);
    std::vector<cv::Mat> channels;
    split(disp_img, channels);
    for (int r = y_padding; r < y_padding + table_y_; ++r)
    {
      for (int c = x_padding; c < x_padding + table_x_; ++c)
      {
        double col0 = static_cast<double>(
            channels[0].at<uchar>(r, c));
        double col1 = static_cast<double>(
            channels[1].at<uchar>(r, c));
        double col2 = static_cast<double>(
            channels[2].at<uchar>(r, c));
        if (col0 == table_color_[0] ||
            col1 == table_color_[1] ||
            col2 == table_color_[2])
        {
          bin_img.at<float>(r,c) = 1.0;
        }
      }
    }
    return bin_img;
  }

  /**
   * Method to generate a random object on the tabletop domain.
   *
   * @return The randomly generated object.
   */
  TabletopObject generateRandObject()
  {
    cv::Scalar rand_color;
    rand_color[0] = rand() % 255;
    rand_color[1] = rand() % 255;
    rand_color[2] = rand() % 255;

    TabletopObject obj;
    obj.disp_color = rand_color;
    obj.is_pushable =  ((static_cast<float>(rand()) /
                         static_cast<float>(RAND_MAX)) > pushable_prob_);
    obj.is_oval = ((static_cast<float>(rand()) /
                    static_cast<float>(RAND_MAX)) < oval_prob_);
    if (obj.is_oval)
    {
      obj.x_size = rand() % (max_oval_radius_ - min_oval_radius_) +
          min_oval_radius_;
#ifdef FORCE_CIRCLES
      obj.y_size = obj.x_size;
#else
      obj.y_size = rand() % (max_oval_radius_ - min_oval_radius_) +
          min_oval_radius_;
#endif
    }
    else
    {
      obj.x_size = rand() % (max_rect_length_ - min_rect_length_) +
          min_rect_length_;
      obj.y_size = rand() % (max_rect_length_ - min_rect_length_) +
          min_rect_length_;
    }

    do
    {
      obj.pose.x = rand() % table_x_;
      obj.pose.y = rand() % table_y_;
      obj.pose.theta = (static_cast<float>(rand()) /
                        static_cast<float>(RAND_MAX))*2.0*M_PI - M_PI;
    } while (!objectOnTable(obj) || collidesWithCurrentObjects(obj));
    return obj;
  }

  /**
   * Rotate a Vector (cv::Point) theta radians
   *
   * @param to_rot Vector to be rotated
   * @param sin_t sin(theta)
   * @param cos_t cos(theta)
   *
   * @return Vector rotated by theta radians
   */
  cv::Point rotateVector(cv::Point to_rot, float sin_t, float cos_t)
  {
    const float x = to_rot.x*cos_t - to_rot.y*sin_t;
    const float y = to_rot.x*sin_t + to_rot.y*cos_t;
    to_rot.x = x;
    to_rot.y = y;
    return to_rot;
  }

  cv::Point rotateVector(cv::Point to_rot, float theta)
  {
    return rotateVector(to_rot, sin(theta), cos(theta));
  }

  void getRectangleCorners(TabletopObject rect, cv::Point* points)
  {
    int x_radius = rect.x_size / 2;
    int y_radius = rect.y_size / 2;

    float ct = cos(rect.pose.theta);
    float st = sin(rect.pose.theta);

    // Rotate corners by the object orientation and offset the object by its
    // center
    points[0] = rotateVector(cv::Point(-x_radius, -y_radius), st, ct);
    points[1] = rotateVector(cv::Point(x_radius, -y_radius), st, ct);
    points[2] = rotateVector(cv::Point(x_radius, y_radius), st, ct);
    points[3] = rotateVector(cv::Point(-x_radius, y_radius), st, ct);
    cv::Point center(rect.pose.x, rect.pose.y);
    for (int i = 0; i < 4; ++i)
    {
      points[i] += center;
    }
  }

  /**
   * Test if a given object collides with any of the currently placed objects
   *
   * @param to_test The object to test, it should not be in the objects_ vector.
   *
   * @return true if the object overlaps any of the objects on the table,
   *         false otherwise.
   */
  bool collidesWithCurrentObjects(TabletopObject to_test)
  {
    for (unsigned int i = 0; i < objects_.size(); ++i)
    {
      // First determine if the object occludes the other object, but does not
      // intersect with its boundary
      if ( isInside(to_test, objects_[i]) ) return true;
      if ( isInside(objects_[i], to_test) ) return true;

      // Test if the object boundaries intersect
      if ( boundariesIntersect(to_test, objects_[i]) ) return true;
    }
    return false;
  }

  /**
   * Determine if the boundaries of two objects intersect
   *
   * @param a The first object
   * @param b The second object
   *
   * @return true if the boundaries intersect, otherwise false.
   */
  bool boundariesIntersect(TabletopObject a, TabletopObject b)
  {
    if (a.is_oval && b.is_oval) // Both circles
    {
      // Distance between the centers of two circles must be greater than the
      // sum of their radii
      float d_x = a.pose.x - b.pose.x;
      float d_y = a.pose.y - b.pose.y;
      return (std::sqrt(d_x*d_x+d_y*d_y) <= a.x_size + b.x_size);
    }
    else if (a.is_oval) // B is a rectangle
    {
      return rectOvalIntersection(b, a);
    }
    else if (b.is_oval) // A is a rectangle
    {
      return rectOvalIntersection(a, b);
    }
    else // Both objects are rectangles
    {
      const int n = 4;
      const int n_a = 4;
      const int n_b = 4;
      cv::Point poly_a[n];
      getRectangleCorners(a, poly_a);
      cv::Point poly_b[n];
      getRectangleCorners(b, poly_b);

      // Iterate through the segments of polygon a
      for (int i =  0; i < n_a; ++i)
      {
        cv::Point p1 = poly_a[i];
        cv::Point p2 = poly_a[(i+1)%n_a];
        // Check intersection of (p1,p2) and poly_b
        if ( segmentPolygonIntersection(p1, p2, poly_b, n_b) ) return true;
      }

      return false;
    }
  }

  /**
   * Determin if a line semgent and polygon intersect
   *
   * @param p1 first endpoint of the line segment
   * @param p2 second endpoint of the line segment
   * @param poly points defining the polygon boundary
   * @param n number of points in the polygon boundary
   *
   * @return true if the two intersect, otherwise false
   */
  bool segmentPolygonIntersection(cv::Point p1, cv::Point p2, cv::Point* poly,
                                  const int n)
  {
    for (int i = 0; i < n; ++i)
    {
      cv::Point w1 = poly[i];
      cv::Point w2 = poly[(i+1)%n];
      if (segmentSegmentIntersection(p1,p2,w1,w2)) return true;
    }

    return false;
  }

  /**
   * Determine if two line segments intersect
   *
   * @param p1 first endpoint of the first line segment
   * @param p2 second endpoint of the first line segment
   * @param w1 first endpoint of the second line segment
   * @param w2 second endpoint of the second line segment
   *
   * @return true if they intersect, false otherwise
   */
  bool segmentSegmentIntersection(cv::Point p1, cv::Point p2,
                                  cv::Point w1, cv::Point w2)
  {
    // Check if line 1 is vertical
    if (p1.x == p2.x)
    {
      if ((w1.x <= p1.x && w2.x >= p2.x) || (w1.x >= p1.x && w2.x <= p2.x))
      {
        // float wy = (w2.y - w1.y) / (w2.x - w2.x)*(p1.x - w1.x) + w1.y;
        // if ((wy <= p1.y && wy >= p2.y) ||(wy >= p1.y && wy <= p2.y ))
        // {
        //   return true;
        // }
      }
    }
    // Check if line 2 is vertical
    if (w1.x == w2.x)
    {
      if ((p1.x <= w1.x && p2.x >= w2.x) || (p1.x >= w1.x && p2.x <= w2.x))
      {
        // float py = (p2.y - p1.y) / (p2.x - p2.x)*(w1.x - p1.x) + p1.y;
        // if ((py <= w1.y && py >= w2.y) ||(py >= w1.y && py <= w2.y ))
        // {
        //   return true;
        // }
      }
    }

    // Find intersection of the two lines:
    float m_p = (p1.y - p2.y)/(p1.x - p2.x);
    float m_w = (w1.y - w2.y)/(w1.x - w2.x);

    float x_int = (p1.y - m_p*p1.x - w1.y + m_w*w1.x) / (m_w - m_p);

    return (((x_int <= p1.x && x_int >= p2.x) ||
             (x_int >= p1.x && x_int <= p2.x)) &&
            ((x_int <= w1.x && x_int >= w2.x) ||
             (x_int >= w1.x && x_int <= w2.x)));
  }

  /**
   * Test if a rectangle and circle intersect.
   *
   * @param rect The rectangle to test.
   * @param circl The circle to test.
   *
   * @return true if they intersect, false otherwise.
   */
  bool rectOvalIntersection(TabletopObject rect, TabletopObject circl)
  {
    // TODO: implement this function
    return false;
  }

  /**
   * Determine if the center of object a resides inside the boundary of object b
   *
   * @param a The object the center of which is to be tested
   * @param b The object the boundary of which is to be tested
   *
   * @return true if the center of a resides within the boundary of b
   */
  bool isInside(TabletopObject a, TabletopObject b)
  {
    if (b.is_oval)
      return (a.pose.x < b.pose.x + b.x_size &&
              a.pose.x > b.pose.x - b.x_size &&
              a.pose.y < b.pose.y + b.y_size &&
              a.pose.y > b.pose.y - b.y_size);
    else
    {
      const int n_b = 4;
      cv::Point points[n_b];
      getRectangleCorners(b, points);
      cv::Point center(a.pose.x, a.pose.y);

      if (pointInPolygon(center, points, n_b)) return true;
      if (! a.is_oval)
      {
        const int n_a = 4;
        cv::Point a_points[n_a];
        getRectangleCorners(a, a_points);
        for (int i = 0; i < n_a; ++i)
        {
          if(pointInPolygon(a_points[i], points, n_b)) return true;
        }
      }
    }

    return false;
  }

  /**
   * Determin if a point is inside a polygon.
   * NOTE: This and other methods should probably be moved to a separate class.
   *
   * @param pt The point to be tested.
   * @param poly The boundary points in order defining the polygon
   * @param n The number of elements in the boundary
   *
   * @return true if the point lies within the boundary of the polygon,
   *         otherwise false
   */
  bool pointInPolygon(cv::Point pt, cv::Point* poly, const int n)
  {
      bool inside = false;
      cv::Point p1 = poly[0];
      for (int i = 0; i <= n; ++i)
      {
        cv::Point p2 = poly[i % n];
        if (pt.y > std::min(p1.y, p2.y) && pt.y <= std::max(p1.y, p2.y) &&
            pt.x < std::max(p1.x, p2.x))
        {
          float x_cross = 0.0;
          if (p1.y != p2.y)
            x_cross = (pt.y - p1.y)*(p2.x - p1.x)/(p2.y - p1.y)+p1.x;
          if (p1.x == p2.x || pt.x <= x_cross)
            inside = !inside;
        }
        p1 = p2;
      }
      return inside;
  }

  /**
   * Test that the entire object is currently on the table
   *
   * @param to_test The object to examine
   *
   * @return true if on the table, false otherwise
   */
  bool objectOnTable(TabletopObject to_test)
  {
    if (to_test.is_oval)
    {
      return (to_test.pose.x - to_test.x_size > 0 &&
              to_test.pose.x + to_test.x_size < table_x_ &&
              to_test.pose.y - to_test.y_size > 0 &&
              to_test.pose.y + to_test.y_size < table_y_);
    }
    else
    {
      bool on_table = true;
      cv::Point points[4];
      getRectangleCorners(to_test, points);
      for (int i = 0; i < 4; ++i)
      {
        on_table = (on_table &&
                    points[i].x > 0 && points[i].x < table_x_ &&
                    points[i].y > 0 && points[i].y < table_y_);
      }
      return on_table;
    }
    return true;
  }

  /**
   * Method to transform the coordinates in the world frame to those in the
   * image frame for approrpirate display.
   *
   * @param world The point in the coordinate frame of the table.
   *
   * @return A point representing this point in the frame of the image
   */
  cv::Point worldPointToImagePoint(cv::Point world)
  {
    cv::Point img_point(x_padding_ + world.x,
                        y_padding_ + world.y);
    return img_point;
  }

 protected:
  ros::NodeHandle n_;
  ros::ServiceServer placement_srv_;
  int x_padding_;
  int y_padding_;
  int table_x_;
  int table_y_;
  int num_init_objects_;
  int min_oval_radius_;
  int max_oval_radius_;
  int min_rect_length_;
  int max_rect_length_;
  double oval_prob_;
  double pushable_prob_;
  double max_score_thresh_percent_;
  int num_test_orientations_;
  int test_x_;
  int test_y_;
  cv::Scalar table_color_;
  cv::Scalar padding_color_;
  std::vector<TabletopObject> objects_;
  bool use_binary_padding_;
  bool only_use_modes_;
};

int main(int argc, char** argv)
{
  int seed = time(NULL);
  srand(seed);
  std::cout << "Seed value is: " << seed << std::endl;
  ros::init(argc, argv, "tabletop_sim_node");
  ros::NodeHandle n;
  TabletopSimNode sim_node(n);
  sim_node.spin();
}
