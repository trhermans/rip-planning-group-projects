# Hello World examples
include_directories(../)
rosbuild_add_executable(HelloWorld HelloWorld.cpp)
target_link_libraries (HelloWorld Box2D)
