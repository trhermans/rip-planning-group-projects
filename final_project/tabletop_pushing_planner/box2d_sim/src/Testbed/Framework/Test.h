#ifndef TEST_H
#define TEST_H

#define SCENARIO1 1


#include <Box2D/Box2D.h>
#include "Render.h"
#include <assert.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <cstdlib>
#include <iostream>
#include <stack>
#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include <queue>
#include "TreeNode.h"
#include "tabletop_planner_sim/TabletopEnvironment.h"
#include "tabletop_planner_sim/PlacementScores.h"
#include "tabletop_planner_sim/PlacementHeuristic.h"

class Test;
struct Settings;

typedef Test* TestCreateFcn(ros::NodeHandle n);

#define	RAND_LIMIT	32767
#define IMG_SCALE_VAL   10

/// Random number in range [-1,1]
inline float32 RandomFloat()
{
	float32 r = (float32)(rand() & (RAND_LIMIT));
	r /= RAND_LIMIT;
	r = 2.0f * r - 1.0f;
	return r;
}

using namespace std;


/// Random floating point number in range [lo, hi]
inline float32 RandomFloat(float32 lo, float32 hi)
{

	//srand(time(0));
	float32 r = (float32)(rand() & (RAND_LIMIT));
	r /= RAND_LIMIT;
	r = (hi - lo) * r + lo;
	//cout<<"random float: "<<r<<endl;
	return r;

}

/// Test settings. Some can be controlled in the GUI.
struct Settings
{
	Settings() :
		hz(60.0f),
		velocityIterations(8),
		positionIterations(3),
		drawStats(0),
		drawShapes(1),
		drawJoints(1),
		drawAABBs(0),
		drawPairs(0),
		drawContactPoints(0),
		drawContactNormals(0),
		drawContactForces(0),
		drawFrictionForces(0),
		drawCOMs(0),
		enableWarmStarting(1),
		enableContinuous(1),
		enableSubStepping(0),
		pause(0),
		singleStep(0)
		{}

	float32 hz;
	int32 velocityIterations;
	int32 positionIterations;
	int32 drawStats;
	int32 drawShapes;
	int32 drawJoints;
	int32 drawAABBs;
	int32 drawPairs;
	int32 drawContactPoints;
	int32 drawContactNormals;
	int32 drawContactForces;
	int32 drawFrictionForces;
	int32 drawCOMs;
	
	int32 enableWarmStarting;
	int32 enableContinuous;
	int32 enableSubStepping;
	int32 pause;
	int32 singleStep;
};

struct TestEntry
{
	const char *name;
	TestCreateFcn *createFcn;
};

extern TestEntry g_testEntries[];
// This is called when a joint in the world is implicitly destroyed
// because an attached body is destroyed. This gives us a chance to
// nullify the mouse joint.
class DestructionListener : public b2DestructionListener
{
public:
	void SayGoodbye(b2Fixture* fixture) { B2_NOT_USED(fixture); }
	void SayGoodbye(b2Joint* joint);

	Test* test;
};

const int32 k_maxContactPoints = 2048;
const int32 max_worldNodes = 2048;

struct ContactPoint
{
	b2Fixture* fixtureA;
	b2Fixture* fixtureB;
	b2Vec2 normal;
	b2Vec2 position;
	b2PointState state;
};

class Test : public b2ContactListener
{
public:

        Test(ros::NodeHandle n);
	virtual ~Test();

	enum MoveType
	{
		Left = 1,
		Right,
		Up,
		Down
	};

	struct Plan {
		int collidedBodyID;
		float pushAngle;
		int sensorBodyObjectID;
		bool convertSensorBodyAfterwards;
		bool enableSensorEscapeMode;
		bool pushMotion;
	};
	Plan CompletePlan[max_worldNodes];

	MoveType move;
	vector<TreeNode*> allTreeNodes;
	void destroyAllNodes();
	
	
	
    stack<b2World> allworlds;
    
    unsigned int pushesPerObject;
    int objectCount;
	float32 minx;
	float32 miny;
	float32 maxx;
	float32 maxy;
	float32 gripperWidth;
	float32 gripperHeight;
	bool showGraphicsMaster;
	bool showGraphicsPlannerInternal;

	
	
	void SetTextLine(int32 line) { m_textLine = line; }
    void DrawTitle(int x, int y, const char *string);
	virtual void Step(Settings* settings);
	virtual void Keyboard(unsigned char key) { B2_NOT_USED(key); }
	virtual void KeyboardUp(unsigned char key) { B2_NOT_USED(key); }
	void ShiftMouseDown(const b2Vec2& p);
	virtual void MouseDown(const b2Vec2& p);
	virtual void MouseUp(const b2Vec2& p);
	void MouseMove(const b2Vec2& p);
	void LaunchBomb();
	void LaunchBomb(const b2Vec2& position, const b2Vec2& velocity);
	
	void SpawnBomb(const b2Vec2& worldPt);
	void CompleteBombSpawn(const b2Vec2& p);
	//b2Body* createCircleObject(b2World *world, int ID,b2PolygonShape shape,float x,float y,int type);
	void RandomizeAllObjectSizes();
	void randomizeSize(int id);
	void wait ( float seconds );
	void printAllCollisions(b2World *currentWorld);
	bool collisionCheckForAllImmovableBodies(b2World *currentWorld,  int &bodyHitObjectID);
	bool collisionCheckForAllImmovablesOnImmovables(b2World *currentWorld,  int &bodyHitObjectID);
	bool collisionCheckForAllTableBorders(b2World *currentWorld,  int &bodyHitObjectID);
	bool collisionCheckForABody(b2World *currentWorld,int bodyAobjectID,int acceptedObjectCodes[], int acceptedObjectCodesSize,int &bodyHitObjectID);
	bool IsCollision(b2Body* bodyA, b2Body* bodyB,int indexA, int indexB);
	bool IsCollision2(int objectIDbodyA, int objectIDbodyB);
	bool plan();
	int howManyMovablesOnTable();
	int howManyImmovablesOnTable();
	bool collisionCheckForAllSensorBodies(b2World *currentWorld,  int &bodyHitObjectID);
	void retrieveCurrentSensorBodyIDVector(b2World *currentWorld,vector<int> &sensorBodyIDVector);
	bool confirmIfMovableOrImmovableFromObjectID(int req);
	int32 AddNewWorldToNodeList(b2World *world, int32 parentIndex, MoveType move);
	void makeAllVelocitiesZero(b2World *world);
	void convertSensorBodyToMovableBody(int sensorBodyID);
	void MoveBodyDownWrtBodyFrame(b2Body* body);
	void MoveBodyUpWrtBodyFrame(b2Body* body);
	void MoveBodyLeftWrtBodyFrame(b2Body* body);
	void MoveBodyRightWrtBodyFrame(b2Body* body);
	void MoveBodyGeneral(b2Body* body, float desiredMagnitude, float desiredAngle);
	int getNumberOfObjectsInSensorField(int sensorFieldID);
	void getIntersectingIDsInSensorField(int sensorID, vector<int> &stemObjectIDList);
	float PushWithGripperUpWrtBodyFrame(b2World *world, int pushedBodyID, float desiredMagnitude, int& errorcode, bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID);
	float PushWithGripperDownWrtBodyFrame(b2World *world, int pushedBodyID, float desiredMagnitude, int& errorcode,bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID);
	float PushWithGripperLeftWrtBodyFrame(b2World *world,int pushedBodyID, float desiredMagnitude, int& errorcode,bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID);
	float PushWithGripperRightWrtBodyFrame(b2World *world, int pushedBodyID, float desiredMagnitude, int& errorcode,bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID);
	float PushWithGripperGeneral(b2World *world, int pushedBodyID, float desiredMagnitude, float desiredAngle, int& errorcode, bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID);
	void updateScreenPhysics(bool refreshScreen);
	bool PlaceGripperForPushingMotion(b2World *world, int targetBodyID,float desiredAngle, b2Body* &gripperObjectBody);
	float GetMinPushAngle(int objectIDSensor, int objectIDpushedBody,float angleResolution,bool allowCollisionsWithRegularObjects,float pushForce, int &aFirstContactObjectID);
	float GetMaxPushAngle(int objectIDSensor, int objectIDpushedBody,float angleResolution,bool allowCollisionsWithRegularObjects,bool sensorFieldClearOption,float pushForce);
	bool GetRandomPlacementLocation();
	bool GetRandomObjectPlacementOnTable();
	void saveWorldState(worldState &worldStateToBeSaved);
	void loadWorldState(worldState worldStateToBeLoaded);
	void copyWorldState(worldState sourceWorld,worldState &destinationWorld);
	void myitoa(int n, char s[]);
	void reverse(char s[]);
	bool receiveBodyWithObjectID(int requestedObjID,b2Body* &rcvdBody);
	bool planPushes(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce); //look to the sensor fields, plan to escape them.
	bool planPushesWithConvolution(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce, int sensorBodyIDtoPlan); //look to the sensor fields, plan to escape them.
	bool planPushesWithConvolutionAllowImmovablePushes(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce, int sensorBodyIDtoPlan); //look to the sensor fields, plan to escape them.
	bool planWithConvolution();
	bool planWithConvolutionAllowImmovablePushes();
	bool planIDS();
	bool planPushesIDS(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce, int sensorBodyIDtoPlanint ,int maxAllowedTreeLevel,vector<Plan> &masterPlan); //look to the sensor fields, plan to escape them.
	bool checkIfGoal(float pushForce,int treeIndexes[],TreeNode* nodeArray[],int intersectingObjectID,int targetSensorObjectID,int currentNumberOfIntersectingObjects,vector<Plan> &planVector,bool revertWorldWhenGoalFound);
	void replayPlan(vector<Plan> &PartialPlan,float replayPushForce, float initialWait, float replayWaits);
	bool TestNode();
	void mergePlans(vector<Plan> &original,vector<Plan> &added);
	
	// Let derived tests know that a joint was destroyed.
	virtual void JointDestroyed(b2Joint* joint) { B2_NOT_USED(joint); }
	void printErrorCode(int errorcode);
	int returnObjectID(b2Body* rcvdBody);
	// Callbacks for derived classes.
	virtual void BeginContact(b2Contact* contact) { B2_NOT_USED(contact); }
	virtual void EndContact(b2Contact* contact) { B2_NOT_USED(contact); }
	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
	virtual void PostSolve(const b2Contact* contact, const b2ContactImpulse* impulse)
	{
		B2_NOT_USED(contact);
		B2_NOT_USED(impulse);
	}
	b2Body* newbody[100];
	void getObjectPlacementPose(int inquiredSensorBodyID, bool new_environment = true);
        bool getReliableObjectPlacementPose(int inquiredSensorBodyID, bool new_env = true);
  bool getReliableObjectPlacementPoseAllowImmovablePushes(int inquiredSensorBodyID, bool new_env = true);
	
  inline float box2DxToCVx(float box2d_x) { return (box2d_x + 20)*IMG_SCALE_VAL; }
  inline float box2DyToCVy(float box2d_x) { return (box2d_x)*IMG_SCALE_VAL; }
  inline float CVxToBox2Dx(float cv_x) { return (cv_x)/IMG_SCALE_VAL - 20; }
  inline float CVyToBox2Dy(float cv_x) { return (cv_x)/IMG_SCALE_VAL; }

protected:
	friend class DestructionListener;
	friend class BoundaryListener;
	friend class ContactListener;

	b2Body* m_groundBody;
	b2AABB m_worldAABB;
	ContactPoint m_points[k_maxContactPoints];
	int32 m_pointCount;
	DestructionListener m_destructionListener;
	DebugDraw m_debugDraw;
	int32 m_textLine;
	b2World* m_world;
	b2World* temp_world;
	b2Body* m_bomb;
	b2MouseJoint* m_mouseJoint;
	b2Vec2 m_bombSpawnPoint;
	bool m_bombSpawning;
	b2Vec2 m_mouseWorld;
	int32 m_stepCount;
	b2WorldManifold collisionManifold;



	int32 worldIndex;
	struct worldStruct {
		b2World *nodes;
		int32 parentIndex;
		MoveType move;
	};
	worldStruct worldNodes[max_worldNodes];

 protected:
  ros::NodeHandle n_;
  ros::ServiceClient placement_client_;
  tabletop_planner_sim::PlacementScores ps_;
  double clutter_percent_;
  double object_free_space_;
  bool force_cdf_change_;
  int nodezExplored;
  ofstream logFile;
};

#endif
