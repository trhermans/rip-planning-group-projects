#include "Test.h"
#include "TreeNode.h"
#include "Render.h"
#include <ros/ros.h>

#include "freeglut/GL/glut.h"
#include <list>
#include <cstdio>
#include <stack>

using tabletop_planner_sim::TabletopEnvironment;
using tabletop_planner_sim::TabletopObject;
using tabletop_planner_sim::PlacementScores;
using tabletop_planner_sim::PlacementHeuristic;

//#define DEBUG_POSE_SAMPLING 1

int cdfBinarySearch(vector<float>& scores, float cdf_goal);

int cdfBinarySearch(vector<float>& scores, float cdf_goal)
{
	int min_idx = 0;
	int max_idx = scores.size();
	int cur_idx = min_idx + max_idx / 2;
	while (min_idx != max_idx)
	{
		cur_idx = (min_idx + max_idx)/2;
		float cur_val = scores[cur_idx];
		if (cur_val == cdf_goal || (cur_val > cdf_goal &&
				scores[cur_idx+1] < cdf_goal))
		{
			return cur_idx;
		}
		else if (cur_val > cdf_goal)
		{
			min_idx = cur_idx;
		}
		else
		{
			max_idx = cur_idx;
		}
	}
	return cur_idx;
}

void DestructionListener::SayGoodbye(b2Joint* joint)
{
	if (test->m_mouseJoint == joint)
	{
		test->m_mouseJoint = NULL;
	}
	else
	{
		test->JointDestroyed(joint);
	}
}
Settings currentSettings;

Test::Test(ros::NodeHandle n) : n_(n), clutter_percent_(0.0),
                                object_free_space_(0.0)
{
	objectCount=0;
	minx=-20.0f;
	maxx=20.0f;
	miny=0.0f;
	maxy=30.0f;
	gripperWidth=0.5;
	gripperHeight=2.0;
	showGraphicsPlannerInternal=true;
	showGraphicsMaster=true;
	pushesPerObject=0;


	b2Vec2 gravity;
	gravity.Set(0.0f, -10.0f);
	bool doSleep = true;
	m_world = new b2World(gravity, doSleep);
	m_bomb = NULL;
	m_textLine = 30;
	m_mouseJoint = NULL;
	m_pointCount = 0;

	m_destructionListener.test = this;
	m_world->SetDestructionListener(&m_destructionListener);
	m_world->SetContactListener(this);
	m_world->SetDebugDraw(&m_debugDraw);

	m_bombSpawning = false;

	m_stepCount = 0;

	b2BodyDef bodyDef;
	m_groundBody = m_world->CreateBody(&bodyDef);
	objectCount++;

	worldIndex = 0;
	//cout<<"In Test()"<<endl;
	placement_client_ = n_.serviceClient<tabletop_planner_sim::PlacementHeuristic>(
			"placement_candidates");
	ros::NodeHandle n_private("~");
	n_private.param("froce_cdf_change", force_cdf_change_, false);

}

Test::~Test()
{
	// By deleting the world, we delete the bomb, mouse joint, etc.
	delete m_world;
	m_world = NULL;
}

void Test::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
	const b2Manifold* manifold = contact->GetManifold();

	if (manifold->pointCount == 0)
	{
		return;
	}



	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();

	b2PointState state1[b2_maxManifoldPoints], state2[b2_maxManifoldPoints];
	b2GetPointStates(state1, state2, oldManifold, manifold);

	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);

	// Victor Added
	contact->GetWorldManifold(&collisionManifold);

	for (int32 i = 0; i < manifold->pointCount && m_pointCount < k_maxContactPoints; ++i)
	{
		ContactPoint* cp = m_points + m_pointCount;
		cp->fixtureA = fixtureA;
		cp->fixtureB = fixtureB;
		cp->position = worldManifold.points[i];
		cp->normal = worldManifold.normal;
		cp->state = state2[i];
		++m_pointCount;
	}


}

void Test::DrawTitle(int x, int y, const char *string)
{
	m_debugDraw.DrawString(x, y, string);
}

class QueryCallback : public b2QueryCallback
{
public:
	QueryCallback(const b2Vec2& point)
	{
		m_point = point;
		m_fixture = NULL;
	}

	bool ReportFixture(b2Fixture* fixture)
	{
		b2Body* body = fixture->GetBody();
		if (body->GetType() == b2_dynamicBody)
		{
			bool inside = fixture->TestPoint(m_point);
			if (inside)
			{
				m_fixture = fixture;

				// We are done, terminate the query.
				return false;
			}
		}

		// Continue the query.
		return true;
	}




	b2Vec2 m_point;
	b2Fixture* m_fixture;
};

void Test::MouseDown(const b2Vec2& p)
{
	m_mouseWorld = p;

	if (m_mouseJoint != NULL)
	{
		return;
	}

	// Make a small box.
	b2AABB aabb;
	b2Vec2 d;
	d.Set(0.001f, 0.001f);
	aabb.lowerBound = p - d;
	aabb.upperBound = p + d;

	// Query the world for overlapping shapes.
	QueryCallback callback(p);
	m_world->QueryAABB(&callback, aabb);

	if (callback.m_fixture)
	{
		b2Body* body = callback.m_fixture->GetBody();
		b2MouseJointDef md;
		md.bodyA = m_groundBody;
		md.bodyB = body;
		md.target = p;
		md.maxForce = 1000.0f * body->GetMass();
		m_mouseJoint = (b2MouseJoint*)m_world->CreateJoint(&md);
		body->SetAwake(true);
	}
}

void Test::SpawnBomb(const b2Vec2& worldPt)
{
	m_bombSpawnPoint = worldPt;
	m_bombSpawning = true;
}

void Test::CompleteBombSpawn(const b2Vec2& p)
{
	if (m_bombSpawning == false)
	{
		return;
	}

	const float multiplier = 30.0f;
	b2Vec2 vel = m_bombSpawnPoint - p;
	vel *= multiplier;
	LaunchBomb(m_bombSpawnPoint,vel);
	m_bombSpawning = false;
}

void Test::ShiftMouseDown(const b2Vec2& p)
{
	m_mouseWorld = p;

	if (m_mouseJoint != NULL)
	{
		return;
	}

	SpawnBomb(p);
}

void Test::MouseUp(const b2Vec2& p)
{
	if (m_mouseJoint)
	{
		m_world->DestroyJoint(m_mouseJoint);
		m_mouseJoint = NULL;
	}

	if (m_bombSpawning)
	{
		CompleteBombSpawn(p);
	}
}

void Test::MouseMove(const b2Vec2& p)
{
	m_mouseWorld = p;

	if (m_mouseJoint)
	{
		m_mouseJoint->SetTarget(p);
	}
}

void Test::LaunchBomb()
{
	b2Vec2 p(RandomFloat(-15.0f, 15.0f), 30.0f);
	b2Vec2 v = -5.0f * p;
	LaunchBomb(p, v);
}

void Test::LaunchBomb(const b2Vec2& position, const b2Vec2& velocity)
{
	if (m_bomb)
	{
		m_world->DestroyBody(m_bomb);
		m_bomb = NULL;
	}

	b2BodyDef bd;
	bd.type = b2_dynamicBody;
	bd.position = position;
	bd.bullet = true;
	m_bomb = m_world->CreateBody(&bd);
	m_bomb->SetLinearVelocity(velocity);

	b2CircleShape circle;
	circle.m_radius = 0.3f;

	b2FixtureDef fd;
	fd.shape = &circle;
	fd.density = 20.0f;
	fd.restitution = 0.0f;

	b2Vec2 minV = position - b2Vec2(0.3f,0.3f);
	b2Vec2 maxV = position + b2Vec2(0.3f,0.3f);

	b2AABB aabb;
	aabb.lowerBound = minV;
	aabb.upperBound = maxV;

	m_bomb->CreateFixture(&fd);
}


void Test::RandomizeAllObjectSizes()
{
  b2Body* bodyList=m_world->GetBodyList();
  for (b2Body* a = bodyList; a; a = a->GetNext())
    {
      if (a->GetUserData()!=NULL)
	{
	  MyBodyData *data = (MyBodyData*)a->GetUserData();
	  if(data->type == 1 || data->type == 2 || data->type == 6) //randomize sensors, movable and immovables
	    {
	      randomizeSize(data->objectID);
	    }
	}
    }
  
}

// Add new world and save parent world index - use for planner
int32 Test::AddNewWorldToNodeList(b2World *world, int32 parentIndex, MoveType move)
{
	worldNodes[worldIndex].nodes = world;
	worldNodes[worldIndex].parentIndex = parentIndex;
	worldNodes[worldIndex].move = move;
	worldIndex++;
	return worldIndex;
}

void Test::MoveBodyUpWrtBodyFrame(b2Body* body)
{
	float angle = body->GetAngle();
	MoveBodyGeneral(body,1.0,angle);
}
void Test::MoveBodyRightWrtBodyFrame(b2Body* body)
{
	float angle = body->GetAngle();
	MoveBodyGeneral(body,1.0,angle-M_PI/2);
}
void Test::MoveBodyLeftWrtBodyFrame(b2Body* body)
{
	float angle = body->GetAngle();
	MoveBodyGeneral(body,1.0,angle+M_PI/2);
}
void Test::MoveBodyDownWrtBodyFrame(b2Body* body)
{
	float angle = body->GetAngle();
	MoveBodyGeneral(body,1.0,angle+M_PI);
}

void Test::MoveBodyGeneral(b2Body* body, float desiredMagnitude, float desiredAngle)
{
	b2Vec2 linearVelVector(desiredMagnitude*cos(desiredAngle),desiredMagnitude*sin(desiredAngle));
	body->SetLinearVelocity(linearVelVector);
	body->SetAngularVelocity(0);
}

float Test::PushWithGripperUpWrtBodyFrame(b2World *world, int pushedBodyID, float desiredMagnitude, int& errorcode,bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID)
{
	b2Body* body;
	receiveBodyWithObjectID(pushedBodyID,body);
	return PushWithGripperGeneral(world,pushedBodyID,desiredMagnitude, (float)(body->GetAngle()), errorcode,allowCollisionsWithRegularObjects,enableSensorFieldEscapeMode,sensorFieldToEscapeID,collidedBodyID,firstContactBodyID);
}
float Test::PushWithGripperRightWrtBodyFrame(b2World *world, int pushedBodyID, float desiredMagnitude, int& errorcode,bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID)
{
	b2Body* body;
	receiveBodyWithObjectID(pushedBodyID,body);
	return PushWithGripperGeneral(world,pushedBodyID,desiredMagnitude, (float)body->GetAngle()-M_PI/2, errorcode,allowCollisionsWithRegularObjects,enableSensorFieldEscapeMode,sensorFieldToEscapeID,collidedBodyID,firstContactBodyID);
}
float Test::PushWithGripperLeftWrtBodyFrame(b2World *world, int pushedBodyID, float desiredMagnitude, int& errorcode,bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID)
{
	b2Body* body;
	receiveBodyWithObjectID(pushedBodyID,body);
	return PushWithGripperGeneral(world,pushedBodyID,desiredMagnitude, (float)body->GetAngle()+M_PI/2, errorcode,allowCollisionsWithRegularObjects,enableSensorFieldEscapeMode,sensorFieldToEscapeID,collidedBodyID,firstContactBodyID);
}
float Test::PushWithGripperDownWrtBodyFrame(b2World *world, int pushedBodyID, float desiredMagnitude, int& errorcode,bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID,int &firstContactBodyID)
{
	b2Body* body;
	receiveBodyWithObjectID(pushedBodyID,body);
	return PushWithGripperGeneral(world,pushedBodyID,desiredMagnitude, (float)(body->GetAngle()+M_PI), errorcode,allowCollisionsWithRegularObjects,enableSensorFieldEscapeMode,sensorFieldToEscapeID,collidedBodyID,firstContactBodyID);
}
void Test::updateScreenPhysics(bool refreshScreen)
{
	if(refreshScreen)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
	Step(&currentSettings);
	if(refreshScreen)
	{
		glutSwapBuffers();
	}
}

void Test::copyWorldState(worldState sourceWorld,worldState &destinationWorld)
{
	destinationWorld.MyBodyDataVector=sourceWorld.MyBodyDataVector;
	destinationWorld.b2BodyDefVector=sourceWorld.b2BodyDefVector;
	destinationWorld.b2FixtureDefVector=sourceWorld.b2FixtureDefVector;
	destinationWorld.b2PolygonShapeVector=sourceWorld.b2PolygonShapeVector;
}


//errorcodes: 0=no space for gripper; 1=collided an immovable object; 2=collided the table borders;3=collided with a movable object;4=provided sensor field is not a sensor field!;5=the object is not in the sensor field!; 6=gripper going to infinity!; 7=great news. object got out of sensor field.
float Test::PushWithGripperGeneral(b2World *world, int pushedBodyID, float desiredMagnitude, float desiredAngle, int& errorcode, bool allowCollisionsWithRegularObjects, bool enableSensorFieldEscapeMode,int sensorFieldToEscapeID, int &collidedBodyID ,int &firstContactBodyID)
{


	firstContactBodyID=-99;

	b2Vec2 lastPosGripper;
	b2Vec2 currentPosGripper;
	bool firstContactFound=false;
	bool worldSavedAtLeastOnce=false;
	b2Body* body;
	receiveBodyWithObjectID(pushedBodyID,body);
	int acceptedCodesForMovableCollCheck[2]={1,2};
	worldState lastSavedWorldState;
	worldState last2ndSavedWorldState;
	worldState firstSavedWorldState;
	int lastSavedWorldStateCounter=0;
	int last2ndSavedWorldStateCounter=0;
	//int saveCounterInterval=(int)(50.0/desiredMagnitude); //can stop between 0.5-1.5 times this value
	int saveCounterInterval=2;
	if(desiredMagnitude>10.0)
	  {
	    saveCounterInterval=1;
	  }
	//cout<<"saveCounterInterval: "<<saveCounterInterval<<endl;

	bool rewindWorldMode=true;
	if(rewindWorldMode)
	{
		saveWorldState(lastSavedWorldState);
		saveWorldState(firstSavedWorldState);
	}

	if(enableSensorFieldEscapeMode)
	{
		b2Body* sensorFieldToEscape;
		receiveBodyWithObjectID(sensorFieldToEscapeID,sensorFieldToEscape);
		if(!sensorFieldToEscape->GetFixtureList()->IsSensor()) //4=provided sensor field is not a sensor field!
		{
			errorcode=4;
			return 0.0;
		}
		else if(IsCollision2(pushedBodyID,sensorFieldToEscapeID)==false) //5=the object is not in the sensor field!
		{
			errorcode=5;
			return 0.0;
		}
	}


	b2Body* gripperObjectBody;
	b2Vec2 initialPosition=body->GetPosition();

	bool gripperPlacement=PlaceGripperForPushingMotion(world,pushedBodyID,desiredAngle,gripperObjectBody);
	//cout<<"desiredAngle: "<<desiredAngle<<endl;
	//cout<<"gripperPlacement: "<<gripperPlacement<<endl;

	currentPosGripper=gripperObjectBody->GetPosition();
	lastPosGripper=gripperObjectBody->GetPosition();

	if(gripperPlacement)
	{
	pushesPerObject++;
		gripperObjectBody->SetFixedRotation(true);

		int counter=1;
		while(true)
		{

			MoveBodyGeneral(gripperObjectBody,desiredMagnitude,desiredAngle);
			counter++;
			//currentPosGripper=gripperObjectBody->GetPosition();
			//cout<<"teta: "<<atan2(currentPosGripper.y-lastPosGripper.y,currentPosGripper.x-lastPosGripper.x)<<endl;
			//cout<<counter<<endl;

			updateScreenPhysics(showGraphicsMaster && showGraphicsPlannerInternal);
			lastPosGripper=currentPosGripper;

			if(!firstContactFound && collisionCheckForABody(world,pushedBodyID,acceptedCodesForMovableCollCheck,2,firstContactBodyID))
			{
				firstContactFound=true;
			}
			if(rewindWorldMode&& counter%saveCounterInterval ==0) //save the world
			{
				copyWorldState(lastSavedWorldState,last2ndSavedWorldState);
				last2ndSavedWorldStateCounter=lastSavedWorldStateCounter;

				lastSavedWorldStateCounter=counter;
				//cout<<"Last saved: "<<counter<<endl;
				worldSavedAtLeastOnce=true;

				saveWorldState(lastSavedWorldState);
			}
			if(!allowCollisionsWithRegularObjects && collisionCheckForABody(world,pushedBodyID,acceptedCodesForMovableCollCheck,1,collidedBodyID))
			{
				errorcode=3; //3=collided with a movable object
				break;
			}
			if(collisionCheckForAllImmovableBodies(world,collidedBodyID))
			{
				errorcode=1; //1=collided an immovable object
				break;
			}
			if(collisionCheckForAllImmovablesOnImmovables(world,collidedBodyID))
			{
				errorcode=1; //1=collided an immovable object
				break;
			}
			if(collisionCheckForAllTableBorders(world,collidedBodyID))
			{
				errorcode=2; //2=collided the table borders
				break;
			}
			if(enableSensorFieldEscapeMode && IsCollision2(pushedBodyID,sensorFieldToEscapeID)==false) //the object is out of the sensory field.
			{
				errorcode=7; //7=great news. Target object got out of sensor field.
				break;
			}
			if(counter%50==0) //check if the gripper is going out of bounds & not go to infinity
			{
				//cout<<"In gripper check"<<endl;
				b2Vec2 currentGripperPosition=gripperObjectBody->GetPosition();
				//cout<<"x: "<<currentGripperPosition.x<<" y: "<<currentGripperPosition.y<<endl;
				if(currentGripperPosition.x>(maxx+abs(maxx*0.3)) || currentGripperPosition.x<(minx-abs(minx*0.3)) || currentGripperPosition.y>(maxy+abs(maxy*0.3)) || currentGripperPosition.y<(miny-abs(miny*0.3)))
				{
					errorcode=6;
					break;
				}
				if(!IsCollision2(pushedBodyID,returnObjectID(gripperObjectBody))) //the object slipped from the gripper.
				{
					errorcode=6;
					break;
				}
			}
		}

		if(rewindWorldMode && errorcode!=7)
		{
			if(worldSavedAtLeastOnce)
			{
				if((counter-lastSavedWorldStateCounter)>(saveCounterInterval/2))
				{
					//cout<<"Loading latest"<<endl;
					loadWorldState(lastSavedWorldState);
				}
				else
				{
					//cout<<"loading 2nd latest"<<endl;
					loadWorldState(last2ndSavedWorldState);
				}
			}
			else
			{
				loadWorldState(firstSavedWorldState);
			}

			world->ClearForces();
			makeAllVelocitiesZero(m_world);


			b2Body* gripperBodie;
			receiveBodyWithObjectID(31,gripperBodie);
			if(gripperBodie!=NULL)
			{
				world->DestroyBody(gripperBodie);
				objectCount--;
			}

			/*world->DestroyBody(gripperObjectBody);
			objectCount--;
			 */		
			world->ClearForces();
			makeAllVelocitiesZero(m_world);

			receiveBodyWithObjectID(pushedBodyID,body);
			b2Vec2 lastPosition=body->GetPosition();

			if(!firstContactFound)
			{
				firstContactBodyID=collidedBodyID;
			}
			return b2Distance(initialPosition,lastPosition); //TODO: change distance metric
		}
		else
		{
		  world->ClearForces();
		  makeAllVelocitiesZero(m_world);

			world->DestroyBody(gripperObjectBody);
			objectCount--;

			receiveBodyWithObjectID(pushedBodyID,body);
			b2Vec2 lastPosition=body->GetPosition();
			if(!firstContactFound)
			{
				firstContactBodyID=collidedBodyID;
			}
			return b2Distance(initialPosition,lastPosition); //TODO: change distance metric
		}
	}
	else
	{
		errorcode=0;
		return 0.0;
	}
}

bool Test::PlaceGripperForPushingMotion(b2World *world, int targetBodyID,float desiredAngle, b2Body* &gripperObjectBody)
{
	b2Vec2 incrementVector(0.05*cos(desiredAngle),0.05*sin(desiredAngle));

	b2Body* body;
	receiveBodyWithObjectID(targetBodyID,body);
	b2Vec2 currentBodyPositionVector=body->GetPosition();
	//cout<<"x: "<<currentBodyPositionVector(0)<<" y: "<<currentBodyPositionVector(1)<<endl;

	b2PolygonShape sensorShape; //gripperDefinition
	sensorShape.SetAsBox(gripperWidth, gripperHeight);
	b2FixtureDef sensorfd;
	sensorfd.shape = &sensorShape;
	sensorfd.isSensor = true;
	b2BodyDef sensorbd;
	sensorbd.type = b2_staticBody;
	sensorbd.angle=(float32)desiredAngle;
	sensorbd.position.Set(currentBodyPositionVector(0), currentBodyPositionVector(1));
	MyBodyData* bodyDataGrip = new MyBodyData;
	bodyDataGrip->type=6;
	bodyDataGrip->objectID=objectCount;
	sensorbd.userData=bodyDataGrip;

	b2Body* gripperSensorBody= world->CreateBody(&sensorbd);
	objectCount++;
	gripperSensorBody->CreateFixture(&sensorfd);

	while(IsCollision2(returnObjectID(gripperSensorBody) ,targetBodyID))
	{
		b2Vec2 triedGripperPositionVector=gripperSensorBody->GetPosition()-incrementVector;
		gripperSensorBody->SetTransform(triedGripperPositionVector,(float32)desiredAngle);
	}

	int acceptedCodes[2]={1,2};
	int collidedBodyID;
	if(!collisionCheckForABody(world,returnObjectID(gripperSensorBody),acceptedCodes,2,collidedBodyID)) //place the gripper object. collision check for movable and immovable objects
	{
		b2PolygonShape shape;
		shape.SetAsBox(gripperWidth, gripperHeight);
		b2FixtureDef fd;
		fd.shape = &shape;
		fd.density = 99999.0f;
		fd.friction = 1.0f;
		b2BodyDef bd;
		bd.type = b2_dynamicBody;
		bd.linearDamping=5.0f;
		bd.angularDamping=5.0f;
		MyBodyData* bodyDataGripper = new MyBodyData;
		bodyDataGripper->type=5;
		bodyDataGripper->objectID=31;
		bd.userData=bodyDataGripper;
		bd.position=gripperSensorBody->GetPosition();
		bd.angle=gripperSensorBody->GetAngle();
		gripperObjectBody = m_world->CreateBody(&bd);
		objectCount++;
		gripperObjectBody->CreateFixture(&fd);
		world->DestroyBody(gripperSensorBody);
		objectCount--;
		return true;
	}
	else
	{
		world->DestroyBody(gripperSensorBody);
		objectCount--;
		return false;
	}
	return true;
}




void Test::makeAllVelocitiesZero(b2World *world)
{
	b2Body* bodyList=world->GetBodyList();
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{

		b2Vec2 zeroVector(0.0,0.0);
		a->SetAngularVelocity(0.0);
		a->SetLinearVelocity(zeroVector);
	}
}

int Test::getNumberOfObjectsInSensorField(int sensorFieldID)
{
	int count=0;
	b2Body* bodyList=m_world->GetBodyList();
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type==1 || data->type==2)
			{
				if(IsCollision2(sensorFieldID,data->objectID))
				{
					count++;
				}
			}
		}
	}
	return count;
}

bool Test::IsCollision(b2Body* bodyA, b2Body* bodyB, int indexA, int indexB)
{
	bool collisionResult=false;

	b2Fixture* fixtureA=bodyA->GetFixtureList();
	b2PolygonShape* shapeA = (b2PolygonShape*)(fixtureA->GetShape());

	b2Fixture* fixtureB=bodyB->GetFixtureList();
	b2PolygonShape* shapeB = (b2PolygonShape*)(fixtureB->GetShape());

	if(shapeA->GetType()==b2Shape::e_polygon && shapeB->GetType()==b2Shape::e_polygon)
	{
		collisionResult=b2TestOverlap(shapeA,indexA, shapeB,indexB,bodyA->GetTransform(),bodyB->GetTransform());
	}
	return collisionResult;
}


bool Test::IsCollision2(int objectIDbodyA, int objectIDbodyB)
{
	bool collisionResult=false;

	b2Body* bodyA;
	b2Body* bodyB;
	receiveBodyWithObjectID(objectIDbodyA,bodyA);
	receiveBodyWithObjectID(objectIDbodyB,bodyB);

	b2Fixture* fixtureA=bodyA->GetFixtureList();
	b2Shape* shapeAA=fixtureA->GetShape();

	if(shapeAA->GetType()==b2Shape::e_polygon)
	{
		b2PolygonShape* shapeA = (b2PolygonShape*)(fixtureA->GetShape());

		b2Fixture* fixtureB=bodyB->GetFixtureList();
		b2Shape* shapeBB=fixtureB->GetShape();
		if(shapeBB->GetType()==b2Shape::e_polygon)
		{
			b2PolygonShape* shapeB = (b2PolygonShape*)(fixtureB->GetShape());
			collisionResult=b2TestOverlap(shapeA,0, shapeB,0,bodyA->GetTransform(),bodyB->GetTransform());
		}
		else if(shapeBB->GetType()==b2Shape::e_edge || shapeBB->GetType()==b2Shape::e_loop)
		{
			b2EdgeShape* shapeB=(b2EdgeShape*)(fixtureB->GetShape());
			collisionResult=b2TestOverlap(shapeA,0, shapeB,0,bodyA->GetTransform(),bodyB->GetTransform());
		}

	}
	else if(shapeAA->GetType()==b2Shape::e_edge || shapeAA->GetType()==b2Shape::e_loop)
	{

		b2EdgeShape* shapeA=(b2EdgeShape*)(fixtureA->GetShape());

		b2Fixture* fixtureB=bodyB->GetFixtureList();
		b2Shape* shapeBB=fixtureB->GetShape();
		if(shapeBB->GetType()==b2Shape::e_polygon)
		{
			b2PolygonShape* shapeB = (b2PolygonShape*)(fixtureB->GetShape());
			collisionResult=b2TestOverlap(shapeA,0, shapeB,0,bodyA->GetTransform(),bodyB->GetTransform());
		}
		else if(shapeBB->GetType()==b2Shape::e_edge || shapeBB->GetType()==b2Shape::e_loop)
		{
			b2EdgeShape* shapeB=(b2EdgeShape*)(fixtureB->GetShape());
			collisionResult=b2TestOverlap(shapeA,0, shapeB,0,bodyA->GetTransform(),bodyB->GetTransform());
		}
	}
	return collisionResult;
}

void Test::printAllCollisions(b2World *currentWorld)
{
	b2Body* bodyList=currentWorld->GetBodyList();
	int currentObjectCount=currentWorld->GetBodyCount()-1;
	cout<<"currentObjectCount: "<<currentObjectCount<<endl;
	b2Body* bodyList2[currentObjectCount];
	int kk=0;
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		bodyList2[kk]=a;
		kk++;
	}

	for(int i=0;i<currentObjectCount;i++)
	{
		for(int j=0;j<currentObjectCount;j++)
		{
			if(i!=j)
			{
				if(IsCollision2(returnObjectID(bodyList2[i]),returnObjectID(bodyList2[j])))
				{
					cout<<"Body "<<returnObjectID(bodyList2[i])<<" collides Body "<<returnObjectID(bodyList2[j])<<endl;
				}
			}
		}
	}
}

bool Test::collisionCheckForABody(b2World *currentWorld,int bodyAobjectID, int acceptedObjectCodes[], int acceptedObjectCodesSize,int &bodyHitObjectID) //doesn't look to the borders,grippers, sensor fields for collision check. Returns true if collision
{
	b2Body* bodyList=currentWorld->GetBodyList();
	int currentObjectCount=currentWorld->GetBodyCount()-1;
	//cout<<"currentObjectCount: "<<currentObjectCount<<endl;
	b2Body* bodyList2[currentObjectCount];
	int kk=0;
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{

		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();

			bool objectAccepted=false;
			for(int p=0;p<acceptedObjectCodesSize;p++)
			{
				if(data->type == acceptedObjectCodes[p])
				{
					objectAccepted=true;
					break;
				}
			}
			if(objectAccepted)
			{
				bodyList2[kk]=a;
				kk++;
			}
		}
		else
		{
			bodyList2[kk]=a;
			kk++;
		}
	}

	if(kk>0)
	{
		for(int i=0;i<(kk-1);i++)
		{
			if(returnObjectID(bodyList2[i])!=bodyAobjectID)
			{
				if(IsCollision2(returnObjectID(bodyList2[i]),bodyAobjectID))
				{
					bodyHitObjectID=returnObjectID(bodyList2[i]);
					return true;
				}
			}
		}
	}
	return false;

}

bool Test::collisionCheckForAllImmovablesOnImmovables(b2World *currentWorld,  int &bodyHitObjectID)
{
	b2Body* bodyList=currentWorld->GetBodyList();
	int currentObjectCount=currentWorld->GetBodyCount()-1;
	//cout<<"currentObjectCount: "<<currentObjectCount<<endl;
	b2Body* bodyList2[currentObjectCount];
	b2Body* bodyListImmovables[currentObjectCount];

	int kk=0;
	int ll=0;
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 2) //this object is an immovable
			{
				bodyList2[kk]=a;
				kk++;
			}
			if(data->type == 2) //this object is an immovable
			{
				bodyListImmovables[ll]=a;
				ll++;
			}

		}
	}

	for(int i=0;i<(kk);i++)
	{
		for(int j=0;j<ll;j++)
		{
			if(bodyList2[i]!=bodyListImmovables[j])
			{
				if(IsCollision2(returnObjectID(bodyList2[i]),returnObjectID(bodyListImmovables[j])))
				{
					bodyHitObjectID=returnObjectID(bodyListImmovables[j]);
					return true;
				}
			}
		}
	}
	return false;
}


bool Test::collisionCheckForAllImmovableBodies(b2World *currentWorld,  int &bodyHitObjectID)
{
	b2Body* bodyList=currentWorld->GetBodyList();
	int currentObjectCount=currentWorld->GetBodyCount()-1;
	//cout<<"currentObjectCount: "<<currentObjectCount<<endl;
	b2Body* bodyList2[currentObjectCount];
	b2Body* bodyListImmovables[currentObjectCount];

	int kk=0;
	int ll=0;
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();


			//if(data->type == 1 || data->type == 6) //this object is regular movable or sensor body
			if(data->type == 1) //this object is regular movable or sensor body
			{
				bodyList2[kk]=a;
				kk++;
			}
			else if(data->type == 2) //this object is an immovable
			{
				bodyListImmovables[ll]=a;
				ll++;
			}

		}
	}

	for(int i=0;i<(kk);i++)
	{
		for(int j=0;j<ll;j++)
		{
			if(bodyList2[i]!=bodyListImmovables[j])
			{
				if(IsCollision2(returnObjectID(bodyList2[i]),returnObjectID(bodyListImmovables[j])))
				{
					bodyHitObjectID=returnObjectID(bodyListImmovables[j]);
					return true;
				}
			}
		}
	}
	return false;
}

bool Test::GetRandomObjectPlacementOnTable()
{
	//cout<<"Generating random object locations"<<endl;
	b2Body* bodyList=m_world->GetBodyList();
	vector<int> tableBodyObjectIDVector;
	vector<int> placedTableBodyObjectIDVector;

	int bodyHitObjectID;
	int acceptedCodesForSensorFields[2]={1, 2};

	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if((data->type == 1) || (data->type == 2)) //sensor field
			{
				tableBodyObjectIDVector.push_back(data->objectID);
			}
		}
	}
	//cout<<"# of sensors: "<<sensorBodyObjectIDVector.size()<<endl;
	bool collision=true;
	while(collision)
	{
		collision=false;
		for(unsigned int ii=0;ii<tableBodyObjectIDVector.size();ii++)
		{
			bool collisionFreeTablesImmovables=false;
			while(!collisionFreeTablesImmovables)
			{
				int x = (int)RandomFloat(minx, maxx);
				//wait(0.05);
				int y = (int)RandomFloat(miny, maxy);

				float radians = RandomFloat(-M_PI,M_PI);

				b2Body* currentTableBody;
				receiveBodyWithObjectID(tableBodyObjectIDVector[ii],currentTableBody);
				currentTableBody->SetTransform(b2Vec2(x, y), radians);

				//updateScreenPhysics(true);

				if(collisionCheckForAllImmovableBodies(m_world, bodyHitObjectID) == true)
				{
					//cout<<"Sensor Body place in collision with immovable object"<<endl;
				}
				else if(collisionCheckForAllImmovablesOnImmovables(m_world,bodyHitObjectID) == true)
				{

				}
				else if(collisionCheckForAllTableBorders(m_world, bodyHitObjectID) == true)
				{
					//cout<<"Sensor Body placed in collision with table borders"<<endl;
				}
				else if(collisionCheckForABody(m_world,tableBodyObjectIDVector[ii],acceptedCodesForSensorFields,1,bodyHitObjectID))
				{
					//cout<<"Sensor Body collides with others"<<endl;
					bool passed=true;
					for(unsigned int jj=0;jj<placedTableBodyObjectIDVector.size();jj++)
					{
						if(placedTableBodyObjectIDVector[jj]==bodyHitObjectID)
						{
							passed=false;
							break;
						}
					}
					if(passed)
					{
						collisionFreeTablesImmovables=true;
						placedTableBodyObjectIDVector.push_back(tableBodyObjectIDVector[ii]);
					}

				}
				else
				{
					collisionFreeTablesImmovables=true;
					placedTableBodyObjectIDVector.push_back(tableBodyObjectIDVector[ii]);
				}
			}
		}
	}


	return true;
}


bool Test::GetRandomPlacementLocation()
{
	//cout<<"Generating random object locations"<<endl;
	b2Body* bodyList=m_world->GetBodyList();
	vector<int> sensorBodyObjectIDVector;
	vector<int> placedSensorBodyObjectIDVector;

	int bodyHitObjectID;
	int acceptedCodesForSensorFields[1]={6};

	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 6) //sensor field
			{
				sensorBodyObjectIDVector.push_back(data->objectID);
			}
		}
	}
	//cout<<"# of sensors: "<<sensorBodyObjectIDVector.size()<<endl;
	bool collision=true;
	while(collision)
	{
		collision=false;
		for(unsigned int ii=0;ii<sensorBodyObjectIDVector.size();ii++)
		{
			bool collisionFreeTablesImmovables=false;
			while(!collisionFreeTablesImmovables)
			{
				int x = (int)RandomFloat(minx, maxx);
				//wait(0.05);
				int y = (int)RandomFloat(miny, maxy);

				float radians = RandomFloat(-M_PI,M_PI);

				b2Body* currentSensorBody;
				receiveBodyWithObjectID(sensorBodyObjectIDVector[ii],currentSensorBody);
				currentSensorBody->SetTransform(b2Vec2(x, y), radians);

				//updateScreenPhysics(true);

				if(collisionCheckForAllImmovableBodies(m_world, bodyHitObjectID) == true)
				{
					//cout<<"Sensor Body place in collision with immovable object"<<endl;
				}
				else if(collisionCheckForAllTableBorders(m_world, bodyHitObjectID) == true)
				{
					//cout<<"Sensor Body placed in collision with table borders"<<endl;
				}
				else if(collisionCheckForABody(m_world,sensorBodyObjectIDVector[ii],acceptedCodesForSensorFields,1,bodyHitObjectID))
				{
					//cout<<"Sensor Body collides with others"<<endl;
					bool passed=true;
					for(unsigned int jj=0;jj<placedSensorBodyObjectIDVector.size();jj++)
					{
						if(placedSensorBodyObjectIDVector[jj]==bodyHitObjectID)
						{
							passed=false;
							break;
						}
					}
					if(passed)
					{
						collisionFreeTablesImmovables=true;
						placedSensorBodyObjectIDVector.push_back(sensorBodyObjectIDVector[ii]);
					}

				}
				else
				{
					collisionFreeTablesImmovables=true;
					placedSensorBodyObjectIDVector.push_back(sensorBodyObjectIDVector[ii]);
				}
			}
		}
	}


	return true;
}

bool Test::collisionCheckForAllTableBorders(b2World *currentWorld,  int &bodyHitObjectID)
{
	b2Body* bodyList=currentWorld->GetBodyList();
	int currentObjectCount=currentWorld->GetBodyCount()-1;
	//cout<<"currentObjectCount: "<<currentObjectCount<<endl;
	b2Body* bodyList2[currentObjectCount];
	b2Body* bodyListBorders[currentObjectCount];

	int kk=0;
	int ll=0;
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 9) //border
			{
				//cout<<"added to border list"<<endl;
				bodyListBorders[ll]=a;
				ll++;
				//bodyList2[kk]=a;
				//kk++;
			}
			else
			{
				if(data->type == 2 || data->type == 1 || data->type == 6) //movable or immovable object or sensor object
				{
					bodyList2[kk]=a;
					kk++;
				}
			}
		}
		else
		{
			bodyList2[kk]=a;
			kk++;
		}
	}

	for(int i=0;i<(kk-1);i++)
	{
		for(int j=0;j<ll;j++)
		{
			if(bodyList2[i]!=bodyListBorders[j])
			{
				if(IsCollision2(returnObjectID(bodyList2[i]),returnObjectID(bodyListBorders[j])))
				{
					bodyHitObjectID=returnObjectID(bodyListBorders[j]);
					return true;
				}
			}
		}
	}
	return false;
}

bool Test::collisionCheckForAllSensorBodies(b2World *currentWorld,  int &bodyHitObjectID)
{
	b2Body* bodyList=currentWorld->GetBodyList();
	int currentObjectCount=currentWorld->GetBodyCount()-1;
	//cout<<"currentObjectCount: "<<currentObjectCount<<endl;
	b2Body* bodyList2[currentObjectCount];
	b2Body* bodyListBorders[currentObjectCount];

	int kk=0;
	int ll=0;
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 6) // sensor object
			{
				bodyListBorders[ll]=a; //cout<<"added to border list"<<endl;
				ll++;
			}
		}
		else
		{
			bodyList2[kk]=a;
			kk++;
		}
	}

	for(int i=0;i<(ll);i++)
	{
		for(int j=0;j<ll;j++)
		{
			//if(bodyList2[i]!=bodyListBorders[j])
			if(bodyListBorders[i] != bodyListBorders[j])
			{
				if(IsCollision2(returnObjectID(bodyListBorders[i]),returnObjectID(bodyListBorders[j])))
				{
					bodyHitObjectID=returnObjectID(bodyListBorders[j]);
					return true;
				}
			}
		}
	}
	return false;
}

void Test::saveWorldState(worldState &worldStateToBeSaved)
{
	m_world->ClearForces();
	makeAllVelocitiesZero(m_world);

	worldStateToBeSaved.b2BodyDefVector.clear();
	worldStateToBeSaved.b2FixtureDefVector.clear();
	worldStateToBeSaved.b2PolygonShapeVector.clear();
	worldStateToBeSaved.MyBodyDataVector.clear();

	b2Body* bodyList = m_world->GetBodyList();

	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 1 || data->type == 2 || data->type == 5 || data->type == 6)
			{
				b2Body* currentBodyToLoad=a;
				b2BodyDef tempBodyDef;
				tempBodyDef.active=currentBodyToLoad->IsActive();
				tempBodyDef.allowSleep=currentBodyToLoad->IsSleepingAllowed();
				tempBodyDef.angle=currentBodyToLoad->GetAngle();
				tempBodyDef.angularDamping=currentBodyToLoad->GetAngularDamping();

				tempBodyDef.angularVelocity=currentBodyToLoad->GetAngularVelocity();
				tempBodyDef.linearVelocity=currentBodyToLoad->GetLinearVelocity();

				//tempBodyDef.angularVelocity=0;
				//tempBodyDef.linearVelocity=b2Vec2(0.0,0.0);

				tempBodyDef.awake=currentBodyToLoad->IsAwake();
				tempBodyDef.bullet=currentBodyToLoad->IsBullet();
				tempBodyDef.fixedRotation=currentBodyToLoad->IsFixedRotation();
				//tempBodyDef.inertiaScale=currentBodyToLoad->GetInertia(); //unnecessary?
				tempBodyDef.linearDamping=currentBodyToLoad->GetLinearDamping();

				tempBodyDef.position=currentBodyToLoad->GetPosition();
				tempBodyDef.type=currentBodyToLoad->GetType();

				b2FixtureDef tempFixtureDef;
				b2Fixture* fixtureToLoad = currentBodyToLoad->GetFixtureList();

				MyBodyData tempData;
				tempData.type=data->type;
				tempData.objectID=data->objectID;

				worldStateToBeSaved.MyBodyDataVector.push_back(tempData);

				tempFixtureDef.density=fixtureToLoad->GetDensity();
				tempFixtureDef.isSensor=fixtureToLoad->IsSensor();
				tempFixtureDef.restitution=fixtureToLoad->GetRestitution();
				tempFixtureDef.friction=fixtureToLoad->GetFriction();
				//tempFixtureDef.filter=fixtureToLoad->GetFilterData(); //gives error when uncommented

				b2PolygonShape* shapeToLoad = (b2PolygonShape*)(fixtureToLoad->GetShape());
				b2PolygonShape tempShape;
				tempShape.m_centroid=shapeToLoad->m_centroid;

				for(int ff=0;ff<(b2_maxPolygonVertices);ff++)
				{
					tempShape.m_normals[ff]=shapeToLoad->m_normals[ff];
					tempShape.m_vertices[ff]=shapeToLoad->m_vertices[ff];
				}
				tempShape.m_radius=shapeToLoad->m_radius;
				tempShape.m_type=shapeToLoad->m_type;
				tempShape.m_vertexCount=shapeToLoad->m_vertexCount;

				worldStateToBeSaved.b2BodyDefVector.push_back(tempBodyDef);
				worldStateToBeSaved.b2FixtureDefVector.push_back(tempFixtureDef);
				worldStateToBeSaved.b2PolygonShapeVector.push_back(tempShape);
			}
		}
	}
}


void Test::loadWorldState(worldState worldStateToBeLoaded)
{
	b2Body* bodyList=m_world->GetBodyList();

	for (b2Body* a = bodyList; a; a = a->GetNext()) // Apocalypse!
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 1 || data->type == 2 || data->type == 5 || data->type == 6)
			{
				m_world->DestroyBody(a);
				objectCount--;
			}
		}
	}

	for(unsigned int ii=0;ii<worldStateToBeLoaded.b2BodyDefVector.size();ii++)
	{
		b2BodyDef tempBodyDef=worldStateToBeLoaded.b2BodyDefVector[ii];
		MyBodyData tempMyBodyData=worldStateToBeLoaded.MyBodyDataVector[ii];

		MyBodyData* bodyDataZ = new MyBodyData;
		bodyDataZ->type=tempMyBodyData.type;
		bodyDataZ->objectID=tempMyBodyData.objectID;
		tempBodyDef.userData=bodyDataZ;

		b2FixtureDef tempFixtureDef=worldStateToBeLoaded.b2FixtureDefVector[ii];
		b2PolygonShape tempPolygonShape=worldStateToBeLoaded.b2PolygonShapeVector[ii];
		tempFixtureDef.shape=&tempPolygonShape;

		b2Body* tempBody;
		tempBody = m_world->CreateBody(&tempBodyDef);
		objectCount++;
		tempBody->CreateFixture(&tempFixtureDef);
	}
}





float Test::GetMinPushAngle(int objectIDSensor, int objectIDpushedBody, float angleResolution,bool allowCollisionsWithRegularObjects,float pushForce, int &aFirstContactObjectID)
{

	vector<int> firstContactsBodyIDVector;
	float pushDistance;
	float minPushDistance = 99999999999.00;
	float minAngle = 0.00;
	int errorCode;
	int collidedBodyID;
	int firstContactBodyID;
	worldState worldStateToBeSaved;
	//cout<<"In GetMinPushAngle"<<endl;

	bool minAngleInitialized=false;

	saveWorldState(worldStateToBeSaved);


	b2Body* pushBody;
	b2Body* sensorFieldBody;

	receiveBodyWithObjectID(objectIDSensor,sensorFieldBody);
	receiveBodyWithObjectID(objectIDpushedBody,pushBody);

	//cout<<" "<<endl;
	//wait(0.1);
	if(sensorFieldBody->GetFixtureList()->IsSensor() == true)
	{
		//cout<<"sensor"<<endl;
		for(float angle = -M_PI; angle < M_PI; angle += angleResolution)
		{

			receiveBodyWithObjectID(objectIDpushedBody,pushBody);
			receiveBodyWithObjectID(objectIDSensor,sensorFieldBody);

			float bodyAngle = pushBody->GetAngle();
			float actualPushAngle=bodyAngle+angle;

			if(actualPushAngle>M_PI)
			{
				actualPushAngle=actualPushAngle-2*M_PI;
			}
			else if(actualPushAngle<-M_PI)
			{
				actualPushAngle=actualPushAngle+2*M_PI;
			}

			//cout<<"actualPushAngle: "<<actualPushAngle<<endl;
			pushDistance = PushWithGripperGeneral(m_world, objectIDpushedBody, pushForce, actualPushAngle, errorCode, allowCollisionsWithRegularObjects, true, objectIDSensor, collidedBodyID,firstContactBodyID);

			//cout<<"Angle: "<<angle<<" ,Dist:"<<pushDistance<<endl;
			if(errorCode == 7)
			{
				//cout<<"Angle: "<<angle<<" ,Dist:"<<pushDistance<<endl;
				if(pushDistance < minPushDistance)
				{
					minPushDistance=pushDistance;
					minAngle = actualPushAngle;
					minAngleInitialized=true;
				}
			}
			else
			{
				b2Body* firstContactBody;
				if(receiveBodyWithObjectID(firstContactBodyID,firstContactBody))
				{
					if (firstContactBody->GetUserData()!=NULL)
					{
						MyBodyData *data = (MyBodyData*)firstContactBody->GetUserData();
						if(data->type == 1 || data->type == 2)
						{
							bool alreadyExists=false;
							for(unsigned int ff=0;ff<firstContactsBodyIDVector.size();ff++)
							{
								if(firstContactBodyID==firstContactsBodyIDVector[ff])
								{
									alreadyExists=true;
									break;
								}
							}
							if(!alreadyExists)
							{
								firstContactsBodyIDVector.push_back(firstContactBodyID);
							}

						}
					}
				}
			}

			//printErrorCode(errorCode);
			loadWorldState(worldStateToBeSaved);
			//receiveBodyWithObjectID(objectIDpushedBody,pushBody);
			//receiveBodyWithObjectID(objectIDSensor,sensorFieldBody);
		}

		for(unsigned int ff=0;ff<firstContactsBodyIDVector.size();ff++)
		{
			int randomInt=(int)RandomFloat(0.0, (float)(firstContactsBodyIDVector.size())-0.01);
			aFirstContactObjectID=firstContactsBodyIDVector[randomInt];
			//cout<<"Touched objs size: "<<firstContactsBodyIDVector.size()<<" rand: "<<randomInt<<endl;
		}


	}
	else
	{
		cout<<"Input SensorField Body is not a Sensor Field!"<<endl;
	}

	loadWorldState(worldStateToBeSaved);
	if(minAngleInitialized)
	{
		return minAngle;
	}
	else
	{
		//cout<<"returning -31.0"<<endl;
		return -31.0;
	}
}

float Test::GetMaxPushAngle(int objectIDSensor, int objectIDpushedBody, float angleResolution,bool allowCollisionsWithRegularObjects,bool sensorFieldClearOption,float pushForce)
{

	float pushDistance;
	float maxPushDistance = -99999999999.00;
	float maxAngle = 0.00;
	int errorCode;
	int collidedBodyID;
	int firstContactBodyID;
	worldState worldStateToBeSaved;
	//cout<<"In GetMinPushAngle"<<endl;

	bool maxAngleInitialized=false;

	saveWorldState(worldStateToBeSaved);


	b2Body* pushBody;
	b2Body* sensorFieldBody;

	receiveBodyWithObjectID(objectIDSensor,sensorFieldBody);
	receiveBodyWithObjectID(objectIDpushedBody,pushBody);

	//cout<<" "<<endl;
	//wait(0.1);
	if(sensorFieldBody->GetFixtureList()->IsSensor() == true)
	{
		//cout<<"sensor"<<endl;
		for(float angle = -M_PI; angle < M_PI; angle += angleResolution)
		{

			receiveBodyWithObjectID(objectIDpushedBody,pushBody);
			receiveBodyWithObjectID(objectIDSensor,sensorFieldBody);

			float bodyAngle = pushBody->GetAngle();
			float actualPushAngle=bodyAngle+angle;

			if(actualPushAngle>M_PI)
			{
				actualPushAngle=actualPushAngle-2*M_PI;
			}
			else if(actualPushAngle<-M_PI)
			{
				actualPushAngle=actualPushAngle+2*M_PI;
			}

			//cout<<"actualPushAngle: "<<actualPushAngle<<endl;
			pushDistance = PushWithGripperGeneral(m_world, objectIDpushedBody, pushForce, actualPushAngle, errorCode, allowCollisionsWithRegularObjects, sensorFieldClearOption, objectIDSensor, collidedBodyID,firstContactBodyID);

			//cout<<"Angle: "<<actualPushAngle<<" ,Dist:"<<pushDistance<<endl;
			if(sensorFieldClearOption)
			{
				if(errorCode == 7)
				{
					//cout<<"Angle: "<<angle<<" ,Dist:"<<pushDistance<<endl;
					if(pushDistance > maxPushDistance)
					{
						maxPushDistance=pushDistance;
						maxAngle = actualPushAngle;
						maxAngleInitialized=true;
					}
				}
			}
			else
			{
				//cout<<"Angle: "<<angle<<" ,Dist:"<<pushDistance<<endl;
				if(pushDistance > maxPushDistance)
				{
					maxPushDistance=pushDistance;
					maxAngle = actualPushAngle;
					maxAngleInitialized=true;
				}
			}
			//printErrorCode(errorCode);
			loadWorldState(worldStateToBeSaved);
			//receiveBodyWithObjectID(objectIDpushedBody,pushBody);
			//receiveBodyWithObjectID(objectIDSensor,sensorFieldBody);
		}
	}
	else
	{
		cout<<"Input SensorField Body is not a Sensor Field!"<<endl;
	}

	loadWorldState(worldStateToBeSaved);
	if(maxAngleInitialized)
	{
		return maxAngle;
	}
	else
	{
		//cout<<"returning -31.0"<<endl;
		return -31.0;
	}
}


void Test::Step(Settings* settings)
{
	currentSettings=*settings;

	float32 timeStep = settings->hz > 0.0f ? 1.0f / settings->hz : float32(0.0f);

	if (settings->pause)
	{
		if (settings->singleStep)
		{
			settings->singleStep = 0;
		}
		else
		{
			timeStep = 0.0f;
		}

		m_debugDraw.DrawString(5, m_textLine, "****PAUSED****");
		m_textLine += 15;
	}

	uint32 flags = 0;
	flags += settings->drawShapes			* b2DebugDraw::e_shapeBit;
	flags += settings->drawJoints			* b2DebugDraw::e_jointBit;
	flags += settings->drawAABBs			* b2DebugDraw::e_aabbBit;
	flags += settings->drawPairs			* b2DebugDraw::e_pairBit;
	flags += settings->drawCOMs				* b2DebugDraw::e_centerOfMassBit;
	m_debugDraw.SetFlags(flags);

	m_world->SetWarmStarting(settings->enableWarmStarting > 0);
	m_world->SetContinuousPhysics(settings->enableContinuous > 0);
	m_world->SetSubStepping(settings->enableSubStepping > 0);

	m_pointCount = 0;

	m_world->Step(timeStep, settings->velocityIterations, settings->positionIterations);


glClearColor(1.0f,1.0f,1.0f,1.0f);
	m_world->DrawDebugData();
	if(true)
	{
		b2Body* bodyList=m_world->GetBodyList();
		for (b2Body* b = bodyList; b; b = b->GetNext())
		{

			for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext())
			{

				if (b->GetUserData()!=NULL)
				{
					MyBodyData *data = (MyBodyData*)b->GetUserData();
					if(data->type == 1 || data->type == 2|| data->type == 6)
					{
						float factor=13.2;
						float ymargin=2.9;
						int pixelx=(b->GetPosition().x+20.0+ymargin)*factor; //max 640
						int pixely=(480-10.0*(b->GetPosition().y+ymargin));


						char buffer[2];
						myitoa (data->objectID,buffer);
						
						m_debugDraw.DrawString(pixelx,pixely,buffer);
						//glColor3f(1.0f, 1.0f, 1.0f);
						//m_debugDraw.DrawString(100.0,200.0,"AM");
					}
				}
			}
		}
	}


	if (timeStep > 0.0f)
	{
		++m_stepCount;
	}

	if (settings->drawStats)
	{
		m_debugDraw.DrawString(5, m_textLine, "bodies/contacts/joints/proxies = %d/%d/%d",
				m_world->GetBodyCount(), m_world->GetContactCount(), m_world->GetJointCount(), m_world->GetProxyCount());
		m_textLine += 15;
	}

	if (m_mouseJoint)
	{
		b2Vec2 p1 = m_mouseJoint->GetAnchorB();
		b2Vec2 p2 = m_mouseJoint->GetTarget();

		glPointSize(4.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glBegin(GL_POINTS);
		glVertex2f(p1.x, p1.y);
		glVertex2f(p2.x, p2.y);
		glEnd();
		glPointSize(1.0f);

		glColor3f(0.8f, 0.8f, 0.8f);
		glBegin(GL_LINES);
		glVertex2f(p1.x, p1.y);
		glVertex2f(p2.x, p2.y);
		glEnd();
	}

	if (m_bombSpawning)
	{
		glPointSize(4.0f);
		glColor3f(0.0f, 0.0f, 1.0f);
		glBegin(GL_POINTS);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex2f(m_bombSpawnPoint.x, m_bombSpawnPoint.y);
		glEnd();

		glColor3f(0.8f, 0.8f, 0.8f);
		glBegin(GL_LINES);
		glVertex2f(m_mouseWorld.x, m_mouseWorld.y);
		glVertex2f(m_bombSpawnPoint.x, m_bombSpawnPoint.y);
		glEnd();
	}







	if (settings->drawContactPoints)
	{
		//const float32 k_impulseScale = 0.1f;
		const float32 k_axisScale = 0.3f;

		for (int32 i = 0; i < m_pointCount; ++i)
		{
			ContactPoint* point = m_points + i;

			if (point->state == b2_addState)
			{
				// Add
				m_debugDraw.DrawPoint(point->position, 10.0f, b2Color(0.3f, 0.95f, 0.3f));
			}
			else if (point->state == b2_persistState)
			{
				// Persist
				m_debugDraw.DrawPoint(point->position, 5.0f, b2Color(0.3f, 0.3f, 0.95f));
			}

			if (settings->drawContactNormals == 1)
			{
				b2Vec2 p1 = point->position;
				b2Vec2 p2 = p1 + k_axisScale * point->normal;
				m_debugDraw.DrawSegment(p1, p2, b2Color(0.9f, 0.9f, 0.9f));
			}
			else if (settings->drawContactForces == 1)
			{
				//b2Vec2 p1 = point->position;
				//b2Vec2 p2 = p1 + k_forceScale * point->normalForce * point->normal;
				//DrawSegment(p1, p2, b2Color(0.9f, 0.9f, 0.3f));
			}

			if (settings->drawFrictionForces == 1)
			{
				//b2Vec2 tangent = b2Cross(point->normal, 1.0f);
				//b2Vec2 p1 = point->position;
				//b2Vec2 p2 = p1 + k_forceScale * point->tangentForce * tangent;
				//DrawSegment(p1, p2, b2Color(0.9f, 0.9f, 0.3f));
			}
		}
	}

}

bool Test::receiveBodyWithObjectID(int requestedObjID,b2Body* &rcvdBody)
{
	b2Body* bodyList=m_world->GetBodyList();
	rcvdBody=NULL;
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->objectID == requestedObjID)
			{
				rcvdBody=a;
				return true;
			}
		}
	}
	return false;
}

bool Test::confirmIfMovableOrImmovableFromObjectID(int requestedObjID)
{
	b2Body* bodyList=m_world->GetBodyList();
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->objectID == requestedObjID)
			{
				if(data->type==1 || data->type==2)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	return false;
}


int Test::returnObjectID(b2Body* rcvdBody)
{
	if (rcvdBody->GetUserData()!=NULL)
	{
		MyBodyData *data = (MyBodyData*)rcvdBody->GetUserData();
		return (data->objectID);
	}
	else
	{
		return -31;
	}
}

void Test::printErrorCode(int errorcode)
{
	if (errorcode < 0)
		return;
	cout<<"ERRORCODE: ";
	if(errorcode==0)
	{
		cout<<"No space for gripper!"<<endl;
	}
	else if(errorcode==1)
	{
		cout<<"Hit to an immovable object!"<<endl;
	}
	else if(errorcode==2)
	{
		cout<<"Something touched the table edge"<<endl;
	}
	else if(errorcode==3)
	{
		cout<<"Hit to a movable object"<<endl;
	}
	else if(errorcode==4)
	{
		cout<<"Provided sensor field is not a sensor field!"<<endl;
	}
	else if(errorcode==5)
	{
		cout<<"Object must be in the sensor field to escape!"<<endl;
	}
	else if(errorcode==6)
	{
		cout<<"The pushed object slipped!"<<endl;
	}
	else if(errorcode==7)
	{
		cout<<"Great news. Object got out of sensor field!"<<endl;
	}
}
void Test::myitoa(int n, char s[])
{
	int i, sign;

	if ((sign = n) < 0)  /* record sign */
		n = -n;          /* make n positive */
	i = 0;
	do {       /* generate digits in reverse order */
		s[i++] = n % 10 + '0';   /* get next digit */
	} while ((n /= 10) > 0);     /* delete it */
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}
void Test::reverse(char s[])
{
	int i, j;
	char c;

	for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void Test::retrieveCurrentSensorBodyIDVector(b2World *currentWorld,vector<int> &sensorBodyIDVector)
{
	sensorBodyIDVector.clear();
	b2Body* bodyList=currentWorld->GetBodyList();
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 6) //sensor field
			{
				sensorBodyIDVector.push_back(data->objectID);
			}
		}
	}
}
void Test::convertSensorBodyToMovableBody(int sensorBodyID)
{
	static bool alternate=true;
	
	b2Body* sensorBody;
	receiveBodyWithObjectID(sensorBodyID,sensorBody);
	//wait(0.1);
	sensorBody->GetFixtureList()->SetSensor(false);
	makeAllVelocitiesZero(m_world);
	MyBodyData *data = (MyBodyData*)sensorBody->GetUserData();
//	data->type=1;

	if(alternate)
	{
	alternate=false;
	data->type=1;
	}
	else
	{
	alternate=true;
	data->type=2;
	}

}

void Test::getObjectPlacementPose(int inquiredSensorBodyID, bool new_environment)
{
	b2Body* sensor_body;
	receiveBodyWithObjectID(inquiredSensorBodyID,sensor_body);

	if (new_environment)
	{
		// Put information defining the current environment into the service
		// request
		TabletopEnvironment te;
		ps_.scores.clear();
		ps_.poses.clear();

		te.table.x_size = box2DxToCVx(maxx) - box2DxToCVx(minx);
		te.table.y_size = box2DyToCVy(maxy) - box2DyToCVy(miny);

		int obj_count = 0;
		b2Body* bodyList=m_world->GetBodyList();
		for (b2Body* a = bodyList; a; a = a->GetNext())
		{
			if (a->GetUserData()!=NULL)
			{
				MyBodyData *data = (MyBodyData*)a->GetUserData();
				if(data->type == 1 || data->type == 2)
				{
					obj_count++;
					// Convert valid objects into the ROS msg data type
					TabletopObject current_obj;
					b2Vec2 currentPose = a->GetPosition();
					current_obj.pose.x = box2DxToCVx(currentPose.x);
					current_obj.pose.y = box2DyToCVy(currentPose.y);
					current_obj.pose.theta = a->GetAngle();
					current_obj.is_oval = false;

					b2Fixture* fixtureToLoad = a->GetFixtureList();
					b2PolygonShape* shapeToLoad =
							(b2PolygonShape*)(fixtureToLoad->GetShape());
					for(int ff=0; ff < shapeToLoad->m_vertexCount; ff++)
					{
						b2Vec2 currentVertex=shapeToLoad->m_vertices[ff];
						geometry_msgs::Point p;
						p.x = box2DxToCVx(currentVertex.x + currentPose.x);
						p.y = box2DyToCVy(currentVertex.y + currentPose.y);
						current_obj.vertices.push_back(p);
					}
					te.objects.push_back(current_obj);
				}
			}
		}
		// Convert the current sensor body to the ROS msg type for the service
		// request
		TabletopObject to_place;
		// Get the current verticies from the sensor body
		b2Fixture* fixtureToLoad = sensor_body->GetFixtureList();
		b2PolygonShape* shapeToLoad =
				(b2PolygonShape*)(fixtureToLoad->GetShape());
		b2Vec2 currentPose = sensor_body->GetPosition();

		for(int ff=0; ff < shapeToLoad->m_vertexCount; ff++)
		{
			b2Vec2 currentVertex = shapeToLoad->m_vertices[ff];
			geometry_msgs::Point p;
			p.x = box2DxToCVx(currentVertex.x + currentPose.x);
			p.y = box2DyToCVy(currentVertex.y + currentPose.y);
			to_place.vertices.push_back(p);
		}

		PlacementHeuristic ph;
		ph.request.te = te;
		ph.request.to_place = to_place;

		if (!placement_client_.call(ph))
		{
			std::cerr << "Error calling placement service!" << std::endl;
		}

		// Save the results
		ps_ = ph.response.ps;
                clutter_percent_ = ph.response.clutter_percent;
                object_free_space_ = ph.response.object_free_space;
                // std::cout << "Object/Free Space %: " << object_free_space_
                //           << std::endl;
                // std::cout << "Clutter %: " << clutter_percent_
                //           << std::endl;

	}
	int pose_idx = 0;


	// Sample from the proposed distribution of poses
	if (!new_environment || force_cdf_change_)
	{
		float cdf_goal = rand()/static_cast<float>(RAND_MAX);
		// Binary search to find smallest value greater than cdf_goal
		pose_idx = cdfBinarySearch(ps_.scores, cdf_goal);
#ifdef DEBUG_POSE_SAMPLING
		ROS_INFO_STREAM("Goal val is: " << cdf_goal);
		ROS_INFO_STREAM("Returned val is: " << ps_.scores[pose_idx]);
		ROS_INFO_STREAM("Returned idx is: " << pose_idx);
		ROS_INFO_STREAM("Val @ idx-1 is : " << ps_.scores[max(pose_idx-1, 0)]);
		ROS_INFO_STREAM("Val @ idx+1 is : " << ps_.scores[min(pose_idx+1,
				(int)ps_.scores.size())]);
#endif // DEBUG_POSE_SAMPLING
	}

	geometry_msgs::Pose2D pn = ps_.poses[pose_idx];
	//float score = ps_.scores[pose_idx];
	/*ps_.poses.erase(ps_.poses.begin()+pose_idx);*/
	/*ps_.scores.erase(ps_.scores.begin()+pose_idx);*/

#ifdef DEBUG_POSE_SAMPLING
	std::cout << "Giving pose of: (" << pn.x << ", " << pn.y << ", "
			<< pn.theta << ")" << std::endl;
	//std::cout << "Score is: " << score << std::endl;
	std::cout << "Objects left: " << ps_.poses.size() << std::endl;
#endif // DEBUG_POSE_SAMPLING
	// Set current sample to be the sensor body pose...
	b2Vec2 new_position;
	new_position.x = CVxToBox2Dx(pn.x);
	new_position.y = CVyToBox2Dy(pn.y);
	sensor_body->SetTransform(new_position, pn.theta);
}


bool Test::planPushes(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce) //look to the sensor fields, plan to escape them.
{

	showGraphicsPlannerInternal=true;

	bool modifiedBestFirstSearch=true;

	worldState initialWorldState;
	saveWorldState(initialWorldState);
	int acceptedCodes[1]={1};

	vector<int> sensorBodyIDVector;


	int numCollidedBodies = 0;
	int errorCode;
	int collidedBody2ID;

	bool solutionFound=false;
	int counter=0;

	while(true)
	{
		if(counter>17)
		{
			cout<<"NO SOLUTION!"<<endl;
			return false;
		}
		else
		{
			counter++;
		}
		cout<<counter<<" ";

		retrieveCurrentSensorBodyIDVector(m_world,sensorBodyIDVector);

		/*
		cout<<"Sensors: ";
		for(int kk=0;kk<sensorBodyIDVector.size();kk++)
		{
			cout<<sensorBodyIDVector[kk]<<",";
		}
		cout<<endl;
		 */

		if(sensorBodyIDVector.empty())
		{
			cout<<"NO SENSOR FIELDS LEFT. SOLUTION FOUND"<<endl;
			solutionFound=true;
			break;
			//return true;
		}
		else
		{
			//cout<<"sensorBodyIDVector size: "<<sensorBodyIDVector.size()<<endl;
		}
		for(unsigned int ii=0;ii<sensorBodyIDVector.size();ii++)
		{
			//cout<<"Sensor ID: "<<sensorBodyIDVector[ii]<<endl;

			int collidedBodyID;
			int firstContactBodyID;
			if(collisionCheckForABody(m_world,sensorBodyIDVector[ii],acceptedCodes,1,collidedBodyID))
			{
				//cout<<"Collided Object, ID: "<<collidedBodyID<<endl;
				//cout<<"Sensor, ID: "<<sensorBodyIDVector[ii]<<endl;
				int aFirstContactObjectID;
				float minAngle=GetMinPushAngle(sensorBodyIDVector[ii],collidedBodyID,angleResolutionForPushSearch,allowCollisionsWithRegularObjects,pushForce,aFirstContactObjectID);
				//cout<<"After GetMinPushAngle"<<endl;

				if(abs(minAngle+31.0)<0.1)
				{
					if(!modifiedBestFirstSearch)
					{
						cout<<"NO SOLUTION!"<<endl;
						return false;
					}
					else
					{
						b2Body* aFirstContactBody;
						if(receiveBodyWithObjectID(aFirstContactObjectID,aFirstContactBody)==true)
						{
							cout<<"Received a first contact body, ID:"<<aFirstContactObjectID<<endl;
							float maxAngle=GetMaxPushAngle(sensorBodyIDVector[ii],aFirstContactObjectID,angleResolutionForPushSearch,allowCollisionsWithRegularObjects,false,pushForce);
							//cout<<"maxAngle: "<<maxAngle<<endl;
							if(abs(maxAngle+31.0)<0.1)
							{
								cout<<"NO SOLUTION!"<<endl;

								return false;
							}
							else
							{
								PushWithGripperGeneral(m_world, aFirstContactObjectID, pushForce, maxAngle, errorCode, allowCollisionsWithRegularObjects, false, sensorBodyIDVector[ii], collidedBody2ID,firstContactBodyID);
								//cout<<"Pushingg: "<<pushDistance<<endl;
								//printErrorCode(errorCode);
								CompletePlan[numCollidedBodies].pushMotion = true;
								CompletePlan[numCollidedBodies].collidedBodyID = aFirstContactObjectID;
								CompletePlan[numCollidedBodies].pushAngle = maxAngle;
								CompletePlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDVector[ii];
								CompletePlan[numCollidedBodies].convertSensorBodyAfterwards = false;
								CompletePlan[numCollidedBodies].enableSensorEscapeMode=false;
								numCollidedBodies++;
								ii--; // Added

								if(counter>17) // Added
								{
									cout<<"NO SOLUTION!"<<endl;
									return false;
								}
								else
								{
									counter++;
								}
								//break;
							}
						}
					}
				}

				if(abs(minAngle+31.0)>0.1) // Added
				{
					PushWithGripperGeneral(m_world, collidedBodyID, pushForce, minAngle, errorCode, allowCollisionsWithRegularObjects, true, sensorBodyIDVector[ii], collidedBody2ID,firstContactBodyID);

					//printErrorCode(errorCode);


					CompletePlan[numCollidedBodies].pushMotion = true;
					CompletePlan[numCollidedBodies].collidedBodyID = collidedBodyID;
					CompletePlan[numCollidedBodies].pushAngle = minAngle;
					CompletePlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDVector[ii];
					CompletePlan[numCollidedBodies].convertSensorBodyAfterwards = false;
					CompletePlan[numCollidedBodies].enableSensorEscapeMode=true;
					numCollidedBodies++;
				}

				if(errorCode==7)
				{
					if(!collisionCheckForABody(m_world,sensorBodyIDVector[ii],acceptedCodes,1,collidedBodyID))
					{
						convertSensorBodyToMovableBody(sensorBodyIDVector[ii]);
						CompletePlan[numCollidedBodies-1].convertSensorBodyAfterwards = true;
					}
					break;
				}
			}
			else
			{
				convertSensorBodyToMovableBody(sensorBodyIDVector[ii]);
				updateScreenPhysics(showGraphicsMaster);
				CompletePlan[numCollidedBodies].pushMotion = false;
				CompletePlan[numCollidedBodies].convertSensorBodyAfterwards = true;
				CompletePlan[numCollidedBodies].enableSensorEscapeMode=true;
				CompletePlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDVector[ii];
				numCollidedBodies++;
				break;
			}

		}
	}

	wait(2.0);
	loadWorldState(initialWorldState);
	updateScreenPhysics(showGraphicsMaster);
	cout<<"Executing plan.."<<" # of moves: "<<numCollidedBodies<<endl;
	wait(1.0);

	showGraphicsPlannerInternal=true;
	for(int k = 0; k < numCollidedBodies; k++)
	{
		wait(1.5);
		int firstContactBodyID;

		if(CompletePlan[k].pushMotion == true)
		{
			if(CompletePlan[k].enableSensorEscapeMode==true)
			{
				PushWithGripperGeneral(m_world, CompletePlan[k].collidedBodyID, 8.0, CompletePlan[k].pushAngle, errorCode, true, true, CompletePlan[k].sensorBodyObjectID, collidedBody2ID,firstContactBodyID);
				if(CompletePlan[k].convertSensorBodyAfterwards==true)
				{
					wait(1.5);
					convertSensorBodyToMovableBody(CompletePlan[k].sensorBodyObjectID);

				}
				updateScreenPhysics(showGraphicsMaster);
			}
			else
			{
				PushWithGripperGeneral(m_world, CompletePlan[k].collidedBodyID, 8.0, CompletePlan[k].pushAngle, errorCode, true, CompletePlan[k].enableSensorEscapeMode, CompletePlan[k].sensorBodyObjectID, collidedBody2ID,firstContactBodyID);
				updateScreenPhysics(showGraphicsMaster);
			}

		}
		else if(CompletePlan[k].convertSensorBodyAfterwards==true)
		{
			convertSensorBodyToMovableBody(CompletePlan[k].sensorBodyObjectID);
			updateScreenPhysics(showGraphicsMaster);
		}

	}
	return true;

}


bool Test::planPushesWithConvolution(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce, int sensorBodyIDtoPlan)
{

	showGraphicsPlannerInternal=true;

	bool modifiedBestFirstSearch=true;

	worldState initialWorldState;
	saveWorldState(initialWorldState);
	int acceptedCodes[1]={1};

	vector<int> sensorBodyIDVector;

	int numCollidedBodies = 0;
	int errorCode;
	int collidedBody2ID;

	bool solutionFound=false;
	int counter=0;

	sensorBodyIDVector.push_back(sensorBodyIDtoPlan);

	while(!solutionFound)
	{
		if(counter>7)
		{
			cout<<"NO SOLUTION!"<<endl;
			return false;
		}
		else
		{
			counter++;
		}
		cout<<counter<<" ";

		if(sensorBodyIDVector.empty())
		{
			cout<<"SOLUTION FOUND.."<<endl;
			solutionFound=true;
			break;
		}
		//cout<<"Sensor ID: "<<sensorBodyIDtoPlan<<endl;
		int collidedBodyID;
		int firstContactBodyID;
		if(collisionCheckForABody(m_world,sensorBodyIDtoPlan,acceptedCodes,1,collidedBodyID))
		{
			//cout<<"Collided Object, ID: "<<collidedBodyID<<endl;
			//cout<<"Sensor, ID: "<<sensorBodyIDVector[ii]<<endl;
			int aFirstContactObjectID;
			float minAngle=GetMinPushAngle(sensorBodyIDtoPlan,collidedBodyID,angleResolutionForPushSearch,allowCollisionsWithRegularObjects,pushForce,aFirstContactObjectID);
			//cout<<"After GetMinPushAngle"<<endl;

			if(abs(minAngle+31.0)<0.1)
			{
				if(!modifiedBestFirstSearch)
				{
					cout<<"NO SOLUTION!"<<endl;
					return false;
				}
				else
				{
					b2Body* aFirstContactBody;
					if(receiveBodyWithObjectID(aFirstContactObjectID,aFirstContactBody)==true)
					{
						//cout<<"Received a first contact body, ID:"<<aFirstContactObjectID<<endl;
						float maxAngle=GetMaxPushAngle(sensorBodyIDtoPlan,aFirstContactObjectID,angleResolutionForPushSearch,allowCollisionsWithRegularObjects,false,pushForce);
						//cout<<"maxAngle: "<<maxAngle<<endl;
						if(abs(maxAngle+31.0)<0.1)
						{
							cout<<"NO SOLUTION!"<<endl;
							return false;
						}
						else
						{
							PushWithGripperGeneral(m_world, aFirstContactObjectID, pushForce, maxAngle, errorCode, allowCollisionsWithRegularObjects, false, sensorBodyIDtoPlan, collidedBody2ID,firstContactBodyID);
							//cout<<"Pushingg: "<<pushDistance<<endl;
							//printErrorCode(errorCode);
							CompletePlan[numCollidedBodies].pushMotion = true;
							CompletePlan[numCollidedBodies].collidedBodyID = aFirstContactObjectID;
							CompletePlan[numCollidedBodies].pushAngle = maxAngle;
							CompletePlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDtoPlan;
							CompletePlan[numCollidedBodies].convertSensorBodyAfterwards = false;
							CompletePlan[numCollidedBodies].enableSensorEscapeMode=false;
							numCollidedBodies++;
						}
					}
				}
			}

			if(abs(minAngle+31.0)>0.1) // Added
			{
				PushWithGripperGeneral(m_world, collidedBodyID, pushForce, minAngle, errorCode, allowCollisionsWithRegularObjects, true, sensorBodyIDtoPlan, collidedBody2ID,firstContactBodyID);
				//printErrorCode(errorCode);
				CompletePlan[numCollidedBodies].pushMotion = true;
				CompletePlan[numCollidedBodies].collidedBodyID = collidedBodyID;
				CompletePlan[numCollidedBodies].pushAngle = minAngle;
				CompletePlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDtoPlan;
				CompletePlan[numCollidedBodies].convertSensorBodyAfterwards = false;
				CompletePlan[numCollidedBodies].enableSensorEscapeMode=true;
				numCollidedBodies++;
			}

			if(errorCode==7)
			{
				if(!collisionCheckForABody(m_world,sensorBodyIDtoPlan,acceptedCodes,1,collidedBodyID))
				{
					convertSensorBodyToMovableBody(sensorBodyIDtoPlan);
					CompletePlan[numCollidedBodies-1].convertSensorBodyAfterwards = true;
				}
			}
		}
		else
		{
			solutionFound=true;
			convertSensorBodyToMovableBody(sensorBodyIDtoPlan);
			updateScreenPhysics(showGraphicsMaster);
			CompletePlan[numCollidedBodies].pushMotion = false;
			CompletePlan[numCollidedBodies].convertSensorBodyAfterwards = true;
			CompletePlan[numCollidedBodies].enableSensorEscapeMode=true;
			CompletePlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDtoPlan;
			numCollidedBodies++;
		}


	}

	wait(0.5);
	loadWorldState(initialWorldState);
	updateScreenPhysics(showGraphicsMaster);
	cout<<"I will now execute the complete plan in sequence ...."<<endl;
	wait(0.5);

	showGraphicsPlannerInternal=true;
	cout<<"# of moves: "<<numCollidedBodies<<endl;
	for(int k = 0; k < numCollidedBodies; k++)
	{
		wait(1.5);
		int firstContactBodyID;

		if(CompletePlan[k].pushMotion == true)
		{
			if(CompletePlan[k].enableSensorEscapeMode==true)
			{
				PushWithGripperGeneral(m_world, CompletePlan[k].collidedBodyID, 1.0, CompletePlan[k].pushAngle, errorCode, true, true, CompletePlan[k].sensorBodyObjectID, collidedBody2ID,firstContactBodyID);
				if(CompletePlan[k].convertSensorBodyAfterwards==true)
				{
					wait(1.5);
					convertSensorBodyToMovableBody(CompletePlan[k].sensorBodyObjectID);

				}
				updateScreenPhysics(showGraphicsMaster);
			}
			else
			{
				PushWithGripperGeneral(m_world, CompletePlan[k].collidedBodyID, 1.0, CompletePlan[k].pushAngle, errorCode, true, CompletePlan[k].enableSensorEscapeMode, CompletePlan[k].sensorBodyObjectID, collidedBody2ID,firstContactBodyID);
				updateScreenPhysics(showGraphicsMaster);
			}

		}
		else if(CompletePlan[k].convertSensorBodyAfterwards==true)
		{
			convertSensorBodyToMovableBody(CompletePlan[k].sensorBodyObjectID);
			updateScreenPhysics(showGraphicsMaster);
		}

	}
	return true;

}



bool Test::TestNode()
{
  //queue<TreeNode*> fifoQueue;
  cout<<"started testnode"<<endl;

  
  vector<float> tempVector;
  for(int i=0;i<50;i++)
    {
      tempVector.push_back(1.12);
    }
  nodeStruct testNodeStruct;
  testNodeStruct.relatedSensorFieldID=16;
  testNodeStruct.isStart=true;
  testNodeStruct.nodeID=0;
  testNodeStruct.currentPushedID=16;
  testNodeStruct.pastPushedID=16;
  testNodeStruct.pastPushedAngleVector=tempVector;
  testNodeStruct.pastPushedDistanceVector=tempVector;


  //for(int i=0;i<100;i++)
    //{
  TreeNode* node = new TreeNode(testNodeStruct);
  testNodeStruct.nodeID=1;
  TreeNode* childNode = node->AddNode(testNodeStruct);
  testNodeStruct.nodeID=2;
  TreeNode* grandChildNode = childNode->AddNode(testNodeStruct);
  testNodeStruct.nodeID=3;
  TreeNode*  g_grandChildNode= grandChildNode->AddNode(testNodeStruct);
  
  //cout<<"Adding to vector"<<endl;
  allTreeNodes.push_back(node);
  allTreeNodes.push_back(childNode);
  allTreeNodes.push_back(grandChildNode);
  allTreeNodes.push_back(g_grandChildNode);
    //}


  destroyAllNodes();
  
  
  //  cout<<"After destroy"<<endl;
    cout<<"Size: "<<allTreeNodes.size()<<endl;
    cout<<"node id: "<<node->nodeInfo.nodeID<<endl;
  
  return true;
}


bool Test::planPushesWithConvolutionAllowImmovablePushes(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce, int sensorBodyIDtoPlan)
{

	Plan PartialPlan[max_worldNodes];

	showGraphicsPlannerInternal=true;
	float replayWaits=1.0; //1.2
	float initialWaits=0.35; //0.5

	worldState initialWorldState;
	saveWorldState(initialWorldState);
	int acceptedCodes[2]={1,2};

	int numCollidedBodies = 0;
	int errorCode;
	int collidedBody2ID;

	bool solutionFound=false;
	int counter=0;
	while(!solutionFound)
	{
		if(counter>6)
		{
			cout<<"NO SOLUTION!"<<endl;
			return false;
		}
		else
		{
			counter++;
		}
		//cout<<counter<<" ";


		int collidedBodyID;
		int firstContactBodyID;
		if(collisionCheckForABody(m_world,sensorBodyIDtoPlan,acceptedCodes,2,collidedBodyID))
		{
			//cout<<"Collided Object, ID: "<<collidedBodyID<<endl;
			//cout<<"Sensor ID: "<<sensorBodyIDtoPlan<<endl;
			int aFirstContactObjectID;
			float minAngle=GetMinPushAngle(sensorBodyIDtoPlan,collidedBodyID,angleResolutionForPushSearch,allowCollisionsWithRegularObjects,pushForce,aFirstContactObjectID);

			if(abs(minAngle+31.0)<0.1)
			{
				b2Body* aFirstContactBody;
				if(receiveBodyWithObjectID(aFirstContactObjectID,aFirstContactBody)==true) // There is an object with ID : aFirstContactObjectID
				{	
					//cout<<"Received a first contact body, ID:"<<aFirstContactObjectID<<endl;
					float maxAngle=GetMaxPushAngle(sensorBodyIDtoPlan,aFirstContactObjectID,angleResolutionForPushSearch,allowCollisionsWithRegularObjects,false,pushForce);
					//cout<<"maxAngle: "<<maxAngle<<endl;
					if(abs(maxAngle+31.0)<0.1)
					{
						cout<<"NO SOLUTION!"<<endl;
						return false;
					}
					else
					{						
						PushWithGripperGeneral(m_world, aFirstContactObjectID, pushForce, maxAngle, errorCode, allowCollisionsWithRegularObjects, false, sensorBodyIDtoPlan, collidedBody2ID,firstContactBodyID);
						//printErrorCode(errorCode);
						PartialPlan[numCollidedBodies].pushMotion = true;
						PartialPlan[numCollidedBodies].collidedBodyID = aFirstContactObjectID;
						PartialPlan[numCollidedBodies].pushAngle = maxAngle;
						PartialPlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDtoPlan;
						PartialPlan[numCollidedBodies].convertSensorBodyAfterwards = false;
						PartialPlan[numCollidedBodies].enableSensorEscapeMode=false;
						numCollidedBodies++;
					}
				}

			}
			else
			{
				PushWithGripperGeneral(m_world, collidedBodyID, pushForce, minAngle, errorCode, allowCollisionsWithRegularObjects, true, sensorBodyIDtoPlan, collidedBody2ID,firstContactBodyID);
				//printErrorCode(errorCode);
				PartialPlan[numCollidedBodies].pushMotion = true;
				PartialPlan[numCollidedBodies].collidedBodyID = collidedBodyID;
				PartialPlan[numCollidedBodies].pushAngle = minAngle;
				PartialPlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDtoPlan;
				PartialPlan[numCollidedBodies].convertSensorBodyAfterwards = false;
				PartialPlan[numCollidedBodies].enableSensorEscapeMode=true;
				numCollidedBodies++;
			}

			if(errorCode==7)
			{
				if(!collisionCheckForABody(m_world,sensorBodyIDtoPlan,acceptedCodes,2,collidedBodyID))
				{
					convertSensorBodyToMovableBody(sensorBodyIDtoPlan);
					PartialPlan[numCollidedBodies-1].convertSensorBodyAfterwards = true;
					solutionFound=true;
					break;
				}
			}
		}
		else
		{

			convertSensorBodyToMovableBody(sensorBodyIDtoPlan);
			updateScreenPhysics(showGraphicsMaster);
			PartialPlan[numCollidedBodies].pushMotion = false;
			PartialPlan[numCollidedBodies].convertSensorBodyAfterwards = true;
			PartialPlan[numCollidedBodies].enableSensorEscapeMode=true;
			PartialPlan[numCollidedBodies].sensorBodyObjectID = sensorBodyIDtoPlan;
			numCollidedBodies++;
			solutionFound=true;
			break;
		}


	}

	wait(initialWaits);
	loadWorldState(initialWorldState);
	updateScreenPhysics(showGraphicsMaster);
	cout<<"Executing plan.."<<" # of moves: "<<numCollidedBodies<<endl;
	wait(initialWaits);
	showGraphicsPlannerInternal=true;
	float replayPushForce=pushForce/2.0;

	for(int k = 0; k < numCollidedBodies; k++)
	{
		wait(replayWaits);
		int firstContactBodyID;

		if(PartialPlan[k].pushMotion == true)
		{
			if(PartialPlan[k].enableSensorEscapeMode==true)
			{
				PushWithGripperGeneral(m_world, PartialPlan[k].collidedBodyID, replayPushForce, PartialPlan[k].pushAngle, errorCode, true, true, PartialPlan[k].sensorBodyObjectID, collidedBody2ID,firstContactBodyID);
				if(PartialPlan[k].convertSensorBodyAfterwards==true)
				{
					wait(replayWaits);
					convertSensorBodyToMovableBody(PartialPlan[k].sensorBodyObjectID);
				}
				updateScreenPhysics(showGraphicsMaster);
			}
			else
			{
				PushWithGripperGeneral(m_world, PartialPlan[k].collidedBodyID, replayPushForce, PartialPlan[k].pushAngle, errorCode, true, PartialPlan[k].enableSensorEscapeMode, PartialPlan[k].sensorBodyObjectID, collidedBody2ID,firstContactBodyID);
				updateScreenPhysics(showGraphicsMaster);
			}

		}
		else if(PartialPlan[k].convertSensorBodyAfterwards==true)
		{
			convertSensorBodyToMovableBody(PartialPlan[k].sensorBodyObjectID);
			updateScreenPhysics(showGraphicsMaster);
		}

	}
	return true;

}



void Test::wait ( float seconds )
{
	clock_t endwait;
	endwait = static_cast<int>(clock () + seconds * CLOCKS_PER_SEC) ;
	while (clock() < endwait) {}
}

bool Test::plan()
{
	float angleResolutionForPushSearch=M_PI/16;
	bool allowCollisionsWithRegularObjects=true;
	float pushForce=1.5;
	worldState initialWorldState;
	vector<int> sensorBodyIDVector;


	saveWorldState(initialWorldState);
	for(int ii=0;ii<10;ii++)
	{

		GetRandomPlacementLocation(); // Place sensor bodies in random locations
		cout<<"Generated Random Locations"<<endl;
		updateScreenPhysics(showGraphicsMaster);
		bool currentPlanResult=planPushes(angleResolutionForPushSearch,allowCollisionsWithRegularObjects,pushForce);

		if(currentPlanResult)
		{
			cout<<"SOLUTION FOUND"<<endl;
			break;
		}
		else
		{
			loadWorldState(initialWorldState);
		}

	}
	return true;
}

bool Test::getReliableObjectPlacementPose(int inquiredSensorBodyID, bool new_env)
{
	int acceptedCodesForCollCheck[2]={9,2}; //borders and immovables
	int firstContactBodyID;
	getObjectPlacementPose(inquiredSensorBodyID, new_env);
	updateScreenPhysics(showGraphicsMaster);
	//for(int ii=0;ii<50;ii++)
	while(ps_.scores.size() > 0)
	{
		if(collisionCheckForABody(m_world,
				inquiredSensorBodyID,
				acceptedCodesForCollCheck,2,
				firstContactBodyID))
		{
		  std::cout << "This placement is in collision, resampling." << std::endl;
#ifdef DEBUG_POSE_SAMPLING
			std::cout << "This placement is in collision, resampling." << std::endl << std::endl;
#endif // DEBUG_POSE_SAMPLING
			getObjectPlacementPose(inquiredSensorBodyID, false);
			updateScreenPhysics(showGraphicsMaster);
		}
		else
		{
#ifdef DEBUG_POSE_SAMPLING
			std::cout << "Sampled a good pose." << std::endl << std::endl;
#endif // DEBUG_POSE_SAMPLING
			return true;
		}
	}
	return false;
}

bool Test::getReliableObjectPlacementPoseAllowImmovablePushes(int inquiredSensorBodyID, bool new_env)
{
	//int acceptedCodesForCollCheck[2]={9,2}; //borders and immovables
	int acceptedCodesForCollCheck[1]={9}; //borders
	int firstContactBodyID;
	getObjectPlacementPose(inquiredSensorBodyID, new_env);
	updateScreenPhysics(showGraphicsMaster);
	//for(int ii=0;ii<50;ii++)
	while(ps_.scores.size() > 0)
	{
		if(collisionCheckForABody(m_world,inquiredSensorBodyID,acceptedCodesForCollCheck,1,firstContactBodyID))
		{
#ifdef DEBUG_POSE_SAMPLING
			std::cout << "This placement is in collision, resampling."
					<< std::endl << std::endl;
#endif // DEBUG_POSE_SAMPLING

			getObjectPlacementPose(inquiredSensorBodyID, false);
			updateScreenPhysics(showGraphicsMaster);
		}
		else
		{
#ifdef DEBUG_POSE_SAMPLING
			std::cout << "Sampled a good pose." << std::endl << std::endl;
#endif // DEBUG_POSE_SAMPLING
			return true;
		}
	}
	return false;
}

bool Test::planWithConvolution()
{
	float angleResolutionForPushSearch=M_PI/10;
	bool allowCollisionsWithRegularObjects=true;
	float pushForce=1.0;
	worldState initialWorldState;
	int sensorBodyIDtoPlan;

	vector<int> sensorBodyIDVectorALL;
	retrieveCurrentSensorBodyIDVector(m_world,sensorBodyIDVectorALL);
	if(sensorBodyIDVectorALL.empty())
	{
		cout<<"NO OBJECTS LEFT.."<<endl;
		return true;
	}
	else
	{
		sensorBodyIDtoPlan=sensorBodyIDVectorALL[0];
	}

	saveWorldState(initialWorldState);

	for(int ii=0;ii<5;ii++)
	{
		if(getReliableObjectPlacementPose(sensorBodyIDtoPlan, ii == 0))
		{
			updateScreenPhysics(showGraphicsMaster);
			wait(0.75);
			bool currentPlanResult=planPushesWithConvolution(angleResolutionForPushSearch,allowCollisionsWithRegularObjects,pushForce,sensorBodyIDtoPlan);

			if(currentPlanResult)
			{
				cout<<"SOLUTION FOUND!"<<endl;
				break;
			}
			else
			{
				loadWorldState(initialWorldState);
			}
		}
	}
	return true;
}

bool Test::planWithConvolutionAllowImmovablePushes()
{
	float angleResolutionForPushSearch=M_PI/9;
	bool allowCollisionsWithRegularObjects=true;
	float pushForce=9.0;
	worldState initialWorldState;
	int sensorBodyIDtoPlan;

	vector<int> sensorBodyIDVectorALL;
	retrieveCurrentSensorBodyIDVector(m_world,sensorBodyIDVectorALL);
	if(sensorBodyIDVectorALL.empty())
	{
		cout<<"NO OBJECTS LEFT.."<<endl;
		return true;
	}
	else
	{
		int randomIndex = (int)(rand() % sensorBodyIDVectorALL.size());
		sensorBodyIDtoPlan=sensorBodyIDVectorALL[randomIndex];
		//sensorBodyIDtoPlan=sensorBodyIDVectorALL[0];
	}

	saveWorldState(initialWorldState);

	for(int ii=0;ii<5;ii++)
	{	
		cout<<endl<<"-Convolution Trial#"<<ii<<endl;
		if(getReliableObjectPlacementPoseAllowImmovablePushes(sensorBodyIDtoPlan, ii == 0))
		{
			updateScreenPhysics(showGraphicsMaster);
			wait(0.5);
			bool currentPlanResult=planPushesWithConvolutionAllowImmovablePushes(angleResolutionForPushSearch,allowCollisionsWithRegularObjects,pushForce,sensorBodyIDtoPlan);
			if(currentPlanResult)
			{
				cout<<"SOLUTION FOUND, total pushes: "<<pushesPerObject<<endl;
				break;
			}
			else
			{
				loadWorldState(initialWorldState);
			}
		}

	}
	pushesPerObject=0;
	return true;
}


bool Test::planIDS()
{
  bool enableLog=true;
  if(enableLog)
    {
      logFile.open("./result.txt",ios::app);
      //logFile.seekp(0, ios::end); 
    }

  worldState startWorld;
  saveWorldState(startWorld);

  int maxAllowedTreeLevel=4;
#ifdef SCENARIO1
  float angleResolutionForPushSearch=M_PI/12.0; 
  float pushForce=3.0; //7.0
  float replayWaits=1.0;
#else
  float angleResolutionForPushSearch=M_PI/12.0; 
  float pushForce=3.0; //7.0
  float replayWaits=0.3;
#endif
   bool allowCollisionsWithRegularObjects=true;

  worldState initialWorldState;
  int sensorBodyIDtoPlan;


  //showGraphicsPlannerInternal=false;
  // showGraphicsMaster=false;

      clock_t start, stop;
      assert((start = clock())!=-1);
      double t=0.0;

      vector<int> sensorBodyIDVectorALL;
      retrieveCurrentSensorBodyIDVector(m_world,sensorBodyIDVectorALL);
      if(sensorBodyIDVectorALL.empty())
	{	  
	  cout<<"NO OBJECTS LEFT.."<<endl;
	  loadWorldState(startWorld);
	  //GetRandomObjectPlacementOnTable(); // create random object configuration on table top.
	  // retrieveCurrentSensorBodyIDVector(m_world,sensorBodyIDVectorALL);	  
	  return false;
	}

      int randomIndex = (int)(rand() % sensorBodyIDVectorALL.size());
      sensorBodyIDtoPlan=sensorBodyIDVectorALL[randomIndex];

#ifdef SCENARIO1
      sensorBodyIDtoPlan=6;
#else
            randomizeSize(sensorBodyIDtoPlan);
#endif
      saveWorldState(initialWorldState);
      bool solutionFound=false;
      vector<Plan> masterPlan;
      nodezExplored=0;
      
#ifndef SCENARIO1
      for(int lvl=0;lvl<maxAllowedTreeLevel+1;lvl++)
#else
	for(int lvl=maxAllowedTreeLevel;lvl<maxAllowedTreeLevel+1;lvl++)
#endif
	{
	  loadWorldState(initialWorldState);
	  cout<<endl<<"**********Allowing "<<lvl<<" tree levels***********"<<endl;
#ifndef SCENARIO1	 
	  for(int ii=0;ii<19;ii++)
#else
	    for(int ii=0;ii<1;ii++)
#endif
	    {	
	      cout<<"-Conv#"<<ii<<" ";
#ifndef SCENARIO1	      
	      if(getReliableObjectPlacementPoseAllowImmovablePushes(sensorBodyIDtoPlan, ii == 0))
#endif		
		{
		cout<<"Clut: "<<(double)clutter_percent_<<endl;
		  destroyAllNodes();
		  updateScreenPhysics(showGraphicsMaster);
		  //wait(0.1);
		  
		  masterPlan.clear();
		  bool currentPlanResult=planPushesIDS(angleResolutionForPushSearch,allowCollisionsWithRegularObjects,pushForce,sensorBodyIDtoPlan,lvl,masterPlan);
		 
		  if(currentPlanResult)
		    {
		      solutionFound=true;
		      cout<<"SOLUTION FOUND!"<<endl;
		      //cout<<" Total pushes: "<<pushesPerObject<<endl;
		      if(!masterPlan.empty())
			{
			  //showGraphicsPlannerInternal=true;
			  replayPlan(masterPlan,pushForce,replayWaits,replayWaits);
			  //showGraphicsPlannerInternal=false;
			}
#ifndef SCENARIO1
		      convertSensorBodyToMovableBody(sensorBodyIDtoPlan);
#endif
		      break;
		    }
		  else
		    {
		      cout<<" <Conv trial failed>"<<endl;
		      //loadWorldState(initialWorldState);
		    }
		}
	      cout<<"End of trial"<<endl;
	    }
	
	  if(solutionFound)
	    {
	      break;
	    }
	}

#ifndef SCENARIO1
      if(enableLog)
	{
	  stop = clock();
	  t = (double) (stop-start)/CLOCKS_PER_SEC;	  
	  logFile.precision(5);
	  logFile<<(double)clutter_percent_<<"\t"<<(double)object_free_space_<<"\t"<<howManyMovablesOnTable()<<"\t"<<howManyImmovablesOnTable()<<"\t"<<pushesPerObject<<"\t"<<t<<"\t"<<nodezExplored<<"\t"<<masterPlan.size()<<"\t"<<solutionFound<<"\n";
	  cout<<"Clutter: %"<<clutter_percent_<<" Obj/Free: "<<object_free_space_<<" Success: "<<solutionFound<<" pushesPerObject: "<<pushesPerObject<< "Solution pushes: "<<masterPlan.size()<<" Run time: "<<t<<" Explored Nodes="<<nodezExplored<<endl;
	  logFile.close();
	}

#else
	  stop = clock();
	  t = (double) (stop-start)/CLOCKS_PER_SEC;	  
	  cout<<"Clutter: %"<<clutter_percent_<<" Obj/Free: "<<object_free_space_<<" Success: "<<solutionFound<<" pushesPerObject: "<<pushesPerObject<< "Solution pushes: "<<masterPlan.size()<<" Run time: "<<t<<" Explored Nodes="<<nodezExplored<<endl;
#endif
      pushesPerObject=0;

      if(solutionFound)
	{	  
	  return true;
	  // cout<<"SOLUTION FOUND!"<<endl;
	}
      else
	{
	  // cout<<"NO SOLUTION!"<<endl;
	  return false;
	}
}

int Test::howManyMovablesOnTable()
{
  int cnt=0;
  b2Body* bodyList=m_world->GetBodyList();
  for (b2Body* a = bodyList; a; a = a->GetNext())
    {
      if (a->GetUserData()!=NULL)
	{
	  MyBodyData *data = (MyBodyData*)a->GetUserData();
	  if(data->type == 1)
	    {
	      cnt++;
	    }
	}
    }
  return cnt;
}

int Test::howManyImmovablesOnTable()
{
  int cnt=0;
  b2Body* bodyList=m_world->GetBodyList();
  for (b2Body* a = bodyList; a; a = a->GetNext())
    {
      if (a->GetUserData()!=NULL)
	{
	  MyBodyData *data = (MyBodyData*)a->GetUserData();
	  if(data->type == 2)
	    {
	      cnt++;
	    }
	}
    }
  return cnt;
}


void Test::destroyAllNodes()
{
  unsigned int size = allTreeNodes.size();
  
  for(unsigned int i=0;i<size;i++)
    {
      //TreeNode* deleteNode=allTreeNodes.back();
	  delete allTreeNodes.back();
      allTreeNodes.pop_back();
      //delete deleteNode;
      //deleteNode=NULL;

      // allTreeNodes.pop_back();

      // delete allTreeNodes[i];
      // allTreeNodes[i]=NULL;
      
      //TreeNode* deleteNode = allTreeNodes.back();
      //allTreeNodes.pop_back();
      //delete deleteNode;
      // deleteNode = NULL;
    }
    allTreeNodes.clear();
}

void Test::randomizeSize(int id)
{
  b2Body* body;
  receiveBodyWithObjectID(id,body);	   
  b2Fixture* fixtureToLoad = body->GetFixtureList();

  b2PolygonShape* shapeToLoad = (b2PolygonShape*)(fixtureToLoad->GetShape());


  float const0=((float)RandomFloat(0.7,1.3));
  for(int ff=0;ff<(b2_maxPolygonVertices);ff++)
    {
      (shapeToLoad->m_normals[ff])*=const0;
      (shapeToLoad->m_vertices[ff])*=const0;
    }
  /*
    b2PolygonShape randomShape;
  randomShape.SetAsBox(1.0f, 1.0f);
      
  b2FixtureDef sensorfd;
  sensorfd.shape = &randomShape;
  */
}

void Test::getIntersectingIDsInSensorField(int sensorID, vector<int> &stemObjectIDList)
{
	stemObjectIDList.clear();
	b2Body* bodyList=m_world->GetBodyList();
	for (b2Body* a = bodyList; a; a = a->GetNext())
	{
		if (a->GetUserData()!=NULL)
		{
			MyBodyData *data = (MyBodyData*)a->GetUserData();
			if(data->type == 1 || data->type == 2)
			{
				if(IsCollision2(sensorID,data->objectID))
				{
					stemObjectIDList.push_back(data->objectID);
				}
			}
		}
	}
	/*
	cout<<"Intersecting IDs: "<<endl;
	for(unsigned int ii=0;ii<stemObjectIDList.size();ii++)
	  {
	    cout<<stemObjectIDList[ii]<<endl;
	  }
	  cout<<"---"<<endl;*/
}

bool Test::planPushesIDS(float angleResolutionForPushSearch,bool allowCollisionsWithRegularObjects,float pushForce, int sensorBodyIDtoPlan,int maxAllowedTreeLevel,vector<Plan> &masterPlan)
{
   bool localVerbose=false;
   vector<int> stemObjectIDList;
  
   worldState worldBeforePlanning;
   saveWorldState(worldBeforePlanning);

   int nodeCount=0;
   while(true) //do it till masterPlan is found
     {
       
       getIntersectingIDsInSensorField(sensorBodyIDtoPlan,stemObjectIDList);
       if(stemObjectIDList.empty())
	 {
	   loadWorldState(worldBeforePlanning);
	   return true;
	 }
       else
	 {
	   if(maxAllowedTreeLevel==0)
	     {
	       return false;
	     }
	 }
       
       bool subGoalSatisfied=false;
       queue<TreeNode*> nodeQueue;
       int currentIntersectingObjectID=stemObjectIDList[0]; //get the first stem from the list
       if(localVerbose)
	 {
	   cout<<"<Stem ID: "<<currentIntersectingObjectID<<" >"<<endl;
	 }

       int currentNumberOfIntersectingObjects=getNumberOfObjectsInSensorField(sensorBodyIDtoPlan);
       
       //add the first node to the tree
       nodeStruct initialNodeStruct;
       initialNodeStruct.nodeID=nodeCount;
       initialNodeStruct.relatedSensorFieldID=sensorBodyIDtoPlan;
       initialNodeStruct.currentPushedID=currentIntersectingObjectID;
       initialNodeStruct.isStart=true;
       nodeCount++;
       TreeNode* stemNode=new TreeNode(initialNodeStruct);
       nodeQueue.push(stemNode);
       allTreeNodes.push_back(stemNode);
       
       while (true)
	 {	
	   if(localVerbose)
	     {
	       cout<<"nodeQueue has: "<<nodeQueue.size()<<" nodes.."<<endl;
	     }

	   //There's no node to expand. Declare no solution!
	   if(nodeQueue.empty())
	     {
	       if(localVerbose)			
		 {
		   cout<<"<No Nodes to Explore  "<<currentIntersectingObjectID<<"> "; 
		 }
	       loadWorldState(worldBeforePlanning);
	       return false;
	     }
	   
	   TreeNode* currentNodePtr=nodeQueue.front(); //select the next node in the queue
	   nodeQueue.pop();
	   nodezExplored++;
	   
	   
	   //the explored node has a level larger than allowed.Declare no solution!
	   if((currentNodePtr->treeLevel)>maxAllowedTreeLevel)
	     {
	       if(localVerbose)
		 {
		   cout<<" <Node lvl not allowed "<<currentIntersectingObjectID<<"> ";
		 }
	       loadWorldState(worldBeforePlanning);
	       return false;
	     }
	   if(localVerbose)
	     {
	       cout <<" <Node ID#" <<currentNodePtr->nodeInfo.nodeID<<" ,LvL: "<<currentNodePtr->treeLevel<<"> ";
	     }
	   

	   //get the push b2Body*
	   b2Body* pushBody;
	   if(!receiveBodyWithObjectID(currentNodePtr->nodeInfo.currentPushedID,pushBody))
	     {
	       cout<<"Couldn't get the pushBody in PushIDS"<<endl;
	       cout<<"For ID: "<<currentNodePtr->nodeInfo.currentPushedID<<endl;
	       exit(0);
	     }
	   float bodyAngle = pushBody->GetAngle();

	   
	   //look to the tree structure, save # of moves
	   int treeIndexes[10];
	   TreeNode* nodeArray[10];
	   for(unsigned i=0;i<10;i++)
	     {
	       treeIndexes[i]=0;
	     }
	   int index = 0;
	   TreeNode* copyNode=currentNodePtr; 
	   while(true)
	     {
	       if(copyNode->nodeInfo.isStart)
		 {
		   break;
		 }
	       treeIndexes[index]=copyNode->nodeInfo.pastPushedAngleVector.size();
	       nodeArray[index]=copyNode;
	       index++;	
	       copyNode=copyNode->parentNode;	
	     }
	   /*
	     for(unsigned int i=0;i<10;i++)
	     {
	     cout<<"treeIndexes["<<i<<"]: "<<treeIndexes[i]<<endl;
	     if(treeIndexes[i]!=0)
	     {
	     vector<float> pushAngleV=nodeArray[i]->nodeInfo.pastPushedAngleVector;
	     for(unsigned int j=0;j<pushAngleV.size();j++)
	     {
	     cout<<"Theta: "<<pushAngleV[j]<<endl;
	     }
	     }
	     }		
	   */

	   worldState worldStateBeforePush;
	   saveWorldState(worldStateBeforePush);
	   vector<nodeStruct> candidNodeStructVector;
	   vector<int> firstContactIDsVector;
	   
	   for(float angle = -M_PI; angle < M_PI; angle += angleResolutionForPushSearch)
	     {
	       float actualPushAngle=bodyAngle+angle;
	       if(actualPushAngle>M_PI)
		 {
		   actualPushAngle=actualPushAngle-2*M_PI;
		 }
	       else if(actualPushAngle<-M_PI)
		 {
		   actualPushAngle=actualPushAngle+2*M_PI;
		 }
	       int errorCode;
	       int collidedBodyID;
	       int firstContactBodyID;
	       float pushDistance = PushWithGripperGeneral(m_world, currentNodePtr->nodeInfo.currentPushedID, pushForce, actualPushAngle, errorCode, allowCollisionsWithRegularObjects, currentNodePtr->nodeInfo.isStart, sensorBodyIDtoPlan, collidedBodyID,firstContactBodyID);
	       //cout<<"pushDistance: "<<pushDistance<<endl;
	       
	       vector<Plan> partialPlan;
	       Plan tempPlan;
	       tempPlan.collidedBodyID=currentNodePtr->nodeInfo.currentPushedID;
	       tempPlan.pushAngle=actualPushAngle;
	       tempPlan.sensorBodyObjectID=sensorBodyIDtoPlan;
	       tempPlan.enableSensorEscapeMode=currentNodePtr->nodeInfo.isStart;
	       partialPlan.push_back(tempPlan);
	       
	       if(errorCode!=0 && pushDistance>0.01) //if there's place for gripper
		 {	
		   vector<Plan> additionPlan;
		   subGoalSatisfied=checkIfGoal(pushForce,treeIndexes,nodeArray,currentIntersectingObjectID,sensorBodyIDtoPlan,currentNumberOfIntersectingObjects,additionPlan,false);
		   if(subGoalSatisfied) //sub-goal is satisfied!!
		     {
		       if(localVerbose)
			 {
			   cout<< " Sub-Goal satisfied"<<endl;
			 }
		       mergePlans(partialPlan,additionPlan);												
		       //replayPlan(partialPlan,pushForce,0.5,0.5);
		       mergePlans(masterPlan,partialPlan);
		       break;
		       //loadWorldState(worldStateBeforePush);
		     }
		   else //goal is not satisfied!
		     {
		       loadWorldState(worldStateBeforePush);	  
		       if(confirmIfMovableOrImmovableFromObjectID(firstContactBodyID))
			 {				
			   //cout<<"firstContactBodyID: "<<firstContactBodyID<<endl;
			   bool contactEncounteredBefore=false;
			   for(unsigned int ii=0;ii<firstContactIDsVector.size();ii++)
			     {
			       if(firstContactBodyID==firstContactIDsVector[ii]) // this contact had already been saved
				 {
				   candidNodeStructVector[ii].pastPushedAngleVector.push_back(actualPushAngle);
				   candidNodeStructVector[ii].pastPushedDistanceVector.push_back(pushDistance);
				   contactEncounteredBefore=true;
				   break;							
				 }
			     }
			   if(!contactEncounteredBefore) // contact is seen 1st time.
			     {
			       firstContactIDsVector.push_back(firstContactBodyID);
			       nodeStruct newNodeStruct;
			       vector<float> tempVector1;
			       tempVector1.push_back(actualPushAngle);
			       vector<float> tempVector2;
			       tempVector2.push_back(pushDistance);
			       newNodeStruct.relatedSensorFieldID=sensorBodyIDtoPlan;
			       newNodeStruct.pastPushedID=currentNodePtr->nodeInfo.currentPushedID;
			       newNodeStruct.pastPushedAngleVector=tempVector1;
			       newNodeStruct.pastPushedDistanceVector=tempVector2;
			       newNodeStruct.currentPushedID=firstContactBodyID;
			       newNodeStruct.isStart=false;
			       candidNodeStructVector.push_back(newNodeStruct);						
			     }
			 } //the contact object is either a movable or immovable
		     } //goal is not satisfied ends
		 } //if there's space for gripper
	     } //for all angle resolutions ends
	   
	   
	   if(!subGoalSatisfied)
	     {
	       //convert the candid nodes into actual nodes
	       for(unsigned int ii=0;ii<candidNodeStructVector.size();ii++)
		 {
		   //check if the object is manipulated in the parent nodes
		   TreeNode* upperNode=currentNodePtr;
		   bool objectVirgin=true;
		   while(true)
		     {
		       if(upperNode->nodeInfo.isStart)
			 {
			   break;
			 }
		       upperNode=upperNode->parentNode;
		       if(upperNode->nodeInfo.currentPushedID==candidNodeStructVector[ii].currentPushedID) // the object is manipulated before
			 {
			   objectVirgin=false;
			   break;
			 }
		     }
		   if(objectVirgin)
		     {
		       candidNodeStructVector[ii].nodeID=nodeCount;
		       nodeCount++;
		       TreeNode* newNode= currentNodePtr->AddNode(candidNodeStructVector[ii]);
		       //cout<<"Just Created a node with ID: "<<newNode->nodeInfo.nodeID<<endl;
		       nodeQueue.push(newNode);
		       allTreeNodes.push_back(newNode);
		     }
		   else
		     {
		       //cout<<"Object had been pushed before"<<endl;
		     }
		 }
	     }
	   else //sub-goal satisfied
	     {
	       //fill stem list
	       break;
	     }
	   
	 } //while the node queue is not empty
       
     } //while Master plan not found
   
   loadWorldState(worldBeforePlanning);
   return false;
}

bool Test::checkIfGoal(float pushForce,int treeIndexes[], TreeNode* nodeArray[],int intersectingObjectID,int targetSensorObjectID,int currentNumberOfIntersectingObjects,vector<Plan> &planVector,bool revertWorldWhenGoalFound)
{
  vector<Plan> planSteps(10);
  float lastPushDist;
  int lvl;
  int collidedBodyID;
  int errorCode;
  int firstContactBodyID;
  bool success = false;
  worldState defaultWorld;
  worldState intermediateWorld;
  saveWorldState(defaultWorld);
  saveWorldState(intermediateWorld);
  



  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
    {
      return true;
    }	
		
 Plan tempPlan;
  for(int i0 = 0; i0 < treeIndexes[0]; i0++)
    {
      lvl=0; 
      loadWorldState(defaultWorld);
      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i0], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
      if(errorCode!=0 && lastPushDist>0.00001)
	{	  
	  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
	    {		      
	      saveWorldState(intermediateWorld);
	    }
	  // Plan tempPlan;
	  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
	  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i0];
	  tempPlan.sensorBodyObjectID=targetSensorObjectID;
	  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
	  planSteps[lvl]=tempPlan;
	  
	  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
	    {
	      success=true;
	    }			
	  if(success)
	    {
	      if(revertWorldWhenGoalFound)
		{
	      loadWorldState(defaultWorld);
		}
	      planSteps.resize(lvl+1);
	      planVector=planSteps;
	      return true;
	    }
	  else
	    {
	      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
		{
		  if(i0==treeIndexes[0]-1)
		    {
		      loadWorldState(defaultWorld);
		    }
		  else
		    {
		      loadWorldState(intermediateWorld);
		    }
		}
	    }	

	  for(int i1 = 0; i1 < treeIndexes[1]; i1++)
	    {
	      lvl=1;
	      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i1], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
	      if(errorCode!=0 && lastPushDist>0.00001)
		{	  
		  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
		    {		      
		      saveWorldState(intermediateWorld);
		    }
		  // Plan tempPlan;
		  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
		  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i1];
		  tempPlan.sensorBodyObjectID=targetSensorObjectID;
		  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
		  planSteps[lvl]=tempPlan;
	  
		  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
		    {
		      success=true;
		    }			
		  if(success)
		    {
		      if(revertWorldWhenGoalFound)
			{
			  loadWorldState(defaultWorld);
			}		 
		      planSteps.resize(lvl+1);
		      planVector=planSteps;
		      return true;
		    }
		  else
		    {
		      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
			{
			  if(i1==treeIndexes[1]-1)
			    {
			      loadWorldState(defaultWorld);
			    }
			  else
			    {
			      loadWorldState(intermediateWorld);
			    }
			}
		    }	


		  for(int i2 = 0; i2 < treeIndexes[2]; i2++)
		    {
		      lvl=2;
		      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i2], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
		      if(errorCode!=0 && lastPushDist>0.00001)
			{	  
			  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
			    {		      
			      saveWorldState(intermediateWorld);
			    }
			  // Plan tempPlan;
			  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
			  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i2];
			  tempPlan.sensorBodyObjectID=targetSensorObjectID;
			  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
			  planSteps[lvl]=tempPlan;
	  
			  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
			    {
			      success=true;
			    }			
			  if(success)
			    {
			      if(revertWorldWhenGoalFound)
				{
				  loadWorldState(defaultWorld);
				}			
			      planSteps.resize(lvl+1);
			      planVector=planSteps;
			      return true;
			    }
			  else
			    {
			      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
				{
				  if(i2==treeIndexes[2]-1)
				    {
				      loadWorldState(defaultWorld);
				    }
				  else
				    {
				      loadWorldState(intermediateWorld);
				    }
				}
			    }	

			  for(int i3 = 0; i3 < treeIndexes[3]; i3++)
			    {
			      lvl=3;
			      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i3], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
			      if(errorCode!=0 && lastPushDist>0.00001)
				{	  
				  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
				    {		      
				      saveWorldState(intermediateWorld);
				    }
				  // Plan tempPlan;
				  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
				  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i3];
				  tempPlan.sensorBodyObjectID=targetSensorObjectID;
				  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
				  planSteps[lvl]=tempPlan;
	  
				  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
				    {
				      success=true;
				    }			
				  if(success)
				    {
				      if(revertWorldWhenGoalFound)
					{
				      loadWorldState(defaultWorld);
					}
				      planSteps.resize(lvl+1);
				      planVector=planSteps;
				      return true;
				    }
				  else
				    {
				      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
					{
					  if(i3==treeIndexes[3]-1)
					    {
					      loadWorldState(defaultWorld);
					    }
					  else
					    {
					      loadWorldState(intermediateWorld);
					    }
					}
				    }	

				  for(int i4 = 0; i4 < treeIndexes[4]; i4++)
				    {		
				      lvl=4;
				      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i4], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
				      if(errorCode!=0 && lastPushDist>0.00001)
					{	  
					  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
					    {		      
					      saveWorldState(intermediateWorld);
					    }
					  // Plan tempPlan;
					  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
					  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i4];
					  tempPlan.sensorBodyObjectID=targetSensorObjectID;
					  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
					  planSteps[lvl]=tempPlan;
	  
					  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
					    {
					      success=true;
					    }			
					  if(success)
					    {
					      if(revertWorldWhenGoalFound)
						{
						  loadWorldState(defaultWorld);
						}
					      planSteps.resize(lvl+1);
					      planVector=planSteps;
					      return true;
					    }
					  else
					    {
					      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
						{
						  if(i4==treeIndexes[4]-1)
						    {
						      loadWorldState(defaultWorld);
						    }
						  else
						    {
						      loadWorldState(intermediateWorld);
						    }
						}
					    }	

					  for(int i5 = 0; i5 < treeIndexes[5]; i5++)
					    {
					      lvl=5;
					      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i5], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
					      if(errorCode!=0 && lastPushDist>0.00001)
						{	  
						  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
						    {		      
						      saveWorldState(intermediateWorld);
						    }
						  // Plan tempPlan;
						  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
						  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i5];
						  tempPlan.sensorBodyObjectID=targetSensorObjectID;
						  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
						  planSteps[lvl]=tempPlan;
	  
						  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
						    {
						      success=true;
						    }			
						  if(success)
						    {
						      if(revertWorldWhenGoalFound)
							{
							  loadWorldState(defaultWorld);
							}
						      planSteps.resize(lvl+1);
						      planVector=planSteps;
						      return true;
						    }
						  else
						    {
						      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
							{
							  if(i5==treeIndexes[5]-1)
							    {
							      loadWorldState(defaultWorld);
							    }
							  else
							    {
							      loadWorldState(intermediateWorld);
							    }
							}
						    }	

						  for(int i6 = 0; i6 < treeIndexes[6]; i6++)
						    {
						      lvl=6;
						      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i6], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
						      if(errorCode!=0 && lastPushDist>0.00001)
							{	  
							  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
							    {		      
							      saveWorldState(intermediateWorld);
							    }
							  // Plan tempPlan;
							  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
							  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i6];
							  tempPlan.sensorBodyObjectID=targetSensorObjectID;
							  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
							  planSteps[lvl]=tempPlan;
	  
							  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
							    {
							      success=true;
							    }			
							  if(success)
							    {
							      if(revertWorldWhenGoalFound)
								{
								  loadWorldState(defaultWorld);
								}
							      
							      planSteps.resize(lvl+1);
							      planVector=planSteps;
							      return true;
							    }
							  else
							    {
							      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
								{
								  if(i6==treeIndexes[6]-1)
								    {
								      loadWorldState(defaultWorld);
								    }
								  else
								    {
								      loadWorldState(intermediateWorld);
								    }
								}
							    }	

							  for(int i7 =0; i7 < treeIndexes[7]; i7++)
							    {
							      lvl=7;
							      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i7], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
							      if(errorCode!=0 && lastPushDist>0.00001)
								{	  
								  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
								    {		      
								      saveWorldState(intermediateWorld);
								    }
								  // Plan tempPlan;
								  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
								  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i7];
								  tempPlan.sensorBodyObjectID=targetSensorObjectID;
								  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
								  planSteps[lvl]=tempPlan;
	  
								  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
								    {
								      success=true;
								    }			
								  if(success)
								    {
								      
								      if(revertWorldWhenGoalFound)
									{
									  loadWorldState(defaultWorld);
									}
								      planSteps.resize(lvl+1);
								      planVector=planSteps;
								      return true;
								    }
								  else
								    {
								      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
									{
									  if(i7==treeIndexes[7]-1)
									    {
									      loadWorldState(defaultWorld);
									    }
									  else
									    {
									      loadWorldState(intermediateWorld);
									    }
									}
								    }	

								  for(int i8 = 0; i8 < treeIndexes[8]; i8++)
								    {
								      lvl=8;
								      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i8], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
								      if(errorCode!=0 && lastPushDist>0.00001)
									{	  
									  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
									    {		      
									      saveWorldState(intermediateWorld);
									    }
									  // Plan tempPlan;
									  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
									  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i8];
									  tempPlan.sensorBodyObjectID=targetSensorObjectID;
									  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
									  planSteps[lvl]=tempPlan;
	  
									  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
									    {
									      success=true;
									    }			
									  if(success)
									    {
									      if(revertWorldWhenGoalFound)
										{									 
										  loadWorldState(defaultWorld);
										}
									      
									      planSteps.resize(lvl+1);
									      planVector=planSteps;
									      return true;
									    }
									  else
									    {
									      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
										{
										  if(i8==treeIndexes[8]-1)
										    {
										      loadWorldState(defaultWorld);
										    }
										  else
										    {
										      loadWorldState(intermediateWorld);
										    }
										}
									    }	

									  for(int i9 = 0; i9 < treeIndexes[9]; i9++)
									    {
									      lvl=9;
									      lastPushDist=PushWithGripperGeneral(m_world, nodeArray[lvl]->nodeInfo.pastPushedID, pushForce, nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i9], errorCode, true, nodeArray[lvl]->parentNode->nodeInfo.isStart, targetSensorObjectID, collidedBodyID , firstContactBodyID);
									      if(errorCode!=0 && lastPushDist>0.00001)
										{	  
										  if(!nodeArray[lvl]->parentNode->nodeInfo.isStart)
										    {		      
										      saveWorldState(intermediateWorld);
										    }
										  // Plan tempPlan;
										  tempPlan.collidedBodyID=nodeArray[lvl]->nodeInfo.pastPushedID;
										  tempPlan.pushAngle=nodeArray[lvl]->nodeInfo.pastPushedAngleVector[i9];
										  tempPlan.sensorBodyObjectID=targetSensorObjectID;
										  tempPlan.enableSensorEscapeMode=nodeArray[lvl]->parentNode->nodeInfo.isStart;
										  planSteps[lvl]=tempPlan;
	  
										  if(getNumberOfObjectsInSensorField(targetSensorObjectID)<currentNumberOfIntersectingObjects)
										    {
										      success=true;
										    }			
										  if(success)
										    {
										      if(revertWorldWhenGoalFound)
											{
											  loadWorldState(defaultWorld);
											}
										      planSteps.resize(lvl+1);
										      planVector=planSteps;
										      return true;
										    }
										  else
										    {
										      if(nodeArray[lvl]->parentNode->nodeInfo.isStart)
											{
											  if(i9==treeIndexes[9]-1)
											    {
											      loadWorldState(defaultWorld);
											    }
											  else
											    {
											      loadWorldState(intermediateWorld);
											    }
											}
										    }	
										}
									    }
									}
								    }
								}
							    }
							}
						    }
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }	
	}	
    }
  loadWorldState(defaultWorld);
  return success;
}
 
void Test::replayPlan(vector<Plan> &PartialPlan, float replayPushForce, float initialWait, float replayWaits)
{
  int errorCode;
  int collidedBody2ID;
  int firstContactBodyID;
  unsigned int planSize=PartialPlan.size();
  
  wait(initialWait);
  for(unsigned int k=0;k<planSize;k++)
    {
      PushWithGripperGeneral(m_world, PartialPlan[k].collidedBodyID, replayPushForce, PartialPlan[k].pushAngle, errorCode, true, PartialPlan[k].enableSensorEscapeMode, PartialPlan[k].sensorBodyObjectID, collidedBody2ID,firstContactBodyID);
      updateScreenPhysics(showGraphicsMaster);
      if(k!=planSize-1)
	{
	  wait(replayWaits);
	}
    }
}

void Test::mergePlans(vector<Plan> &original,vector<Plan> &added)
{
	for(unsigned int i=0;i<added.size();i++)
	{
		original.push_back(added[i]);
	}
}
