
#ifndef TREENODE_H
#define TREENODE_H

#include <vector>
class TreeNode;

struct nodeStruct 
{
	int nodeID;
	int relatedSensorFieldID;
	int pastPushedID;
	std::vector<float> pastPushedAngleVector;
	std::vector<float> pastPushedDistanceVector;	
	//worldState currentWorldState;
	float currentPushAngle;
	int currentPushedID;
	bool isStart;
	bool isGoal;
};

class TreeNode
{
public:
	int treeLevel;
	TreeNode(nodeStruct startNodeStruct);
	//virtual ~TreeNode();
	~TreeNode();
	nodeStruct nodeInfo;
	TreeNode* parentNode;
	TreeNode* AddNode(nodeStruct startNodeStruct);
};

#endif
