#ifndef APPLY_FORCE_H
#define APPLY_FORCE_H



#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;



class ApplyForce : public Test
{



  //b2Body* bodies[25];
  b2PolygonShape b2PolygonShapes[25];
  b2FixtureDef b2FixtureDefs[25];
  b2BodyDef b2BodyDefs[25];
  MyBodyData* myDatas[25];

  b2Body* createObject(b2World *worldd,int ID,bool isSensorr,b2PolygonShape shapee,float xx,float yy,int typee)
{						
      b2FixtureDef tempFixtureDef;
      tempFixtureDef.shape=&shapee;
      tempFixtureDef.density=0.9f;
      tempFixtureDef.friction=1.0f;
      tempFixtureDef.isSensor=isSensorr;
      b2FixtureDefs[ID]=tempFixtureDef;
      
      b2BodyDef tempBodyDef;
      tempBodyDef.type=b2_dynamicBody;
      tempBodyDef.linearDamping=5.0f;
      tempBodyDef.angularDamping=5.0f;
      tempBodyDef.position.Set(xx,yy);
      b2BodyDefs[ID]=tempBodyDef;
            
      MyBodyData* tempBodyData=new MyBodyData;
      tempBodyData->type=typee;
      tempBodyData->objectID=ID;
      tempBodyDef.userData=tempBodyData;
      myDatas[ID]=tempBodyData;
      
      b2Body* tempBody=worldd->CreateBody(&tempBodyDef);
      tempBody->CreateFixture(&tempFixtureDef);
      body[ID]=tempBody;
      return tempBody;

}

 public:
  ApplyForce(ros::NodeHandle &n) : Test(n)
  {
    objectCount=0;
    m_world->SetGravity(b2Vec2(0.0f, 0.0f));
    const float32 k_restitution = 0.1f;

    b2Body* ground0;
    b2Body* ground1;
    b2Body* ground2;
    b2Body* ground3;
    {
      b2BodyDef bd0;
      bd0.linearDamping=6.0f;
      bd0.angularDamping=11.0f;
      bd0.position.Set(0.0f, 0.0f);
      MyBodyData* bodyData0 = new MyBodyData;
      bodyData0->type=9;
      bodyData0->objectID=objectCount;
      bd0.userData=bodyData0;
      objectCount++;

      b2BodyDef bd1;
      bd1.linearDamping=6.0f;
      bd1.angularDamping=11.0f;
      bd1.position.Set(0.0f, 0.0f);
      MyBodyData* bodyData1 = new MyBodyData;
      bodyData1->type=9;
      bodyData1->objectID=objectCount;
      bd1.userData=bodyData1;
      objectCount++;

      b2BodyDef bd2;
      bd2.linearDamping=6.0f;
      bd2.angularDamping=11.0f;
      bd2.position.Set(0.0f, 0.0f);
      MyBodyData* bodyData2 = new MyBodyData;
      bodyData2->type=9;
      bodyData2->objectID=objectCount;
      bd2.userData=bodyData2;
      objectCount++;
      b2BodyDef bd3;
      bd3.linearDamping=6.0f;
      bd3.angularDamping=11.0f;
      bd3.position.Set(0.0f, 0.0f);
      MyBodyData* bodyData3 = new MyBodyData;
      bodyData3->type=9;
      bodyData3->objectID=objectCount;
      bd3.userData=bodyData3;
      objectCount++;

      ground0 = m_world->CreateBody(&bd0);
      ground1 = m_world->CreateBody(&bd1);
      ground2 = m_world->CreateBody(&bd2);
      ground3 = m_world->CreateBody(&bd3);


      b2EdgeShape shape;
      b2FixtureDef sd;
      sd.shape = &shape;
      sd.density = 5.0f;
      sd.friction = 1.0f;
      sd.restitution = k_restitution;
      sd.isSensor=true;

      float32 epsilon=0.98f;

      // Left vertical
      shape.Set(b2Vec2(minx, miny), b2Vec2(minx, maxy));
      ground0->CreateFixture(&sd);

      // Right vertical
      shape.Set(b2Vec2(maxx, miny), b2Vec2(maxx, maxy));
      ground1->CreateFixture(&sd);

      // Top horizontal
      shape.Set(b2Vec2(minx*epsilon, maxy), b2Vec2(maxx*epsilon, maxy));
      ground2->CreateFixture(&sd);

      // Bottom horizontal
      shape.Set(b2Vec2(minx*epsilon, miny), b2Vec2(maxx*epsilon, miny));
      ground3->CreateFixture(&sd);
    }

    /*
      b2Body* ground;
      b2BodyDef bd;
      bd.linearDamping=5.0f;
      bd.angularDamping=10.0f;
      bd.position.Set(0.0f, 20.0f);
      ground = m_world->CreateBody(&bd);

      b2LoopShape chain;
      b2Vec2 vertices[4];
      b2Vec2 vertex0=b2Vec2(-20.0f, -20.0f);
      b2Vec2 vertex1=b2Vec2(20.0f, -20.0f);
      b2Vec2 vertex2=b2Vec2(20.0f, 20.0f);
      b2Vec2 vertex3=b2Vec2(-20.0f, 20.0f);
      vertices[0]=vertex0;
      vertices[1]=vertex1;
      vertices[2]=vertex2;
      vertices[3]=vertex3;
      chain.Create(vertices,4);

      b2FixtureDef sd;
      sd.shape = &chain;
      //sd.density = 5.0f;
      sd.friction = 1.0f;
      sd.restitution = k_restitution;
      ground->CreateFixture(&chain,1.0);
    */

    {

    	
    	
#ifdef SCENARIO1

      b2Vec2 circleVertices[b2_maxPolygonVertices];
      float angleIncrement=2*M_PI/(float)b2_maxPolygonVertices;
      float currentAngle=0.0;
      float r=3.5;
      for(int j=0;j< b2_maxPolygonVertices;j++)
	{
	  circleVertices[j].Set(r*cos(currentAngle),r*sin(currentAngle));
	  //cout<<"Vertex "<<j<<": x: "<<r*cos(currentAngle)<<" y: "<<r*sin(currentAngle)<<endl;
	  currentAngle=currentAngle+angleIncrement;
	}								
      
      b2PolygonShape circularPolygon;
      circularPolygon.Set(circleVertices,b2_maxPolygonVertices);

      //create sensor Fields


      b2PolygonShape sensorShapeBox;
      sensorShapeBox.SetAsBox(3.0f, 7.5f);

      b2PolygonShape smallBox;
      smallBox.SetAsBox(4.0f, 4.0f);
      
      b2PolygonShape sensorShapeHorzRect;
      sensorShapeHorzRect.SetAsBox(8.0f, 1.5f);
      
      b2PolygonShape sensorShapeBigRectangle;
      sensorShapeBigRectangle.SetAsBox(3.0f, 7.5f);



      body[objectCount]==createObject(m_world,objectCount,false,circularPolygon,-12.0,10.0,1);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,false,circularPolygon,12.0,18.0,1);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeBigRectangle,12.0,12.5,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,false,sensorShapeBox,0.0,15.0,1);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,false,sensorShapeBigRectangle,-4.0,22.0,1);
      objectCount++;

      body[objectCount]==createObject(m_world,objectCount,false,sensorShapeHorzRect,-12.0,15.0,1);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,false,smallBox,-17.0,5.0,1);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,false,smallBox,4.0,18.0,1);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,false,smallBox,-4.0,19.0,1);
      objectCount++;

#else
    	
   b2Vec2 circleVertices[b2_maxPolygonVertices];
      float angleIncrement=2*M_PI/(float)b2_maxPolygonVertices;
      float currentAngle=0.0;
      float r=3.5;
      for(int j=0;j< b2_maxPolygonVertices;j++)
	{
	  circleVertices[j].Set(r*cos(currentAngle),r*sin(currentAngle));
	  //cout<<"Vertex "<<j<<": x: "<<r*cos(currentAngle)<<" y: "<<r*sin(currentAngle)<<endl;
	  currentAngle=currentAngle+angleIncrement;
	}								
      
      b2PolygonShape circularPolygon;
      circularPolygon.Set(circleVertices,b2_maxPolygonVertices);


      //create sensor Fields
      b2PolygonShape sensorShapeSmallRectangle;
      sensorShapeSmallRectangle.SetAsBox(2.2f, 6.5f);
     
      b2PolygonShape sensorShapeBigRectangle;
      sensorShapeBigRectangle.SetAsBox(2.5f, 7.5f);
      
      b2PolygonShape sensorShapeSmallBox;
      sensorShapeSmallBox.SetAsBox(3.6f, 3.6f);
      
      b2PolygonShape sensorShapeBigBox;
      sensorShapeBigBox.SetAsBox(4.8f, 4.8f);
      
      b2PolygonShape sensorShapeLongRectangle;
      sensorShapeLongRectangle.SetAsBox(1.5f, 9.3f);


      float circleGroupHeight=-20.0;
      float rectangleGroupHeight=100.0;
      float squareGroupHeight=60.0;
      body[objectCount]==createObject(m_world,objectCount,false,circularPolygon,12.0,15.0,2);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,circularPolygon,-40.0,circleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,circularPolygon,-20.0,circleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,circularPolygon,-0.0,circleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,circularPolygon,20.0,circleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,circularPolygon,40.0,circleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,circularPolygon,60.0,circleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,circularPolygon,80.0,circleGroupHeight,6);
      objectCount++;



      body[objectCount]==createObject(m_world,objectCount,false,sensorShapeSmallBox,0.0,15.0,1);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallBox,-40.0,squareGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallBox,-20.0,squareGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallBox,0.0,squareGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallBox,20.0,squareGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallBox,40.0,squareGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallBox,60.0,squareGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallBox,80.0,squareGroupHeight,6);
      objectCount++;



      body[objectCount]==createObject(m_world,objectCount,false,sensorShapeSmallRectangle,-12.0,15.0,2);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallRectangle,-40.0,rectangleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallRectangle,-20.0,rectangleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallRectangle,0.0,rectangleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallRectangle,20.0,rectangleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallRectangle,40.0,rectangleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallRectangle,60.0,rectangleGroupHeight,6);
      objectCount++;
      body[objectCount]==createObject(m_world,objectCount,true,sensorShapeSmallRectangle,80.0,rectangleGroupHeight,6);
      objectCount++;

#endif								
								
								
    }
  }


  void Keyboard(unsigned char key)
  {

    //FILE* pFile;
    int errorcode=-1;
    float distanceTraversed=-1.0;
    bool allowCollisionsWithRegularObjects=true;
    bool enableSensorFieldEscapeMode=false;
    float pushForce=15.0;

    //b2Body* collidedBody;
    //b2Body* pushedBody;
    //receiveBodyWithObjectID(7,pushedBody);

    int pushedBodyID=7;
    int collidedBodyID;
    int sensorFieldBodyID=9;
    int firstContactBodyID;

    

    switch (key)
    {
      case 'v':
        {
          //float minDistance = GetMinPushAngle(sensorFieldBodyID,pushedBodyID,M_PI/10,allowCollisionsWithRegularObjects,pushForce,firstContactBodyID);
         // cout<<"Min push angle for sensor body: "<<minDistance<<endl;
        	
	  //planWithConvolutionAllowImmovablePushes();
	  //GetRandomObjectPlacementOnTable(); // create random object configuration on table top.
          planIDS();
        }
        break;

      case 'f':
        {
          //PushBodyUp(body[1]);
          //MoveBodyUpWrtBodyFrame(body[1]);

          distanceTraversed=PushWithGripperUpWrtBodyFrame(m_world,pushedBodyID,pushForce,errorcode,allowCollisionsWithRegularObjects,enableSensorFieldEscapeMode,sensorFieldBodyID,collidedBodyID,firstContactBodyID);
          cout<<"collidedBodyID: "<<collidedBodyID<<endl;
          cout<<"firstContactBodyID: "<<firstContactBodyID<<endl;
	  cout<<"Distance: "<<distanceTraversed<<endl;

          //printAllCollisions(m_world);
        }
        break;

      case 'b':
        {
          distanceTraversed=PushWithGripperDownWrtBodyFrame(m_world,pushedBodyID,pushForce,errorcode,allowCollisionsWithRegularObjects,enableSensorFieldEscapeMode,sensorFieldBodyID,collidedBodyID,firstContactBodyID);
        }
        break;

      case 'l':
        {
	  worldState firstWorld;
	  saveWorldState(firstWorld);

	  bool lastResult=false;
	  for(int i=0;i<3000;i++)
	    {
	      cout<<"ITERATION"<<i<<endl;
	      if(!lastResult)
		{
		  loadWorldState(firstWorld);
		  RandomizeAllObjectSizes();
		  GetRandomObjectPlacementOnTable(); // create random object configuration on table top.
		}		
	      lastResult=planIDS();
		
	      if(lastResult)
		{
		  cout<<"SOLUTION FOUND!"<<endl;
		}
	      else
		{
		  cout<<"NO SOLUTION!"<<endl;
		}
	    }
	  
	}
        break;
        
      case 'c':
      {
    	  GetRandomObjectPlacementOnTable(); // create random object configuration on table top.
      }
      break;
      case 'k':
        {
        	
        	//planWithConvolution();
	  RandomizeAllObjectSizes(); 
        	
          /*distanceTraversed = PushWithGripperRightWrtBodyFrame(
                  m_world, pushedBodyID, pushForce, errorcode,
                  allowCollisionsWithRegularObjects,
                  enableSensorFieldEscapeMode, sensorFieldBodyID,
                  collidedBodyID, firstContactBodyID);*/

        }
        break;

      case 'a':

        {
          //TestNode();
	  Test::saveWorldState(tempWorldState);
	  //Test::loadWorldState(tempWorldState);
          int bodyCnt=m_world->GetBodyCount();
          cout<<"bodyCnt: "<<bodyCnt<<endl;
        }
        break;

      case 'd':
        {
	  Test::loadWorldState(tempWorldState);
          int bodyCnt=m_world->GetBodyCount();
          cout<<"bodyCnt: "<<bodyCnt<<endl;
        }
        break;
        

    }

    //printErrorCode(errorcode);
    //cout<<"-Total Distance : "<<distanceTraversed<<endl;
  }

  static Test* Create(ros::NodeHandle n)
  {
    return new ApplyForce(n);
  }

  b2Body* m_body;
  b2Body* body[100];
  //int objectCount;
  //vector<b2Body*> bodyVectorToSaveTo;
  worldState lastWorldState;
  worldState tempWorldState;
};

#endif
