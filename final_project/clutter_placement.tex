%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out
                                                          % if you need a4paper
\IEEEoverridecommandlockouts                              % This command is only
                                                          % needed if you want to
                                                          % use the \thanks command
\overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document



% The following packages can be found on http:\\www.ctan.org
\usepackage{graphics} % for pdf, bitmapped graphics files
\usepackage{subfig}
\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
\usepackage{amsmath} % assumes amsmath package installed
\usepackage{amssymb}  % assumes amsmath package installed

\title{Push Planning for Object Placement in Cluttered Environments}

\author{Akansel Cosgun \hspace{20pt} Victor Emeli \hspace{20pt} Tucker Hermans%
        \thanks{Akansel Cosgun, Victor Emeli, and Tucker Hermans are  with the Center for Robotics and Intelligent Machines and The School of Interactive Computing, Georgia Institute of Technology, Atlanta,~GA.}\\
}

\begin{document}

\maketitle
\thispagestyle{empty}
\pagestyle{empty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
We present a novel planning algorithm for the problem of placing objects on a cluttered surface.  Our contributions are twofold. First, we present a planning algorithm for determining pushing actions on objects in order to place an object on a crowded tabletop, when no continuous space is large enough for the object a priori. Second we introduce a heuristic for determining candidate placement poses of a given object used to guide this search. We show preliminary results for our algorithm in simulation and discuss issues for deployment on a robot.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Natural environments often contain cluttered surfaces.  Robots designed to operate in unstructured domains alongside humans would benefit greatly from the ability to manipulate objects in such environments.  In this work we examine the problem of placing objects onto cluttered surfaces.  There are numerous facets of this problem, such as the perception of what is a surface and what is an object residing on that surface, as well as the control of robot arms both for object placement and non-prehensile manipulation of objects on the table. However, we focus here on the issue of how objects currently residing on the table should be rearranged, so that a new object may be set on the table.  We refer to this problem as ``footprint clearing'', where the footprint represents the two dimensional boundary of the object to be placed, when viewed from above. Additionally we use the term ``clutter'' to mean any object or group of objects present on a surface.

We propose a novel planner for determining the required pushing actions to clear a space large enough to place the object of interest. To guide this search, we have designed a heuristic for determining candidate placement poses in order to decouple the process of finding a goal configuration from the issue of determining the set of required object pushes to reach this goal.

We first examine the relevant literature to this problem, including the perception of objects and clutter on tabletops, non-prehensile manipulation of objects residing on tabletops, object placement, and related planning work on obstacle clearing. Section~\ref{sec:method} describes our proposed algorithm in detail, while also defining the problem more rigorously. Section~\ref{sec:experiments} discusses experimental results produced in simulation under different variations of the domain.  We conclude in Section~\ref{sec:discussion} with a discussion of our planner in the context of the larger cluttered environment problem domain and note some needed abilities for deployment of our algorithm on an actual robot.

\label{sec:simulation}
\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.4\textwidth]{images/simulator-setup1}
  \caption{The simulator interface with an example initial table configuration.}
  \label{fig:sim-setup1}
\end{figure}


\section{Related Work}
\label{sec:related-work}
While much work has focused on pushing of objects by robots in cluttered tabletop domains~\cite{katz-isrr2009, kenney-icra2009, mason-isrr2009, dogar-iros2010, omrcen-ichr2009, fitzpatrick-iros2002}, none of these works focus on pushing objects as a prerequisite to object placement.  Recently \cite{schuster-ichr2010} shows work on placing objects on cluttered surfaces, but this work takes for granted that a suitably large area of tabletop exists on which the object can rest. We discuss these areas of perceiving tabletop clutter, tabletop object pushing, and planning for object rearrangement in detail below.

\subsection{Tabletop Clutter Perception}
Explicit plane fitting in 3D points clouds such as those proposed in~\cite{rusu-ras2008, bauer-aapc2003, weingarten-icra2004, triebel-icra05} can be used to fit a plane to the data and label points near the plane as the surface and the remaining points as clutter. The authors in~\cite{jain-auro2010} produce a variant of this where they build histograms based on point cloud heights and find tables through the clustering of dominant heights via connected components.

\cite{schuster-ichr2010}~showed that these methods do not hold up to the clutter of natural environments, since many of the assumptions required by such methods do not match the naturally occurring statistics of clutter and surface height. As such \cite{schuster-ichr2010} proposed a set of features to use in training a discriminate classifier to label points as clutter or surface.  The features used encode local properties of the range data as well as color distributions in the image. AdaBoost is then used to learn a classifier which labels each point as either surface or clutter.
While this approach outperforms the baseline approach of plane fitting it fails to capture local structure of the environment across neighboring pixels. Similar to this work is \cite{rusu-iros2009} which compares the performance of Conditional Random Fields (CRFs) and Support Vector Machines using the Fast Point Feature Histogram in classifying objects on tabletops. While the use of this CRF allows for some neighborhood information to be propagated, the implementation proposed by the authors only operated on a single scanline at a time in order to produce a tractable algorithm.

An alternative approach to these methods takes the form of interactive segmentation of objects through manipulation~\cite{fitzpatrick-iros2002, katz-rss2007, katz-isrr2009, kenney-icra2009}. In these works a robot arm pushes objects sitting on a tabletop and tracks image features in order to perform segmentation of the object from the background.  While these methods have produce interesting results, we will not be examining extensions to the analysis of clutter here.

\subsection{Tabletop Object Pushing}
In addition to the work of~\cite{fitzpatrick-iros2002, katz-rss2007, katz-isrr2009, kenney-icra2009} on manipulation for segmentation, discussed above, other work performed on pushing of tabletop objects has been performed with a variety of goals.  Recent work by Omr\u{c}en et. al. attempts to learn a pushing rule for large, flat objects (such as books), which is then used to guide the objects to slightly hang off the edge of the table to facilitate grasping for lifting~\cite{omrcen-ichr2009}.

In~\cite{dogar-iros2010} the authors propose ``push-grasping'' in cluttered tabletop environments, where the set of possible grasp frames available to the grasp planner is constrained by the environment.  This approach pushes an object so that it rolls into the hand of the robot, creating successful grasping, when objects are close to one another, while avoiding collisions with the clutter. \cite{berenson-ichr2008}~also looks at grasping of objects in cluttered environments, but focuses on finding grasps that avoid collision with other obstacles.  While this work is interesting, it could possibly be helped by pushing of the neighboring objects to allow for simpler grasping strategies to be employed.

\subsection{Planning for Obstacle Clearing}
We have found no work attempting to solve the problem proposed in this paper; however, two planning problems present themselves as relevant to our problem: object rearrangement~\cite{ben-shahar-ram1998} and \emph{Navigation Among Movable Objects} (NAMO)~\cite{reif-fcs1985, stillman-ijrr2008}.

\cite{wilfong-scg1988}~showed the problem of pushing objects for rearrangement to be PSPACE-hard and gave an \(O(n^3)\) algorithm to solve it, where only one object is movable and  \(n\) is the number of corners describing the environment configuration.  Early work on this problem used exact cell decomposition to find a manipulation path for the robot~\cite{alami-isrr1989}. Some differences between this problem and ours is that we allow objects to move one another, as opposed to only one object being moved at a time.  Additionally, while a path to a goal position is desired, our problem is under-constrained, where no goal configuration is known a priori. A competing configuration space method is offered in~\cite{ben-shahar-ram1998}, where states are first valued by a potential field calculation in the configuration space of the objects.  Using this valuation they can solve problems of linear (axis-aligned) pushing using a depth first search method that is guided by the construction of a graph representing the maximal number of linear pushes through the use of a ``permutation net.'' An extension to \(\epsilon\)-linear pushing is given, that builds permutation net in terms of perturbed versions of the state.  While this approach was shown to have nice results, it does not scale well to our domain, where the highly constrained set of movements, requires us to be able to move multiple objects at once inducing many non-linear motions.

NAMO presents equally relevant planning algorithms.  The main differences between the NAMO domain and tabletop pushing for object clearing come first from the under-constrained nature of the placement problem and second from the fact that the placement goal is stationary unlike the robot pose that changes, over time.  These differences are significant enough, that planning algorithms from one domain can not be directly applied to the other; nonetheless, we can still look to this work for inspiration.  Early work by \cite{chen-icra1991} searched over possible robot paths, pushing objects away from the putative solutions. This approach doesn't map well to our domain, since we have no notion of robot paths. \cite{okada-irs2004} models object interference through graphs; however, the planner used can not deal with inter-object interaction.
Extensions to dynamic situations have been performed such as that in~\cite{kindel-icra2000}, which employs probabilistic roadmaps to the problem.

\cite{stillman-ijrr2008} applies means-ends planning in order to efficiently compute plans for the NAMO problem.  This approach is general to any robot movement and seems to address similar issues to that found in our domain. While our underlying algorithm is different from that proposed in~\cite{stillman-ijrr2008} we were inspired by the means-end flavor of work and apply it as key step in our approach.

\section{Method}
\label{sec:method}

Placing an object onto a crowded tabletop requires a joint solution to determining a goal pose \(X_f\) for an object of shape \(Y\) to be placed and a sequence of pushing actions \(P = (p_0, p_1, \ldots, p_m)\) performed on the set of objects \(O = \{o_0, o_1, \ldots, o_n\}\) required to clear a large enough area around \(X_f\), so that the object can be placed on the table free of collision with the objects already present.

Following this description, we decompose the task of placing an object in a crowded environment into two stages.  The first stage determines a ranked list of candidate placement poses of the object, while the second component plans a set of pushing operations to clear a large enough space for the object of interest to be set on the table.  Our algorithm assumes a segmented overhead representation of the surface of interest.  Additionally the procedure requires knowing the footprint of the object to be placed. These assumptions can be met using an overhead camera along with a database of objects to be manipulated.  Moreover \cite{schuster-ichr2010} estimates these values for non-trivial, natural scenes from viewing angles that are not overhead of the surface.

The second stage of our algorithm is then to determine \(P\), the required sequence of pushing actions in order to have the surface of shape \(Y\) centered at pose \(X_f\) clear of any other objects \(o_i \in O\) on the table. Objects are defined by their current pose and their two dimensional footprint, as well as a Boolean value of \texttt{object\_pushable}, which  defines whether the object can be pushed through other objects or if it can only be pushed directly by the robot.  This distinction to avoid having tall, narrow objects knocked over by small objects pushing on their base.
Pushing actions are defined by the object that is pushed as well as a pushing orientation \(p_i = (o_j, d)\).  The push is performed with a constant force along this single direction until either a desired area is cleared or some object comes in contact with a boundary of the table or an object that is not \texttt{object\_pushable}.

We note here, that an exhaustive approach would not be effective.  Given a set of \(n\) objects, we can restrict each object to having 4 axis-aligned possible pushes (a restriction not taken by our algorithm).  This produces a branching factor of \(4n\) which increases exponentially at each level of a search tree. Moreover, since the space of possible positions per object is on the order of \(10^8\) the search tree must expand quite deep to be able to cover all possible plans.

\subsection{Goal Configuration Calculation}
Since the only goal directly specified by our high level task is to place a given object somewhere on the tabletop, there exist a wide range of possible goal configurations satisfying this criteria.  While different goal configurations could be ranked according to some metric (i.e. largest contiguous space left, smallest overall state change from the initial configuration), we choose to heuristically propose goal configurations, in hopes of reducing the search space of the planner.

Our heuristic determines an object placement pose having minimum overlap with the current configuration of objects. We calculate this value at pixel-level resolution of the overhead tabletop view at a finite number \(M\) of orientations (i.e. 11). Given the overhead segmentation of object and surface, we produce a binary image where object pixels have value 1 and surface 0.  For each of the \(M\) orientations we produce a rectangular binary mask of the footprint of the object to be placed.  This kernel has value 1 for object pixels and 0 for the surrounding pixels. An example kernel can be seen in Figure~\ref{fig:conv-kernel1}. This kernel, when convolved with the binary image, produces a score at each pixel, resulting in a grayscale image such as that in figure~\ref{fig:conv-res1}.

\begin{figure}[ht!]
  \centering
  \subfloat[  \label{fig:conv-kernel1}]{\includegraphics[width=0.15\textwidth]{images/conv-kernel1}} \(\;\)
  \subfloat[  \label{fig:conv-res1}]{\includegraphics[width=0.3\textwidth]{images/conv-res1}}
  \caption{Example convolution kernel and resulting scores for the environment shown in Figure~\ref{fig:sim-setup1}.}
\end{figure}

As a post-processing stage for each orientation we examine only the object positions, where the entire object footprint is contained within the boundaries of the table. This step is necessary to remove boundary issues from the convolution, which may find highly scored poses, where the object hangs off the edge of the table.  We then determine the location of locally-maximal points within this interior, which are also above 85\% of the overall maximum value for that orientation.  This produces a set of modes for a given orientation.  Figure~\ref{fig:conv-res-maxes1} shows an example of these modes, for two different orientations of the same object.  The red points correspond to the candidate locations.

\begin{figure*}[ht!]
  \centering
  \subfloat[  \label{fig:conv-res-maxes1a}]{\includegraphics[width=0.25\textwidth]{images/conv-res-maxes1}} \(\;\)
  \subfloat[  \label{fig:conv-res-maxes1b}]{\includegraphics[width=0.25\textwidth]{images/conv-res-maxes2}}
  \caption{Convolution results for two different orientations of the same object. Red pixels indicate the location of locally-maximal modes selected as candidate goal poses to be used as input to our planner.}
  \label{fig:conv-res-maxes1}
\end{figure*}

We note here that this heuristic essentially produces a discrete representation of the configuration space of a given object.  As such for situations where object placement solutions exist without the need for pushing any objects, such as those shown in Figure~\ref{fig:conv-res-maxes1a}, the maximally scored pose corresponds to a solution. In order to exploit this feature we sort the list of returned modes by score, so that the planner quickly see if a solution exists, which does not require the clearing of any obstacles. However, the case where no such solution exists is much more interesting and our approach for solving it is discussed in the next section.

\subsection{Footprint Clearing Planner}
The primary goal of the planner is to clear space within the clutter of the surface area, in preparation for object placement.  We take a Means-Ends Analysis approach, which is a technique used in Artificial Intelligence for controlling search in problem solving.  Given a current state and a goal state, an action is chosen that will reduce the difference between the two states.  It also includes a means for detecting the progress made towards the goal state in order to detect failed attempts.

At the commencement of the algorithm, the planner makes a call to the convolution algorithm.  If the convolution output results in a location that does not require manipulating clutter, then the object is placed with no further actions.  In the case where the convolution positions the object's footprint over clutter, then it is up to the planner to make the appropriate amount of space necessary for collision free placement.  An object footprint placement over clutter and its state space are shown in Figure 4.

\begin{figure}[ht!]
  \centering
  \subfloat[  \label{fig:best-first-search-min}]{\includegraphics[width=0.25\textwidth]{images/Planner/best-first-search-min}} \(\;\)
  \subfloat[  \label{fig:best-first-search-min-state}]{\includegraphics[width=0.25\textwidth]{images/Planner/best-first-search-min-state}}
  \caption{Best-First search (min. distance) for object placement with one overlapping clutter object.}
\end{figure}

The algorithm initially utilizes a Best-First search approach.  It attempts to push clutter that overlaps the placement object's footprint out of the overlapping region.  It attempts push moves at predefined angles that sum to 360 degrees and records the push distance of each attempt.  A push attempt is terminated under the following conditions: 1) Push object collides with an object deemed not \texttt{object\_pushable} or the table border 2) Push object pushes another object into an object deemded not \texttt{object\_pushable} or the table border 3) Push object ceases to overlap with the placement object's footprint.  Distances traversed under conditions 1 and 2 are not recorded.  If there are multiple pushes that clear placement object's footprint, then the minimum push distance is chosen and added to the plan.  In this case, the algorithm increments to the next clutter object that is in overlap with the placement object and repeats  the Best-First search.  If no other clutter is in overlap, then the plan is complete for that particular placement object.  

\begin{figure}[ht!]
  \centering
  \subfloat[  \label{fig:best-first-search-min-collision}]{\includegraphics[width=0.25\textwidth]{images/Planner/best-first-search-min-collision}}\(\;\)
  \subfloat[  \label{fig:best-first-search-min-collision-state}]{\includegraphics[width=0.25\textwidth]{images/Planner/best-first-search-min-collision-state}}
  \caption{Best-First search (min. distance) with multiple clutter.}
\end{figure}

In the event that the Best-First search fails to clear the required space to satisfy the placement goal, then the planner executes another algorithm that utilizes an Informed Random Walk. Figure 5 illustrates an instance where the Best-First search fails. 

During clutter object manipulation in the aforementioned search, a list of all collided objects are stored.  If Best-First search is unsuccessful, then one of the collided objects are chosen at random.  This object is then pushed with the same push angular resolutions previously employed.  Another Best-First search is performed, although in this instance, the maximum distance is recorded as opposed to the minimum distance.  The heuristic behind this approach assumes that you want to move collided objects as far away from the push object as possible.  In addition, the heuristic discards all pushes of the collided object that also collides with the original push object.  This reasoning suggests that it is feasible for a maximum distance push of the collided object to be in the direction of the original push object.  The Figure 6 illustrates this portion of the algorithm.


\begin{figure}[ht!]
  \centering
\includegraphics[width=0.4\textwidth]{images/Planner/best-first-search-max}
  \caption{Best-First search for collided object (max. distance)}.\label{fig:best-first-search-max}}
\end{figure}

An example scenario of the simulator is shown in Figure 7.  If object 17 is placed in overlap with object 5, then the planner will attempt to move object 5 with constant velocity in directions that are dependent on your push angular resolution.  If it can clear object 5, then it is done and will query for the next placement object.  If it cannot clear object 5, then assuming that there were collisons with object 4, 7, and 8 during manipulation, the planner will randomly chose one of these candidates and attempt to manipulate it using a maximum distance Best-First search.  The pushes that collide with object 5 are discarded.

\begin{figure}[ht!]
  \centering
\includegraphics[width=0.4\textwidth]{images/Planner/tabletop1}
  \caption{Example scenario for object placement in clutter.~\label{fig:object-state-space}}
\end{figure}

The Figure 8 illustrates an example state space for an overlapping clutter object.  Once the maximum push distance is acquired, it is performed and the original push object is once again manipulated.  If it still cannot clear the placement object's footprint, then another collided object is selected and the process is repeated.  After a predefined number of iterations, the planner terminates and queries the convolution algorithm for a new placement location and the process iteratively repeats until a global plan for all placement objects is constructed or no solution can be found.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.4\textwidth]{images/Planner/tree}
  \caption{Example state space representation for clutter object pushes.\label{fig:object-state-space}}
\end{figure}

This algorithm is simple but has proven to be effective in most instances.  It is restricted to search to a maximum of \(n+1\) levels deep per object in the state space (where \(n\) is the number of objects in collision with the current push object).  The exploitations of the convolution algorithm also assist in identifying the simplest solutions by providing a ranked list of best placement locations for each object.  The inherent nature of the problem presented in this research has numerous solutions within the search capabilities of our algorithm.  The bias towards finding the simplest solution also bodes well for transfer to real robotic hardware.  More complex solutions will require more push actions in the real world which would accumulate the indeterminate effects of each action on the clutter objects.  This unpredictability will greatly reduce the efficiency of the robot performing the task. We believe this efficiency, along with the empirical results presented in the next section, make up for the planners lack of completeness and inability to guarantee an optimal (shortest) plan.


\section{simulation}

In order to simulate the interaction between objects for a pushing action, we used a 2D physics engine called Box2D~\cite{box2d-ref}. Box2D supports convex polygons and circles and has properties such as collision detection, continious physics with friction and restitution, revolute/prismatic joints. It is open-source written in C++ and uses OpenGL with Freeglut for visualization.

The push actions are directly simulated as a push with a gripper. A gripper is defined as a rectangle with defined dimensions in simulation. In our implementation, only pushes with linear gripper trajectories are allowed. To simulate a pushing action for a given object towards an angle, first a suitable location for the gripper is found. This is done by running the collision check from the center of mass of the object and gradually moving away in the opposite direction of the pushing angle until there is no collision with the pushed object. At this configuration, another collision check for all other objects in the scene is run, to check if it is feasible to place the gripper for the push action. If there's enough room for the gripper, then a constant linear velocity is applied to the gripper.

An example video of the planner running in simulation on a series of objects is available at: \url{http://www.cc.gatech.edu/~thermans/videos/cs-8803-rip-final-video.mpeg}.


%% \begin{figure}
%%   \centering
%%   \includegraphics[width=0.3\textwidth]{images/binary-table1}
%%   \caption{Binary image of overhead table view.  Black areas are objects, white is tabletop.}
%% \label{fig:bin-table1}
%% \end{figure}


\section{experiments}
\label{sec:experiments}
We performed several experiments in order to analyze the effect of the amount of clutter as well as the number of not \texttt{object\_pushable} objects on the performance of the planner. Only \texttt{object\_pushable} object can be manipulated directly, no indirect contact is allowed on objects that are not \texttt{object\_pushable}. We set up 3 scenarios, in all of them there are 7 objects on the table in the initial configuration including squares and triangles. In the first experiment, all 7 objects are \texttt{object\_pushable}, in the second 2 objects are not \texttt{object\_pushable} and 5 are \texttt{object\_pushable}, and in the third all non of the 7 objects are \texttt{object\_pushable}. The initial configurations are shown in the Figure~\ref{fig:experiments1}. The objects outside the table are the footprints of the objects we will be placing to the table one by one.

\begin{figure*}[ht!]
  \centering
    \subfloat[  \label{fig:box2d1}]{\includegraphics[width=0.25\textwidth]{images/box2d1}} \(\;\)
    \subfloat[  \label{fig:box2d0}]{\includegraphics[width=0.25\textwidth]{images/box2d0}} \(\;\)
    \subfloat[  \label{fig:box2d3}]{\includegraphics[width=0.25\textwidth]{images/box2d3}} \(\;\)
  \caption{The initial configurations for 3 experiment cases. a) 7 \texttt{object\_pushable}, b) 5 \texttt{object\_pushable}, c) 0 \texttt{object\_pushable}}
  \label{fig:experiments1}
\end{figure*}

80 runs are conducted for each configuration. For every run, the clutter percentage, the planner success, number of convolution calls, number of moves in the plan and the number of explored actions are noted. At every iteration, an object outside the table is chosen at random for placement and the planner is run. The maximum moves allowed for the gripper was set to 8. If no solution is found until 8 moves for this particular goal pose of the object, the planner is stopped and the convolution algorithm is called again to receive a new goal pose for the object. The convolution algorithm is called for a maximum of 7 times. If no solution is found for all 7 convolution trials, the case is declared unsuccessful. On the first trial for a given configuration the best ranked convolution result is used. On all subsequent trials convolution results are sample from the set of all results, with a bias for higher ranked results. The clutter percentage is calculated as the ratio of the sum of the surface area the objects to the area of the table. In the no solution cases, the number of moves, convolution trials and action states explored are taken as the maximum allowable values. The experimental results are provided in Figure~\ref{fig:experiments2}.


\begin{figure*}[ht!]
  \centering
    \subfloat[  \label{fig:best-first-search-min}]{\includegraphics[width=0.95\textwidth]{images/box2d5}} \(\;\)
  \caption{Experiment results for 3 testbeds for 80 runs each. Green line= 7 \texttt{object\_pushable} objects, Blue= 2 not \texttt{object\_pushable}, 5 \texttt{object\_pushable}, Red=7 not \texttt{object\_pushable}. x axis is clutter percentage in all figures. a)Success Rate b) average number of convolution trials c) number of moves in the plan d)number of action states explored }
  \label{fig:experiments2}
\end{figure*}

The planner has 100\% success rate for configurations below 30\% clutter, no matter if the objects are \texttt{object\_pushable} or not. This tells us that we can always find a solution if the table is not very cluttered. We take advantage of the convolution heuristic in those cases, often placing the object to empty spaces without needing to push objects. The success rate decreases as the number of not \texttt{object\_pushable} objects are increased, which was an expected result. After 65\% clutter, the success rate of the algorithm approaches to zero since those cases are hard to solve with our current algorithm. In fact often it is difficult for a human to determine a solution. The average number of convolution trials and average number of moves in the plan are higher in the presence of objects that are not \texttt{object\_pushable} and it converges to maximum allowed values of 7 and 8, respectively, as the clutter increases. For \%50 percent clutter, which is around the rate for a typical crowded table, the success rate is \%100 for all \texttt{object\_pushable} and \%33 for the no \texttt{object\_pushable} case. The average number of convolution calls are 2 and 5, respectively. The plan has an average of 2.7 and 6.1 push moves for the all \texttt{object\_pushable} and no \texttt{object\_pushable} cases. The number of action states explored are lower when the plans involve fewer pushing moves; however it increases exponentially as the hardness of the problem is increased.

The clutter percentage and number of not \texttt{object\_pushable} objects are not the only parameters to affect the performance. The size of the object to be put is also a parameter. We also analyzed the experiments with the ratio of the object put to the free space. However since footprint areas of the objects we are putting down are similar in our experiments, it didn't give us very meaningful results. The geometry of the objects are also important. If the object is thin and long, it will be harder to find a place for it. We did not quantitatively analyze the effect of object geometry on the performance.

\section{Discussion}
\label{sec:discussion}
We have presented a novel algorithm for clearing tabletops to allow for the placement of objects. We believe that this is a compelling task for robotics research that bridges a gap between pushing strategies such as those in~\cite{omrcen-ichr2009, dogar-iros2010}, tabletop clutter perception~\cite{fitzpatrick-iros2002, katz-rss2007, schuster-ichr2010}, and picking and placing~\cite{jain-auro2010, edsinger-ichr2006}. While our work has focused on the use of planning to enable placing of objects in environments, the same approach could be useful in enabling the grasping of objects.  As noted earlier both~\cite{dogar-iros2010} and~\cite{omrcen-ichr2009} use pushing to assist in grasping objects in constrained settings.  By setting the goal footprint to be an area surrounding the object to be lifted, and treating that object as an immovable obstacle, our planner could then be used to make room for grasping an object in a cluttered environment.

Our planner was able to efficiently solve non-trivial footprint clearing problems and it is open to improvements such as employing a different or modified search method for state exploration. Additionally, the current sampling technique for determining goal configurations could be improved. Additionally, since the simulation is designed to simulate gripper pushes instead of allowing the objects to be rearranged in any way. This imposes restrictions on the actions allowed and takes away the completeness property of the approach, however it reduces the state space and improves the chances for a real robot system to execute the simulated plan by keeping the uncertainties on the object dynamics at a low level.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \section{ACKNOWLEDGMENTS}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Bibliography
\bibliographystyle{IEEEtran}
\bibliography{clutter}

\end{document}
