Compiling our planner is easy using the following commands in the project1 directory:

cmake .
make

Then to run the planner on  the following commands:

./astar_sokoban map.txt [use_labels]

Here the value use_labels can be any string, but should only be present if the map file contains specific labels for the boxes and goals (a,b,c, etc.), otherwise the string should be left blank. To run it on the included map files use the following commands.

./astar_sokoban map1.txt

./astar_sokoban map2.txt use_labels

./astar_sokoban map3.txt use_labels

./astar_sokoban map4.txt

./astar_sokoban mapFail.txt

To use your own map file simply make a map of the following form:

XXXXXX
XA__XX
Xrab_X
XX___X
XXX__X
XXXX_X
XXXXBX
XXXXXX

Here 'X' denotes a blocked cell, '_' denotes a clear cell 'r' denotes the robot, 'a' and 'b' denote boxes and 'A' and 'B' denote their respective goals.  To make a map without labeled boxes use the character 'b' to represent a box and 'g' to represent a goal.  An example for problem 4 is:

xxxxxxx
xxx__xx
xxxb_xx
x_b__Rx
x_ggg_x
xxb__xx
xx__xxx
xxxxxxx

