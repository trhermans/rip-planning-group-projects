/**
 * @file   sokoban.cpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 * @date   Thu Sep 23 17:50:51 2010
 *
 * @brief  This is the file to house the main executable for the Sokoban
 * project. It builds the model and runs the planner.
 *
 */

#include "sokoban_state.h"
#include <iostream>
#include <sstream>

using namespace std;
int main(int argc, char** argv)
{
  string map;
  bool labeled_boxes = false;
  if (argc > 1)
  {
    stringstream map_name;
    map_name << argv[1];
    map = map_name.str();
    if (argc > 2)
      labeled_boxes = true;
  }
  else
  {
    map = "./map1.txt";
  }

  SokobanState init_state(map, labeled_boxes);
  init_state.print();

  SokobanState n0 = init_state.pushBox(BOX_A, PUSH_NORTH);
  cout << "Push A north" << endl;
  n0.print();

  n0 = init_state.pushBox(BOX_A, PUSH_SOUTH);
  cout << "Push A south" << endl;
  n0.print();

  n0 = init_state.pushBox(BOX_A, PUSH_EAST);
  cout << "Push A east" << endl;
  n0.print();

  n0 = init_state.pushBox(BOX_A, PUSH_WEST);
  cout << "Push A west" << endl;
  n0.print();

  n0 = init_state.pushBox(BOX_B, PUSH_NORTH);
  cout << "Push B north" << endl;
  n0.print();

  n0 = init_state.pushBox(BOX_B, PUSH_SOUTH);
  cout << "Push B south" << endl;
  n0.print();

  n0 = init_state.pushBox(BOX_B, PUSH_EAST);
  cout << "Push B east" << endl;
  n0.print();

  n0 = init_state.pushBox(BOX_B, PUSH_WEST);
  cout << "Push B west" << endl;
  n0.print();

  return 0;
}
