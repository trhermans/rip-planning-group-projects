(define (domain sokoban)
(:requirements :strips)
(:predicates (navigable ?l)
             (direction ?d)
             (block-at ?b ?l)
             (robot-at ?l)
             (block ?b)
             (adjacent ?l1 ?l2 ?d) 
             (empty ?l)
)


(:action move
:parameters (?from ?to ?dir)
:precondition (and 
(navigable ?from) 
(navigable ?to) 
(direction ?dir)                   
(robot-at ?from) 
(adjacent ?from ?to ?dir) 
(empty ?to)
)
:effect (and 
(empty ?from)
(robot-at ?to) 
(not (empty ?to))
(not (robot-at ?from))
)
)
             


(:action push
:parameters  (?robotLoc ?blockLoc ?freeLoc  ?dirr ?activeBlock)
:precondition 
(and 
(navigable ?robotLoc) 
(navigable ?blockLoc) 
(navigable ?freeLoc)          
(direction ?dirr) 
(block ?activeBlock) 
(robot-at ?robotLoc)                   
(block-at ?activeBlock ?blockLoc) 
(adjacent ?robotLoc ?blockLoc ?dirr)                   
(adjacent ?blockLoc ?freeLoc ?dirr) 
(empty ?freeLoc)
)
:effect (and 
(robot-at ?blockLoc) 
(block-at ?activeBlock ?freeLoc) 
(empty ?robotLoc)       
(not (robot-at ?robotLoc)) 
(not (block-at ?activeBlock ?blockLoc)) 
(not (empty ?freeLoc)))
)

)

