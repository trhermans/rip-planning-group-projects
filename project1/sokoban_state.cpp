#include "sokoban_state.h"
#include <sstream>
#include <fstream>
#include <queue>
#include <cmath>

using namespace std;

// #define DEBUG_T_MAP
// #define USE_ADMISSABLE_VERSION
//
// Constructors
//

SokobanState::SokobanState(string filename, bool labeled_boxes, bool goal_map) :
    labeled_boxes_(labeled_boxes)
{
  loadMap(filename, labeled_boxes, goal_map);
  buildTraversabilityMap();
}

SokobanState::SokobanState(Coord robot_loc,
                           vector<Coord> box_locs, vector<Coord> goal_locs,
                           bool labeled_boxes,
                           vector<BoxLabel> box_labels,
                           vector<BoxLabel> goal_labels,
                           IDMap box_indexes,
                           IDMap goal_indexes,
                           SokobanMap state_map) :
    robot_(robot_loc), boxes_(box_locs), goals_(goal_locs),
    labeled_boxes_(labeled_boxes), box_ids_(box_labels), goal_ids_(goal_labels),
    box_indexes_(box_indexes), goal_indexes_(goal_indexes)
{
  // Setup map obstacles
  for(unsigned int y = 0; y < state_map.size(); ++y)
  {
    SokobanMapRow map_row;
    for (unsigned int x = 0; x < state_map[y].size(); ++x)
    {
      if( state_map[y][x] == BLOCKED_CELL)
      {
        map_row.push_back(BLOCKED_CELL);
      }
      else
      {
        map_row.push_back(CLEAR_CELL);
      }
    }
    map_.push_back(map_row);
  }

  // Add boxes and goals to map
  for (unsigned int i = 0; i < boxes_.size(); ++i)
  {
    if (labeled_boxes_)
    {
      MapElement box_label = idToBoxMapElement(box_ids_[i]);
      MapElement goal_label = idToGoalMapElement(goal_ids_[i]);
      if (map_[goals_[i].y_][goals_[i].x_] == CLEAR_CELL)
      {
        map_[goals_[i].y_][goals_[i].x_] = goal_label;
      }
      // TODO: To be better we really should label these cells as GOAL_SET,
      // but whatever
      map_[boxes_[i].y_][boxes_[i].x_] = box_label;
    }
    else
    {
      // NOTE: Label differently if box is on goal, do not clobber boxes
      // with goals
      if (map_[goals_[i].y_][goals_[i].x_] == BOX_CELL)
      {
        map_[goals_[i].y_][goals_[i].x_] = GOAL_SET_CELL;
      }
      else
      {
        map_[goals_[i].y_][goals_[i].x_] = GOAL_CELL;
      }
      if (map_[boxes_[i].y_][boxes_[i].x_] == GOAL_CELL)
      {
        map_[boxes_[i].y_][boxes_[i].x_] = GOAL_SET_CELL;
      }
      else
      {
        map_[boxes_[i].y_][boxes_[i].x_] = BOX_CELL;
      }
    }
  }

  // Add robot location to map
  map_[robot_.y_][robot_.x_] = ROBOT_CELL;

  buildTraversabilityMap();
}

/**
 * Push a box in a desired direction.
 *
 * @param box_id The vector index of the box to push.
 * @param a The action to perform on the box
 *
 * @return true if the action can be performed, false otherwise
 */
bool SokobanState::canPushBox(int box_index, Action a)
{
  // Test action feasability
  if (a == PUSH_NORTH)
  {
    if( !canPushNorth(boxes_[box_index]) || !canPushSouth(boxes_[box_index]))
      return false;
    if (traversableTo(Coord(boxes_[box_index].x_,
                            boxes_[box_index].y_ + 1)))
    {
      return true;
    }
  }
  else if (a == PUSH_SOUTH)
  {
    if( !canPushSouth(boxes_[box_index]) || !canPushNorth(boxes_[box_index]))
      return false;
    if (traversableTo(Coord(boxes_[box_index].x_,
                            boxes_[box_index].y_ - 1)))
    {
      return true;
    }
  }
  else if (a == PUSH_WEST)
  {
    if( !canPushWest(boxes_[box_index]) || !canPushEast(boxes_[box_index]))
      return false;
    if (traversableTo(Coord(boxes_[box_index].x_ + 1,
                            boxes_[box_index].y_)))
    {
      return true;
    }
  }
  else if (a == PUSH_EAST)
  {
    if( !canPushEast(boxes_[box_index]) || !canPushWest(boxes_[box_index]))
      return false;
    if (traversableTo(Coord(boxes_[box_index].x_ - 1,
                            boxes_[box_index].y_)))
    {
      return true;
    }

  }
  return false;
}

/**
 * Method returns a new state that is the current state plus after the action is
 * performed, if the action can not be performed the current state is returned.
 * By first calling canPushBox, this state will always return a different state.
 *
 * @param box_index The index of the box to move
 * @param a The action to be performed on the box
 *
 * @return The new state after the action is performed.
 */
SokobanState SokobanState::pushBox(int box_index, Action a)
{
  Coord new_robot_loc(robot_);
  Coord new_box_loc(boxes_[box_index]);
  vector<Coord> new_boxes(boxes_);

  // NOTE: Assume that we have run canPush...
  // if (! canPushBox(box_index, a))
  //   return *this;

  if (a == PUSH_NORTH)
  {
    new_box_loc.y_ -= 1;
  }
  else if (a == PUSH_SOUTH)
  {
    new_box_loc.y_ += 1;
  }
  else if (a == PUSH_WEST)
  {
    new_box_loc.x_ -= 1;
  }
  else if (a == PUSH_EAST)
  {
    new_box_loc.x_ += 1;
  }

  new_robot_loc = boxes_[box_index];

  new_boxes[box_index] = new_box_loc;

  SokobanState next_state(new_robot_loc, new_boxes, goals_, labeled_boxes_,
                          box_ids_, goal_ids_, box_indexes_, goal_indexes_,
                          map_);
  return next_state;
}

bool SokobanState::canPushBox(BoxLabel label, Action a)
{
  int box_idx = box_indexes_[label];
  return canPushBox(box_idx, a);
}

SokobanState SokobanState::pushBox(BoxLabel label, Action a)
{
  int box_idx = box_indexes_[label];
  return pushBox(box_idx, a);
}


//
// Traversability Functions
//

void SokobanState::buildTraversabilityMap()
{
  // Initialize traversability map
  t_map_.clear();
  for (unsigned int y = 0; y < map_.size(); ++y)
  {
    TMapRow t_map_row;
    for (unsigned int x = 0; x < map_[y].size(); ++x)
    {
      MapElement current = map_[y][x];
      if ( current == CLEAR_CELL || current == GOAL_CELL ||
           current == GOAL_A_CELL || current == GOAL_B_CELL ||
           current == GOAL_C_CELL)
      {
        t_map_row.push_back(UNKNOWN_TRAVERSABLE);
      }
      else if ( current == BLOCKED_CELL || current == BOX_CELL ||
                current == BOX_A_CELL || current == BOX_B_CELL ||
                current == BOX_C_CELL || current == GOAL_SET_CELL ||
                current == GOAL_A_SET_CELL || current == GOAL_B_SET_CELL ||
                current == GOAL_C_SET_CELL )
      {
        t_map_row.push_back(NOT_TRAVERSABLE);
      }
      else if ( current == ROBOT_CELL )
      {
        t_map_row.push_back(TRAVERSABLE);
      }
    }
    t_map_.push_back(t_map_row);
  }

  // Build a queue of locations to test for traversability
  queue<Coord> active;
  active.push(robot_);

  // NOTE: Boundaries are always marked as blocked, so we don't have to check if
  // a neighbor is on the boundary when we test to enque neighbors
  while(! active.empty() )
  {
    // Pop the first element
    Coord current = active.front();
    active.pop();
    int cur_x = current.x_;
    int cur_y = current.y_;

    // Test the neighbors
    // North
    if (t_map_[cur_y-1][cur_x] == UNKNOWN_TRAVERSABLE)
    {
      t_map_[cur_y-1][cur_x] = TRAVERSABLE;
      active.push(Coord(cur_x, cur_y-1));
    }
    // South
    if (t_map_[cur_y+1][cur_x] == UNKNOWN_TRAVERSABLE)
    {
      t_map_[cur_y+1][cur_x] = TRAVERSABLE;
      active.push(Coord(cur_x, cur_y+1));
    }
    // West
    if (t_map_[cur_y][cur_x-1] == UNKNOWN_TRAVERSABLE)
    {
      t_map_[cur_y][cur_x-1] = TRAVERSABLE;
      active.push(Coord(cur_x-1, cur_y));
    }
    // East
    if (t_map_[cur_y][cur_x+1] == UNKNOWN_TRAVERSABLE)
    {
      t_map_[cur_y][cur_x+1] = TRAVERSABLE;
      active.push(Coord(cur_x+1, cur_y));
    }
  }

#ifdef DEBUG_T_MAP
  // Mark unknowns as not traversable
  // NOTE: Only need to do this if we want to display the t_map, otherwise we're
  // wasting cycles
  for (unsigned int y = 0; y < map_.size(); ++y)
  {
    for (unsigned int x = 0; x < map_[y].size(); ++x)
    {
      if (t_map_[y][x] == UNKNOWN_TRAVERSABLE)
        t_map_[y][x] = NOT_TRAVERSABLE;
    }
  }
  //print();
  printTMap();
#endif
}

bool SokobanState::traversableTo(Coord l) const
{
  return (t_map_[l.y_][l.x_] == TRAVERSABLE);
}

//
// User I/O methods
//

void SokobanState::loadMap(string filename, bool labeled_boxes, bool goal_map)
{
  ifstream file_in(filename.c_str(), ifstream::in);
  SokobanMap current_map;

  int y = 0;
  while (! file_in.eof())
  {
    char c_line[1024];
    file_in.getline(c_line, 1024);
    stringstream line;
    line << c_line;
    SokobanMapRow map_row;

    for (int x = 0; x < line.str().size(); ++x)
    {
      char c;
      line >> c;

      if(c == '_')
      {
        map_row.push_back(CLEAR_CELL);
      }
      else if(c == 'X' || c == 'x')
      {
        map_row.push_back(BLOCKED_CELL);
      }
      else if (c == 'g' || c == 'A' || c == 'B' || c == 'C')
      {
        if (labeled_boxes)
        {
          if (c == 'A')
          {
            if (goal_map)
            {
              map_row.push_back(GOAL_A_SET_CELL);
            }
            else
            {
              map_row.push_back(GOAL_A_CELL);
            }
            goal_indexes_[BOX_A] = goal_ids_.size();
            goal_ids_.push_back(BOX_A);
          }
          else if (c == 'B')
          {
            if (goal_map)
            {
              map_row.push_back(GOAL_B_SET_CELL);
            }
            else
            {
              map_row.push_back(GOAL_B_CELL);
            }
            goal_indexes_[BOX_B] = goal_ids_.size();
            goal_ids_.push_back(BOX_B);
          }

          else if (c == 'C')
          {
            if (goal_map)
            {
              map_row.push_back(GOAL_C_SET_CELL);
            }
            else
            {
              map_row.push_back(GOAL_C_CELL);
            }
            goal_indexes_[BOX_C] = goal_ids_.size();
            goal_ids_.push_back(BOX_C);
          }
        }
        else
        {
          if (goal_map)
          {
            map_row.push_back(GOAL_SET_CELL);
          }
          else
          {
            map_row.push_back(GOAL_CELL);
          }
        }
        goals_.push_back(Coord(x,y));
      }
      else if (c == 'b' || c == 'a' || c == 'c')
      {
        if (labeled_boxes)
        {
          if (c == 'a')
          {
            if (goal_map)
            {
              map_row.push_back(CLEAR_CELL);
            }
            else
            {
              map_row.push_back(BOX_A_CELL);
            }
            box_indexes_[BOX_A] = box_ids_.size();
            box_ids_.push_back(BOX_A);
          }
          else if (c == 'b')
          {
            if (goal_map)
            {
              map_row.push_back(CLEAR_CELL);
            }
            else
            {
              map_row.push_back(BOX_B_CELL);
            }
            box_indexes_[BOX_B] = box_ids_.size();
            box_ids_.push_back(BOX_B);
          }
          else if (c == 'c')
          {
            if (goal_map)
            {
              map_row.push_back(CLEAR_CELL);
            }
            else
            {
              map_row.push_back(BOX_C_CELL);
            }
            box_indexes_[BOX_C] = box_ids_.size();
            box_ids_.push_back(BOX_C);
          }
        }
        else
        {
          if (goal_map)
          {
            map_row.push_back(CLEAR_CELL);
          }
          else
          {
            map_row.push_back(BOX_CELL);
          }
        }
        boxes_.push_back(Coord(x,y));
      }
      else if (c == 'r' || c == 'R')
      {
        map_row.push_back(ROBOT_CELL);
        robot_.x_ = x;
        robot_.y_ = y;
      }
    }
    y++;
    if (map_row.size())
      current_map.push_back(map_row);
  }
  if (!labeled_boxes_)
  {
    // TODO: Arbitrarily assign the labels to indexes
  }
  file_in.close();
  map_ = current_map;
}

void SokobanState::print()
{
  // Print map elements
  cout << endl;
  for(unsigned int y = 0; y < map_.size(); ++y)
  {
    for(unsigned int x = 0; x < map_[0].size(); ++x)
    {
      if (map_[y][x] == CLEAR_CELL)
      {
        cout << "_";
      }
      if (map_[y][x] == BLOCKED_CELL)
      {
        cout << "|";
      }
      else if (map_[y][x] == ROBOT_CELL)
      {
        cout << "R";
      }
      else if (map_[y][x] == GOAL_CELL)
      {
        cout << "G";
      }
      else if (map_[y][x] == BOX_CELL)
      {
        cout << "O";
      }
      else if (map_[y][x] == GOAL_A_CELL)
      {
        cout << "A";
      }
      else if (map_[y][x] == GOAL_B_CELL)
      {
        cout << "B";
      }
      else if (map_[y][x] == GOAL_C_CELL)
      {
        cout << "C";
      }
      else if (map_[y][x] == BOX_A_CELL || map_[y][x] == GOAL_A_SET_CELL )
      {
        cout << "a";
      }
      else if (map_[y][x] == BOX_B_CELL || map_[y][x] == GOAL_B_SET_CELL)
      {
        cout << "b";
      }
      else if (map_[y][x] == BOX_C_CELL || map_[y][x] == GOAL_C_SET_CELL)
      {
        cout << "c";
      }
      else if (map_[y][x] ==   GOAL_SET_CELL)
      {
        cout << "!";
      }
    }
    cout << endl;
  }
  cout << endl;
}

void SokobanState::printTMap()
{
  cout << endl;
  for (unsigned int y = 0; y < t_map_.size(); ++y)
  {
    for (unsigned int x = 0; x < t_map_[y].size(); ++x)
    {
      TMapElement current = t_map_[y][x];
      if (current == TRAVERSABLE)
        cout << "T";
      else if (current == NOT_TRAVERSABLE)
        cout << "x";
      else if (current == UNKNOWN_TRAVERSABLE)
        cout << "?";
    }
    cout << endl;
  }
  cout << endl;
}

//
// Helper methods
//

bool SokobanState::canPushNorth(Coord l) const
{
  Coord l_prime = l;
  l_prime.y_ -= 1;
  return canPushToCell(l_prime);
}
bool SokobanState::canPushSouth(Coord l) const
{
  Coord l_prime = l;
  l_prime.y_ += 1;
  return canPushToCell(l_prime);
}
bool SokobanState::canPushWest(Coord l) const
{
  Coord l_prime = l;
  l_prime.x_ -= 1;
  return canPushToCell(l_prime);
}
bool SokobanState::canPushEast(Coord l) const
{
  Coord l_prime = l;
  l_prime.x_ += 1;
  return canPushToCell(l_prime);
}

bool SokobanState::canPushToCell(Coord l) const
{
  return (map_[l.y_][l.x_] != BLOCKED_CELL &&
          map_[l.y_][l.x_] != BOX_CELL &&
          map_[l.y_][l.x_] != BOX_A_CELL &&
          map_[l.y_][l.x_] != BOX_B_CELL &&
          map_[l.y_][l.x_] != BOX_C_CELL &&
          map_[l.y_][l.x_] != GOAL_SET_CELL &&
          map_[l.y_][l.x_] != GOAL_A_SET_CELL &&
          map_[l.y_][l.x_] != GOAL_B_SET_CELL &&
          map_[l.y_][l.x_] != GOAL_C_SET_CELL);
}

bool SokobanState::atGoal() const
{
  bool at_goal = true;
  for (unsigned int i=0; i < goals_.size(); ++i)
  {
    at_goal = at_goal && goals_[i] == boxes_[i];
  }
  return at_goal;
}

MapElement SokobanState::idToGoalMapElement(BoxLabel label) const
{
  if(label == BOX_A)
  {
    return GOAL_A_CELL;
  }
  else if (label == BOX_B)
  {
    return GOAL_B_CELL;
  }
  else if (label == BOX_C)
  {
    return GOAL_C_CELL;
  }
  return GOAL_CELL;
}

MapElement SokobanState::idToBoxMapElement(BoxLabel label) const
{
  if(label == BOX_A)
  {
    return BOX_A_CELL;
  }
  else if (label == BOX_B)
  {
    return BOX_B_CELL;
  }
  else if (label == BOX_C)
  {
    return BOX_C_CELL;
  }
  return BOX_CELL;
}

int SokobanState::getNumBoxes() const
{
  return boxes_.size();
}

vector<BoxLabel> SokobanState::getBoxLabels() const
{
  return box_ids_;
}

int SokobanState::manhattanDistance(Coord x1, Coord x2)
{
  int dist = 0;
  dist += abs(x1.x_ - x2.x_);
  dist += abs(x1.y_ - x2.y_);
  return dist;
}

bool SokobanState::isSolved()
{
  if (labeled_boxes_)
  {
    for (unsigned int i = 0; i < boxes_.size(); ++i)
    {
      bool matched = boxes_[i] == goals_[goal_indexes_[box_ids_[i]]];
      if (! matched)
        return false;
    }
  }
  else
  {
    for(unsigned int i = 0; i < boxes_.size(); ++i)
    {
      // NOTE: Assume multiple boxes are not at the same location
      bool matched = false;
      for (unsigned int j = 0; j < goals_.size(); ++j)
      {
        if (goals_[j] == boxes_[i])
          matched = true;
      }
      if (!matched)
        return false;
    }
  }
  return true;
}

//
// Methods to interface with the stlastar code
//

float SokobanState::GoalDistanceEstimate( SokobanState &nodeGoal )
{
  float dist_to_go = 0;
  if (labeled_boxes_)
  {
    for (unsigned int i = 0; i < boxes_.size(); ++i)
    {
      dist_to_go += manhattanDistance(boxes_[i],
                                      goals_[goal_indexes_[goal_ids_[i]]]);
    }
  }
  else
  {
    for (unsigned int i = 0; i < boxes_.size(); ++i)
    {
#ifdef USE_ADMISSABLE_VERSION
      float min_dist_to_go = 1000000;
      for (unsigned int j = 0; j < goals_.size(); ++j)
      {
       float dist = manhattanDistance(boxes_[i], goals_[j]);
       if (dist < min_dist_to_go)
         min_dist_to_go = dist;
      }
      dist_to_go += min_dist_to_go;
#else
      dist_to_go += manhattanDistance(boxes_[i], goals_[i]);
#endif
    }
  }
  return dist_to_go;
}

bool SokobanState::IsGoal(SokobanState &nodeGoal)
{
  return isSolved();
}

bool SokobanState::GetSuccessors( AStarSearch<SokobanState> *astarsearch,
                                  SokobanState *parent_node )
{
  bool ret = true;

  // For each box, attempt each of the four moves
  for(unsigned int i = 0; i < boxes_.size(); ++i)
  {
    if (canPushBox(i, PUSH_NORTH))
    {
      SokobanState nextState = pushBox(i, PUSH_NORTH);
      ret = astarsearch->AddSuccessor(nextState);
      if (!ret) return false;
    }
    if (canPushBox(i, PUSH_SOUTH))
    {
      SokobanState nextState = pushBox(i, PUSH_SOUTH);
      ret = astarsearch->AddSuccessor(nextState);
      if (!ret) return false;
    }
    if (canPushBox(i, PUSH_WEST))
    {
      SokobanState nextState = pushBox(i, PUSH_WEST);
      ret = astarsearch->AddSuccessor(nextState);
      if (!ret) return false;
    }
    if (canPushBox(i, PUSH_EAST))
    {
      SokobanState nextState = pushBox(i, PUSH_EAST);
      ret = astarsearch->AddSuccessor(nextState);
      if (!ret) return false;
    }
  }

  return true;
}

float SokobanState::GetCost(SokobanState& successor)
{
  // Move from one state to a succesor always costs 1.0
  return 1.0f;
}

bool SokobanState::IsSameState(SokobanState &rhs)
{
  for (unsigned int y = 0; y < map_.size(); ++y)
  {
    for (unsigned int x = 0; x < map_.size(); ++x)
    {
      if(map_[y][x] != rhs.map_[y][x])
        return false;
    }
  }
  return true;
}

