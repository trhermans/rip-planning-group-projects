#ifndef sokoban_state_h_DEFINED
#define sokoban_state_h_DEFINED

// AStar search class
#include "GoogleCode_astar/stlastar.h"

#include <vector>
#include <iostream>
#include <string>
#include <map>

enum MapElement
{
  CLEAR_CELL,
  BLOCKED_CELL,
  ROBOT_CELL,
  BOX_CELL,
  BOX_A_CELL,
  BOX_B_CELL,
  BOX_C_CELL,
  GOAL_CELL,
  GOAL_A_CELL,
  GOAL_B_CELL,
  GOAL_C_CELL,
  GOAL_SET_CELL,
  GOAL_A_SET_CELL,
  GOAL_B_SET_CELL,
  GOAL_C_SET_CELL,
};

enum Action
{
  PUSH_NORTH,
  PUSH_SOUTH,
  PUSH_WEST,
  PUSH_EAST
};

enum BoxLabel
{
  BOX_A,
  BOX_B,
  BOX_C
};

enum TMapElement
{
  TRAVERSABLE,
  NOT_TRAVERSABLE,
  UNKNOWN_TRAVERSABLE
};

typedef std::vector<std::vector<MapElement> > SokobanMap;
typedef std::vector<MapElement> SokobanMapRow;
typedef std::map<BoxLabel, unsigned int> IDMap;
typedef std::vector<std::vector<TMapElement> > TraversabilityMap;
typedef std::vector<TMapElement> TMapRow;

class Coord
{
 public:
  Coord() : x_(0), y_(0)
  {
  }

  Coord(int x, int y): x_(x), y_(y)
  {
  }

  Coord(const Coord& obj): x_(obj.x_), y_(obj.y_)
  {
  }

  bool operator==(const Coord &other) const
  {
    return (x_ == other.x_ && y_ == other.y_);
  }

  void operator=(const Coord &other)
  {
    x_ = other.x_;
    y_ = other.y_;
  }
  int x_;
  int y_;
};

class SokobanState
{
 public:
  SokobanState(Coord robot_loc, std::vector<Coord> box_locs,
               std::vector<Coord> goal_locs, bool labeled_boxes,
               std::vector<BoxLabel> box_labels,
               std::vector<BoxLabel> goal_labels,
               IDMap box_indexes, IDMap goal_indexes,
               SokobanMap map);
  SokobanState(std::string filename, bool labeled_boxes, bool goal_map=false);
  SokobanState() {}
  void loadMap(std::string filename, bool labeled_boxes=false,
               bool goal_map=false);
  void print();
  void printTMap();
  bool atGoal() const;
  bool canPushBox(int box_id, Action a);
  SokobanState pushBox(int box_id, Action a);
  bool canPushBox(BoxLabel label, Action a);
  SokobanState pushBox(BoxLabel label, Action a);
  bool usingLabels() const { return labeled_boxes_; }
  int getNumBoxes() const;
  std::vector<BoxLabel> getBoxLabels() const;
  bool isSolved();
  // stlastar methods
  float GoalDistanceEstimate( SokobanState &nodeGoal ); // DONE
  bool IsGoal(SokobanState &nodeGoal); // DONE
  bool GetSuccessors( AStarSearch<SokobanState> *astarsearch,
                      SokobanState *parent_node);
  float GetCost(SokobanState &successor); // DONE
  bool IsSameState(SokobanState &rhs); // DONE

 protected:
  void setMap();
  bool canPushNorth(Coord l) const;
  bool canPushSouth(Coord l) const;
  bool canPushWest(Coord l) const;
  bool canPushEast(Coord l) const;
  bool canPushToCell(Coord l) const;
  bool traversableTo(Coord l) const;
  MapElement idToGoalMapElement(BoxLabel label) const;
  MapElement idToBoxMapElement(BoxLabel label) const;
  void buildTraversabilityMap();
  int manhattanDistance(Coord x1, Coord x2);

 protected:
  Coord robot_;
  std::vector<Coord> boxes_;
  std::vector<Coord> goals_;
 public:
  SokobanMap map_;
 protected:
  bool labeled_boxes_;
  std::vector<BoxLabel> box_ids_;
  std::vector<BoxLabel> goal_ids_;
  IDMap box_indexes_;
  IDMap goal_indexes_;
  TraversabilityMap t_map_;
};

#endif // sokoban_state_h_DEFINED
