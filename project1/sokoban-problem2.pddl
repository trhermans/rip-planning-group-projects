(define (problem sokoban1)
    (:domain sokoban)
  (:objects 
up down left right 
block1 block2
p1-1p p1-2p p1-3p p1-4p p1-5p p1-6p p1-7p p1-8p
p2-1p p2-2p p2-3p p2-4p p2-5p p2-6p p2-7p p2-8p
p3-1p p3-2p p3-3p p3-4p p3-5p p3-6p p3-7p p3-8p
p4-1p p4-2p p4-3p p4-4p p4-5p p4-6p p4-7p p4-8p
p5-1p p5-2p p5-3p p5-4p p5-5p p5-6p p5-7p p5-8p
p6-1p p6-2p p6-3p p6-4p p6-5p p6-6p p6-7p p6-8p 
p7-1p p7-2p p7-3p p7-4p p7-5p p7-6p p7-7p p7-8p
p8-1p p8-2p p8-3p p8-4p p8-5p p8-6p p8-7p p8-8p
p9-1p p9-2p p9-3p p9-4p p9-5p p9-6p p9-7p p9-8p
p10-1p p10-2p p10-3p p10-4p p10-5p p10-6p p10-7p p10-8p
p11-1p p11-2p p11-3p p11-4p p11-5p p11-6p p11-7p p11-8p
)


(:init 
(direction up)
(direction down)
(direction left) 
(direction right)
(block block1)
(block block2)


		  (navigable p2-7p) (navigable p3-7p) (navigable p4-7p)

		  (navigable p2-6p) (navigable p3-6p) (navigable p4-6p) (navigable p5-6p)

				    (navigable p3-5p) (navigable p4-5p) (navigable p5-5p)

						      (navigable p4-4p) (navigable p5-4p)

		  							(navigable p5-3p)

		  							(navigable p5-2p)




		              (empty p2-7p) (empty p3-7p) (empty p4-7p)

		              					        (empty p5-6p)

				            (empty p3-5p) (empty p4-5p) (empty p5-5p)

						          (empty p4-4p) (empty p5-4p)

		  							(empty p5-3p)

		  							(empty p5-2p)


(robot-at p2-6p)
(block-at block1 p3-6p)
(block-at block2 p4-6p)

(adjacent p1-1p p2-1p right) 				 (adjacent p1-1p p1-2p up)
(adjacent p1-2p p2-2p right) (adjacent p1-2p p1-1p down) (adjacent p1-2p p1-3p up)
(adjacent p1-3p p2-3p right) (adjacent p1-3p p1-2p down) (adjacent p1-3p p1-4p up)
(adjacent p1-4p p2-4p right) (adjacent p1-4p p1-3p down) (adjacent p1-4p p1-5p up)
(adjacent p1-5p p2-5p right) (adjacent p1-5p p1-4p down) (adjacent p1-5p p1-6p up)
(adjacent p1-6p p2-6p right) (adjacent p1-6p p1-5p down) (adjacent p1-6p p1-7p up)
(adjacent p1-7p p2-7p right) (adjacent p1-7p p1-6p down) (adjacent p1-7p p1-8p up)
(adjacent p1-8p p2-8p right) (adjacent p1-8p p1-7p down)

(adjacent p2-1p p1-1p left) (adjacent p2-1p p3-1p right) 			     (adjacent p2-1p p2-2p up)
(adjacent p2-2p p1-2p left) (adjacent p2-2p p3-2p right) (adjacent p2-2p p2-1p down) (adjacent p2-2p p2-3p up)
(adjacent p2-3p p1-3p left) (adjacent p2-3p p3-3p right) (adjacent p2-3p p2-2p down) (adjacent p2-3p p2-4p up)
(adjacent p2-4p p1-4p left) (adjacent p2-4p p3-4p right) (adjacent p2-4p p2-3p down) (adjacent p2-4p p2-5p up)
(adjacent p2-5p p1-5p left) (adjacent p2-5p p3-5p right) (adjacent p2-5p p2-4p down) (adjacent p2-5p p2-6p up)
(adjacent p2-6p p1-6p left) (adjacent p2-6p p3-6p right) (adjacent p2-6p p2-5p down) (adjacent p2-6p p2-7p up)
(adjacent p2-7p p1-7p left) (adjacent p2-7p p3-7p right) (adjacent p2-7p p2-6p down) (adjacent p2-7p p2-8p up)
(adjacent p2-8p p1-8p left) (adjacent p2-8p p3-8p right) (adjacent p2-8p p2-7p down)

(adjacent p3-1p p2-1p left) (adjacent p3-1p p4-1p right) 			     (adjacent p3-1p p3-2p up)
(adjacent p3-2p p2-2p left) (adjacent p3-2p p4-2p right) (adjacent p3-2p p3-1p down) (adjacent p3-2p p3-3p up)
(adjacent p3-3p p2-3p left) (adjacent p3-3p p4-3p right) (adjacent p3-3p p3-2p down) (adjacent p3-3p p3-4p up)
(adjacent p3-4p p2-4p left) (adjacent p3-4p p4-4p right) (adjacent p3-4p p3-3p down) (adjacent p3-4p p3-5p up)
(adjacent p3-5p p2-5p left) (adjacent p3-5p p4-5p right) (adjacent p3-5p p3-4p down) (adjacent p3-5p p3-6p up)
(adjacent p3-6p p2-6p left) (adjacent p3-6p p4-6p right) (adjacent p3-6p p3-5p down) (adjacent p3-6p p3-7p up)
(adjacent p3-7p p2-7p left) (adjacent p3-7p p4-7p right) (adjacent p3-7p p3-6p down) (adjacent p3-7p p3-8p up)
(adjacent p3-8p p2-8p left) (adjacent p3-8p p4-8p right) (adjacent p3-8p p3-7p down)

(adjacent p4-1p p3-1p left) (adjacent p4-1p p5-1p right) 			     (adjacent p4-1p p4-2p up)
(adjacent p4-2p p3-2p left) (adjacent p4-2p p5-2p right) (adjacent p4-2p p4-1p down) (adjacent p4-2p p4-3p up)
(adjacent p4-3p p3-3p left) (adjacent p4-3p p5-3p right) (adjacent p4-3p p4-2p down) (adjacent p4-3p p4-4p up)
(adjacent p4-4p p3-4p left) (adjacent p4-4p p5-4p right) (adjacent p4-4p p4-3p down) (adjacent p4-4p p4-5p up)
(adjacent p4-5p p3-5p left) (adjacent p4-5p p5-5p right) (adjacent p4-5p p4-4p down) (adjacent p4-5p p4-6p up)
(adjacent p4-6p p3-6p left) (adjacent p4-6p p5-6p right) (adjacent p4-6p p4-5p down) (adjacent p4-6p p4-7p up)
(adjacent p4-7p p3-7p left) (adjacent p4-7p p5-7p right) (adjacent p4-7p p4-6p down) (adjacent p4-7p p4-8p up)
(adjacent p4-8p p3-8p left) (adjacent p4-8p p5-8p right) (adjacent p4-8p p4-7p down)

(adjacent p5-1p p4-1p left) (adjacent p5-1p p6-1p right) 			     (adjacent p5-1p p5-2p up)
(adjacent p5-2p p4-2p left) (adjacent p5-2p p6-2p right) (adjacent p5-2p p5-1p down) (adjacent p5-2p p5-3p up)
(adjacent p5-3p p4-3p left) (adjacent p5-3p p6-3p right) (adjacent p5-3p p5-2p down) (adjacent p5-3p p5-4p up)
(adjacent p5-4p p4-4p left) (adjacent p5-4p p6-4p right) (adjacent p5-4p p5-3p down) (adjacent p5-4p p5-5p up)
(adjacent p5-5p p4-5p left) (adjacent p5-5p p6-5p right) (adjacent p5-5p p5-4p down) (adjacent p5-5p p5-6p up)
(adjacent p5-6p p4-6p left) (adjacent p5-6p p6-6p right) (adjacent p5-6p p5-5p down) (adjacent p5-6p p5-7p up)
(adjacent p5-7p p4-7p left) (adjacent p5-7p p6-7p right) (adjacent p5-7p p5-6p down) (adjacent p5-7p p5-8p up)
(adjacent p5-8p p4-8p left) (adjacent p5-8p p6-8p right) (adjacent p5-8p p5-7p down)

(adjacent p6-1p p5-1p left) (adjacent p6-1p p7-1p right) 			     (adjacent p6-1p p6-2p up)
(adjacent p6-2p p5-2p left) (adjacent p6-2p p7-2p right) (adjacent p6-2p p6-1p down) (adjacent p6-2p p6-3p up)
(adjacent p6-3p p5-3p left) (adjacent p6-3p p7-3p right) (adjacent p6-3p p6-2p down) (adjacent p6-3p p6-4p up)
(adjacent p6-4p p5-4p left) (adjacent p6-4p p7-4p right) (adjacent p6-4p p6-3p down) (adjacent p6-4p p6-5p up)
(adjacent p6-5p p5-5p left) (adjacent p6-5p p7-5p right) (adjacent p6-5p p6-4p down) (adjacent p6-5p p6-6p up)
(adjacent p6-6p p5-6p left) (adjacent p6-6p p7-6p right) (adjacent p6-6p p6-5p down) (adjacent p6-6p p6-7p up)
(adjacent p6-7p p5-7p left) (adjacent p6-7p p7-7p right) (adjacent p6-7p p6-6p down) (adjacent p6-7p p6-8p up)
(adjacent p6-8p p5-8p left) (adjacent p6-8p p7-8p right) (adjacent p6-8p p6-7p down)

(adjacent p7-1p p6-1p left) (adjacent p7-1p p7-2p up)
(adjacent p7-2p p6-2p left) (adjacent p7-2p p7-1p down) (adjacent p7-2p p7-3p up)
(adjacent p7-3p p6-3p left) (adjacent p7-3p p7-2p down) (adjacent p7-3p p7-4p up)
(adjacent p7-4p p6-4p left) (adjacent p7-4p p7-3p down) (adjacent p7-4p p7-5p up)
(adjacent p7-5p p6-5p left) (adjacent p7-5p p7-4p down) (adjacent p7-5p p7-6p up)
(adjacent p7-6p p6-6p left) (adjacent p7-6p p7-5p down) (adjacent p7-6p p7-7p up)
(adjacent p7-7p p6-7p left) (adjacent p7-7p p7-6p down) (adjacent p7-7p p7-8p up)
(adjacent p7-8p p6-8p left) (adjacent p7-8p p7-7p down))



(:goal (and (block-at block1 p2-7p) (block-at block2 p5-2p) )))

