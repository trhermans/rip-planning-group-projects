\section{Project Part II: Sokoban Planner}
We chose to solve the problem using a state space planner using A* search to find the solution.  We define our state as the locations of the boxes, goals, and robots, as well as the static obstacles in the map.  We define actions in the domain as pushing a box in one of four directions: North, South, East, or West. For a given map, we define these four actions separately four each of the boxes, giving us a maximum of 12 actions in the problems considered here. By not explicitly defining the actions of the robot moving to different locations, we reduce the branching factor of the search tree. We do, however, implicitly deal with the restrictions imposed by the robot's location by not allowing actions to occur when the robot can not reach the required location to push the box (i.e. the robot can not push the box West, if there is no traversable path to the location East of the box). Additionally we remove actions when the location the box should be pushed to is blocked.

We calculate the traversability of the robot to all locations on the map when we encounter a new state.  We use a wavefront expansion method to calculate this traversability map; namely, we set the robot's location to traversable and add this location to a queue. We then examine each of its neighbors and mark those that are clear and unmarked as traversable, and append them to the queue.  We then pop the front element from the queue and repeat this operation until the queue is empty. At this point any reachable location on the grid is marked as traversable and testing traversability for the robot requires only a lookup in the traversability map.

The behavior of A* is most controlled by the choice of heuristic. The heuristic we use is defined as:
\[
h(x)=\sum_{b \in boxes} \texttt{manhattanDistance}(b, g_b)
\]
where \(g_b\) is the goal associated with box \(b\). In problems 2.1, 2.2, and 2.3 this heuristic is well defined, since \(g_b\) is well defined for each box; however, this is not true in general or for the Sokoban Challenge in specific.

We examine two methods of performing this mapping for the general case: one version which is admissible and one that is not, but produces better performance. We first note that the heuristic is admissible when the mapping \(b \rightarrow g_b\) is well defined, since the Manhattan distance is the shortest possible number of \{East, West, North, and South\} pushes between the two locations.  The first mapping we attempted from \(b\) to \(g_b\) was to arbitrarily assign goals to boxes.  Our program simply pairs the first observed box with the first observed goal, the second box with the second goal, and so on. While this makes the heuristic easy to compute, it does not produce an admissible heuristic, since we could rearrange which goal positions the boxes are located in, which still gives a valid goal state, but has a heuristic value greater than zero. While, this method produced a correct solution, we wished to define an admissible version, so that we could ensure finding an optimal solution.  To do so we modify the heuristic to take the following form:
\[
h_{a}(x)=\sum_{b \in boxes} \operatorname*{min}_{g \in Goals}(\texttt{manhattanDistance}(b, g))
\]
In this form of the heuristic the distance of any box is to its nearest goal, which removes the issue defined above, since any box located at a goal location has a distance of score and thus the heuristic is admissible. We compare the results found with these two methods in Figure~\ref{fig:our-planner}.

The main finding is that the admissible version of our heuristic takes nearly twice as long, exploring over 2,000 more states, while returning a plan with an equal number of steps. The main reason for this is that the admissible version of the heuristic is less informative. In terms of \(h_a\) states where multiple boxes are closest to the same goal position may greatly underestimate the distance to the solution, compared to the heuristic values of the original heuristic, which assigns lower costs to states where all boxes are closer to different goals. Furthermore the configurations where our heuristic is admissible are already close to a final goal state, so the overestimate will not be large if the goal locations are near one another (as they are in the Sokoban challenge.)  Our results represent our implementation in C++, using a freely available version of A* search. Results reported in Figure~\ref{fig:our-planner} were computed on a machine with 2 quad-core 2.67 GHz Intel Core i7 processors, 6 GB of RAM running linux-2.6.32-24 x86\_64 compiled with g++ and -o3 optimization.

\begin{figure}
  \centering
  \begin{tabular}{|r|c|c|c|c|}\hline
                          & P.2.1 & P.2.2 & P.2.3 & Sokoban Challenge \\ \hline
    Num Steps in Plan     &   4   &   11  &    21  &       18 / (18)  \\ \hline
    Num States Explored   &   5   &   97  &   454  &   7013 / (9239)  \\ \hline
    Computation Time (s)  &   0.002   &   0.004  &   0.048  &   7.698 / (13.598) \\ \hline

  \end{tabular}
  \caption{Search and solution size for our planner. The values in parenthesis correspond to the results using the admissible version of our heuristic.\label{fig:our-planner}.}
\end{figure}

\subsection{Questions}
\begin{enumerate}
\item \textbf{Plans:}
We present plans as sequences of actions. Each action is defined by (box, push\_direction). For problems where boxes are not labeled, we label boxes in order of position when reading the map from top left to bottom right.
  \begin{itemize}
  \item \textbf{Problem 2.1:} ((a, PUSH\_WEST), (a, PUSH\_NORTH), (a, PUSH\_NORTH), (a, PUSH\_NORTH))
  \item \textbf{Problem 2.2:} ((b, PUSH\_SOUTH), (a, PUSH\_EAST), (b, PUSH\_EAST), (a, PUSH\_SOUTH), (b, PUSH\_SOUTH), (b, PUSH\_SOUTH), (a, PUSH\_NORTH), (a, PUSH\_WEST), (a, PUSH\_NORTH), (a, PUSH\_WEST), (b, PUSH\_SOUTH))
  \item \textbf{Probelm 2.3:} ((c, PUSH\_WEST), (c, PUSH\_WEST), (b, PUSH\_WEST), (b, PUSH\_WEST), (a, PUSH\_WEST), (a, PUSH\_SOUTH), (a, PUSH\_NORTH), (a, PUSH\_NORTH), (a, PUSH\_NORTH), (a, PUSH\_NORTH), (b, PUSH\_EAST), (b, PUSH\_EAST), (b, PUSH\_EAST), (c, PUSH\_EAST), (b, PUSH\_NORTH), (c, PUSH\_EAST), (c, PUSH\_EAST), (c, PUSH\_EAST), (b, PUSH\_NORTH), (c, PUSH\_EAST), (c, PUSH\_NORTH))
  \item \textbf{Sokoban Challenge:}((b, PUSH\_EAST), (b, PUSH\_EAST), (c, PUSH\_NORTH), (c, PUSH\_EAST), (b, PUSH\_WEST), (b, PUSH\_WEST), (c, PUSH\_EAST), (a, PUSH\_SOUTH), (a, PUSH\_SOUTH), (a, PUSH\_SOUTH), (b, PUSH\_EAST), (b, PUSH\_NORTH), (c, PUSH\_WEST), (a, PUSH\_WEST), (c, PUSH\_EAST), (a, PUSH\_NORTH), (b, PUSH\_SOUTH), (b, PUSH\_SOUTH))


  \end{itemize}
\item \textbf{Comparison to PDDL Planners:} The black box planner to so long to run on the Sokoban problems that we did not collect exact results for it. We did, however, examine the results of FF with PDDL language description. It took 0.01s,0.01s,0.03s and 0.03s respectively for the sokoban problems for FF planner. Our planner took 0.002s, 0.004s, 0.048s and 7.69s to find the solutions. The computation time for the first three problems are similar, however our planner took much more time for the sokoban challenge. The heuristics we used was informative, but still was the main reason for the computation to take longer. FF is a proved planner with certain mechanisms to increase efficiency. Our planner proved to be inefficient compared to FF when the state space is bigger. 
\newline For Sokoban Challenge, our planner has 18 steps in the solution. However, we count only the pushing actions as the steps. Therefore, the optimality criteria is the number of push actions in the solution. On the other hand, FF took 90 total steps(24 push actions + 66 move actions). In the sokoban challenge problem, it is not defined which box is going to which spot. Therefore, all 3!=6 possibilities were included in the goal specification by the "OR" operator. Our solution has less push actions than the FF's solution, however this is because FF wants to minimize the total number of steps in the plan. Among the 6 possible goal states, FF came up with the plan with least number of steps. So both algorithms seemed to be optimal with different criterias.

\item \textbf{Completeness:} The core of our planner is the use of the A* search algorithm over our state space.  A* is complete, given a complete domain representation.  As described above our system correctly determines the set of possible actions by testing the legality of pushing a given box as well as the traversability to the box using the wavefront expansion technique.
\item \textbf{Efficiency:} Our main attempt at increasing the speed of the planner was in only reasoning about the pushing of boxes and not the movements of the robot. Beyond this the examination of multiple versions of our heuristic, as explained above, was an attempt to examine the speed of the planner. Finally, the coding of the legality tests for actions, as well as, the building of the traversability map was restructured in such a way, that the returning of a false state would occur as fast as possible, prior to performing all logical tests.
\end{enumerate}
