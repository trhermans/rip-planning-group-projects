/**
 * @file   astar_sokoban.cpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 * @date   Thu Sep 23 17:50:51 2010
 *
 * @brief  This is the file to house the main executable for the Sokoban
 * project. It builds the model and runs the planner.
 *
 */

#include "sokoban_state.h"
#include <iostream>
#include <sstream>

using namespace std;
int main(int argc, char** argv)
{
  string map;
  bool labeled_boxes = false;
  bool print_maps = true;
  bool print_tmaps = false;
  if (argc > 1)
  {
    stringstream map_name;
    map_name << argv[1];
    map = map_name.str();
    if (argc > 2)
      labeled_boxes = true;
  }
  else
  {
    map = "./map1.txt";
  }

  // Setup stuff for running search
  AStarSearch<SokobanState> astar;
  SokobanState init_state(map, labeled_boxes);
  SokobanState goal_state(map, labeled_boxes, true);
  astar.SetStartAndGoalStates( init_state, goal_state );
  unsigned int searchState = 0;
  unsigned int searchSteps = 0;

  // Run the search
  do
  {
    searchState = astar.SearchStep();
    searchSteps++;
  } while (searchState == AStarSearch<SokobanState>::SEARCH_STATE_SEARCHING);

  // Print the results
  cout << "Took " << searchSteps << " search steps." << endl;
  if( searchState == AStarSearch<SokobanState>::SEARCH_STATE_SUCCEEDED )
  {
    cout << "Succesfully completed search." << endl;
    SokobanState *node = astar.GetSolutionStart();
    cout << "Init state" << endl;
    node->print();
    unsigned int steps = 0;
    for( ;; )
    {
      node = astar.GetSolutionNext();

      if( !node )
      {
        break;
      }

      steps ++;
      cout << "Step: " << steps << endl;
      if (print_maps) node->print();
      if (print_tmaps) node->printTMap();
    };
    cout << "Completed plan has " << steps << " steps" << endl;
    cout << "Number of A* steps: " << astar.GetStepCount() << endl;
  }
  else if( searchState == AStarSearch<SokobanState>::SEARCH_STATE_FAILED )
  {
    cout << "Search terminated. Did not find solution." << endl;
  }
  else if( searchState == AStarSearch<SokobanState>::SEARCH_STATE_OUT_OF_MEMORY)
  {
    cout << "Search terminated. Out of memory" << endl;
  }
  return 0;
}
